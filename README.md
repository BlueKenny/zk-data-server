# ZK-DATA für Linux

- ## FEATURES :

- ## REQUIREMENT :

### SERVER :

        OS : Linux/Unix Distro
        command line tool "git"
        command line tool "zip"
        python3
        python pip3
        python module peewee
        command line tool "zip"
        libreoffice (command line)
        LPR (Cups)
        


### CLIENT (PC) :

        Qt5
        Qmlscene
        LPR (Cups)
        xClip

### CLIENT (PHONE) :

## INSTALLATION :

### SERVER :

            1. Cloning git repository "https://gitlab.com/BlueKenny/zk-data.git" in your home directory
            linux command: "git clone https://gitlab.com/BlueKenny/zk-data.git"
        2. start "runServer.py" in qml
            linux command: "cd qml && ./runServer.py"

You should see the Server is runnung: 
![alt text](assets/0.png)

Now your Clients can connect to this Server (PORT 51515)

### CLIENT (PC) :

        1. Cloning git repository "https://gitlab.com/BlueKenny/zk-data.git" in your home directory
            linux command: "git clone https://gitlab.com/BlueKenny/zk-data.git"
        2. run "SucheStock.pyw" in qml
            linux command: "cd qml && ./SucheStock.pyw"

### CLIENT (PHONE) :
