#!/usr/bin/env python3
import os
import sys

cmd = "cd /etc/zk-data-server && ./Server.py"
if len(sys.argv) > 1: cmd = cmd + " " + sys.argv[1]

os.system(cmd)
