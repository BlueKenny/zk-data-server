#!/usr/bin/env python3

# 4600

def promo(client_as_dict, delivery_as_dict, article_as_dict, price): # return price
    original_price = float(article_as_dict["preisvk"])
    try: price = float(price)
    except: price = original_price
    
    if str(client_as_dict["identification"]) == "4600": # = BURKARDT SRL
        new_price = float(article_as_dict["preisek"]) * (1 + float(article_as_dict["tva"])/100 )
        new_price = round(new_price, 2)

        #print("new_price: " + str(new_price) + " vs price: " + str(price))
        if new_price > price:
            new_price = price
        else:
            print("=> " + str(new_price))
    else:
        new_price = price        
    return new_price
