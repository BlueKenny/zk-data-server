#!/usr/bin/env python3

# WENN KUNDE -> MwSt n°
# WENN Artikel identification nicht 160325 - 160326 oder 160379 (Brennholz)
# WENN Artikel Kategorie nicht Produkte (18)
# WENN Artikel Kategorie nicht Socklaender (24)

def promo(client_as_dict, delivery_as_dict, article_as_dict, price): # return bool
    original_price = float(article_as_dict["preisvk"])
    try: price = float(price)
    except: price = original_price
    
    if not client_as_dict["tva"].rstrip() == "":
        print("Firma F10 -> 10%")
        
        exclude_list = ["160325", "160326", "160379"]
        if not str(article_as_dict["identification"]) in exclude_list:
            if not str(article_as_dict["categorie"]) == "18": #Produkte
                if not str(article_as_dict["categorie"]) == "24": #Socklaender
                    new_price = round(original_price * 0.9, 2)
                    
                    #print("new_price: " + str(new_price))
                
                    #print("new_price: " + str(new_price) + " vs price: " + str(price))
                    if new_price > price:
                        new_price = price
                    else:
                        print("=> " + str(new_price))
                else:
                    new_price = price
            else:
                new_price = price
        else:
            new_price = price
    else:
        new_price = price        
    return new_price
