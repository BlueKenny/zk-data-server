#!/usr/bin/env python3

# this is a default promo
# for every client
# if article price < 50.00 or article price = 50.00 then -5%
# the promo is upper rounded to X.5 or X.0

def promo(client_as_dict, delivery_as_dict, article_as_dict, price): # return bool
    original_price = float(article_as_dict["preisvk"])
    try: price = float(price)
    except: price = original_price
    
    if original_price == 50.0 or original_price < 50.0:
        #print("original_price: " + str(original_price))
        print("use lower or egal than 50.00€ => -5%")
        new_price = round(original_price * 0.95, 1)
        #print("new_price: " + str(new_price))

        decimal = int(str(new_price).split(".")[1])
        if decimal == 0:
            new_price = float(int(new_price))
        elif decimal < 6:
            new_price = float(int(new_price) + 0.5)
        else:
            new_price = float(int(new_price) + 1)

        #print("new_price: " + str(new_price) + " vs price: " + str(price))
        if new_price > price:
            new_price = price
        else:
            print("=> " + str(new_price))
    else:
        new_price = price        
    return new_price
