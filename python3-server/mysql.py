#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.BlueFunc
import pick

if len(sys.argv) == 1:
    print("need server name as argument !")
    
    title = "ZK-DATA Server \n\n"
    server_list = os.listdir("/etc/zk-data-server/data/")
    if len(server_list) == 1:
        server_name = server_list[0]
    else:
        server_name = pick.pick(server_list, title + "Server auswählen: ", indicator='-> ')[0]
else:  
    server_name = sys.argv[1]
print("server_name: " + str(server_name))

data_dir = "/etc/zk-data-server/data/" + server_name + "/"
print("data_dir: " + data_dir)


server_mysql_database = libs.BlueFunc.BlueLoad("SERVER_MYSQL_DATABASE", data_dir)
print("server_mysql_database: " + str(server_mysql_database))
    
server_mysql_user = libs.BlueFunc.BlueLoad("SERVER_MYSQL_USER", data_dir)
print("server_mysql_user: " + str(server_mysql_user))

server_mysql_password = libs.BlueFunc.BlueLoad("SERVER_MYSQL_PASSWORD", data_dir)
print("server_mysql_password: [" + str(server_mysql_password) + "]")
    
    
os.system("sudo mysql -u root -p -e \"CREATE DATABASE IF NOT EXISTS " + server_mysql_database + ";\"")
os.system("sudo mysql -u root -p -e \"GRANT ALL PRIVILEGES ON " + server_mysql_database + ".* TO '" + server_mysql_user + "'@'localhost' IDENTIFIED BY '" + server_mysql_password + "';\"")

#os.system("sudo mysql -u root -p -e \"CREATE DATABASE IF NOT EXISTS burkardt_test;\"")
#os.system("sudo mysql -u root -p -e \"GRANT ALL PRIVILEGES ON burkardt_test.* TO 'burk_user1'@'localhost' IDENTIFIED BY 'tustpass41u';\"")




'''
sudo apt install -y default-mysql-server default-mysql-client libgda-5.0-mysql
# or
sudo dnf install -y mariadb-server mariadb
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service

https://blog.bestariwebhost.com/en/how-to-create-user-and-database-mysql-via-command-line-cli/

MariaDB

databasename=burkardt_test
username=burk_user1
domain=localhost
password=tustpass41u

# The first step is to enter mysql as root:
sudo mysql -u root

# then, Create the database:
CREATE DATABASE burkardt_test;
CREATE DATABASE zaunz_test;

# After that, give privileges to the new user:
GRANT ALL PRIVILEGES ON burkardt_test.* TO 'burk_user1'@'localhost' IDENTIFIED BY 'tustpass41u';
GRANT ALL PRIVILEGES ON zaunz_test.* TO 'zaunz_user1'@'localhost' IDENTIFIED BY 'tustpass41u';


#### phpMyAdmin
sudo apt install php8.2-mysql php8.2-mbstring
sudo /etc/init.d/apache2 restart
'''
