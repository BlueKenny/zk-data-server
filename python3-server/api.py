#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
sys.path.append("/etc/zk-data-server/")
sys.path.append("/etc/zk-data-libs/api/")
import libs.send
import libs.BlueFunc

import hashlib

import time
import requests

server_name = sys.argv[1]
api_ip = sys.argv[2]
api_secure_key = sys.argv[3]

data_dir = "/etc/zk-data-server/data/" + server_name + "/"
print("data_dir: " + data_dir)

# zaunz
api_zaunz_lastchange = str(libs.BlueFunc.BlueLoad("api_zaunz_lastchange", data_dir))
if api_zaunz_lastchange == "None": libs.BlueFunc.BlueSave("api_zaunz_lastchange", "0.0", data_dir)

# woocommerce
import libs.api.api_woocommerce

api_woocommerce_url = str(libs.BlueFunc.BlueLoad("api_woocommerce_url", data_dir))
if api_woocommerce_url == "None": libs.BlueFunc.BlueSave("api_woocommerce_url", "None", data_dir)
else:
    if not "wp-json/wc/v3" in api_woocommerce_url:
        if not api_woocommerce_url[-1] == "/":
            api_woocommerce_url = api_woocommerce_url + "/"
            
        api_woocommerce_url = api_woocommerce_url.replace("http://", "https://")   
        api_woocommerce_url = api_woocommerce_url + "wp-json/wc/v3/"            
        libs.BlueFunc.BlueSave("api_woocommerce_url", api_woocommerce_url, data_dir)


api_woocommerce_key = str(libs.BlueFunc.BlueLoad("api_woocommerce_key", data_dir))
if api_woocommerce_key == "None": libs.BlueFunc.BlueSave("api_woocommerce_key", "None", data_dir)

api_woocommerce_secret = str(libs.BlueFunc.BlueLoad("api_woocommerce_secret", data_dir))
if api_woocommerce_secret == "None": libs.BlueFunc.BlueSave("api_woocommerce_secret", "None", data_dir)
 

api_woocommerce_last_order = str(libs.BlueFunc.BlueLoad("api_woocommerce_last_order", data_dir))
if api_woocommerce_last_order == "None": api_woocommerce_last_order = 1

    
timestamp_index = str(libs.BlueFunc.BlueLoad("api_last_timestamp", data_dir))
if timestamp_index == "None": timestamp_index = 0
    
        
def load_article(bcode):
    art_dict = libs.send.GetArt(api_ip, api_secure_key, bcode)
    return art_dict
    
def load_next_article(index):
    print("load_next_article(" + str(index) + ")")
    test = libs.send.get_next_changed_article(api_ip, api_secure_key, str(index))
    print("test: " + str(test))
    if not test == ["", 0.0]:
        identification = test[0]
        timestamp = test[1]
        return [str(identification), str(timestamp)]
    else: return -1


while True:    
    print("\n")        
    
    # Orders WooCommerce woocommerce
    if api_woocommerce_url == "None": 
        woocommerce_orders = []
    else:
        woocommerce_orders = libs.api.api_woocommerce.get_orders(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret)    
    #print("woocommerce_orders: " + str(woocommerce_orders))
    
    for order in woocommerce_orders:
        if int(api_woocommerce_last_order) < int(order):
            print("woocommerce order: " + str(order))
            maschine = "[woocommerce-" + str(order) + "]"
                       
            woocommerce_kunde = libs.api.api_woocommerce.get_client_from_order(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, order)
            woocommerce_kunde_id = "[woocommerce-" + str(woocommerce_kunde["identification"]) + "]"
            print("woocommerce_kunde_id: " + str(woocommerce_kunde_id))    
            
            del woocommerce_kunde["identification"]             
            
            query_list = libs.send.search_client(api_ip, api_secure_key, {"info": woocommerce_kunde_id})
            print("query_list: " + str(query_list))
                
            if query_list == []: #new client
                print("new client")
                client_dict = libs.send.create_client(api_ip, api_secure_key)
                    
                if not client_dict == []:
                    client_dict["info"] = woocommerce_kunde_id
            else:# get client
                client_dict = libs.send.get_client(api_ip, api_secure_key, query_list[0])
                print("client exists: " + str(client_dict))
                   
            for key in woocommerce_kunde:
                key = str(key)
                client_dict[key] = woocommerce_kunde[key]
            
            libs.send.set_client(api_ip, api_secure_key, client_dict)   # set new client data     
            
            query = libs.send.search_delivery(api_ip, api_secure_key, {"maschine": maschine})
            print("query: " + str(query))
            if query == []:## neuer lieferschein
                delivery = libs.send.create_delivery(api_ip, api_secure_key)                
                libs.send.set_delivery_engine(api_ip, api_secure_key, delivery["identification"], maschine)
            else: # get lieferschein
                delivery = libs.send.get_delivery(api_ip, api_secure_key, query[0])
                
            print("delivery: L" + str(delivery["identification"]))
            print("client: K" + str(client_dict["identification"]))
            
            libs.send.set_delivery_client(api_ip, api_secure_key, delivery["identification"] , client_dict["identification"])
            
            delivery_info = libs.api.api_woocommerce.get_info_from_order(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, order)   
            libs.send.set_delivery_info(api_ip, api_secure_key, delivery["identification"], delivery_info)
              
            api_woocommerce_last_order = order
            libs.BlueFunc.BlueSave("api_woocommerce_last_order", order, data_dir)
         
    ########################
    
    query = load_next_article(timestamp_index)
    if not query == -1:        
        identification = query[0]
        timestamp_index = query[1]
        print("identification: " + str(identification))
        
        if True:#len(identification) == 6:
            art_dict = load_article(identification)
            quantity = str(float(art_dict["anzahl"]))
            quantity_minus = str(float(art_dict["anzahl"])-1)
            
            # ZAUNZ            
            response = requests.get( "https://api.zaunz.org/set_stock.php?user=" + server_name + "&identification=" + str(art_dict["identification"]) + "&artikel=" + str(art_dict["artikel"]) + "&ort=" + art_dict["ort"] + "&anzahl=" + str(art_dict["anzahl"]) + "&name=" + art_dict["name_de"] + "&preis=" + str(art_dict["preisvk"]) )
            print("api zaunz: " + str(response.text))        
                         
            # WooCommerce
            try:
                if not api_woocommerce_url == "None":
                    stock_id = libs.api.api_woocommerce.search(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, identification)
                    libs.api.api_woocommerce.set_stock(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, stock_id, quantity)
                    libs.api.api_woocommerce.set_price(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, stock_id, art_dict["preisvk"])
                    libs.api.api_woocommerce.set_reviews_allowed(api_woocommerce_url, api_woocommerce_key, api_woocommerce_secret, stock_id)
            except: 
                print("Error in WooCommerce API")
            
        
        libs.BlueFunc.BlueSave("api_last_timestamp", str(timestamp_index), data_dir)
        
    else:   
        print("All Synced -> Stop API")
        break
        
