#!/usr/bin/env python3
import os
import sys
import pick
import time
   
sys.path.append("/etc/zk-data-libs/")
import libs.sendmail
      
print("------------- > zk-data-server-config < -------------")
def show_create_new_server():
    title = "ZK-DATA Server \n\n"
    create_new_server = bool(pick.pick(["No", "Yes"], title + "Create new server?: ", indicator='-> ')[1])
    if create_new_server:
        new_name = input("Input new server name: \n")
        new_name = new_name.lower()
        if not os.path.exists("/etc/zk-data-server/data/" + new_name): os.system("mkdir /etc/zk-data-server/data/" + new_name)
        os.system("cp -r /etc/zk-data-server/default_files/* /etc/zk-data-server/data/" + new_name + "/")
        
        write_service_file(new_name)
        server_start(new_name)
        
        show_select_server()
        
    else:
        show_select_server()

def show_select_server():    
    server_list = os.listdir("/etc/zk-data-server/data/")
    server_list.append("create new server")
    server_list.append("exit")
    title = "ZK-DATA Server " + show_version() + "\n\n"
    selected_server = pick.pick(server_list, title + "Server auswählen: ", indicator='-> ')[0]
    print("selected_server: " + str(selected_server))
    
    if selected_server == "create new server":
        show_create_new_server()
    elif selected_server == "exit":
        print("exit")
    else:
        show_main_menu(selected_server)
    
def show_main_menu(selected_server):
    title = "ZK-DATA Server - " + selected_server + " - " + show_version() + "\n\n"
    main_menu = [["Start server", "Stop server"][server_status(selected_server)], "Autostart ON/OFF", "LOG", "LOG Extras", "Send Log", "Update Server", "Delete Server", "Exit"]
    selected_menu = pick.pick(main_menu, title + "Main menu: ", indicator='-> ')[1]
    if selected_menu == 0: # "Start / Stop server"
        print("Start / Stop server")
        write_service_file(selected_server)
        if server_status(selected_server):
            server_stop(selected_server)
        else:
            server_start(selected_server)
        show_main_menu(selected_server)    
    elif selected_menu == 1: # "Autostart ON/OFF"
        print("Autostart ON/OFF")  
        show_main_menu(selected_server)        
    elif selected_menu == 2: # LOG
        print(server_get_log(selected_server))
        #os.system("tail -n 100 /tmp/zk-data-server-" + selected_server + ".log")
    elif selected_menu == 3: # LOG Extras
        print(server_extras_get_log(selected_server))  
        #os.system("tail -n 100 /tmp/zk-data-server-extras-" + selected_server + ".log") 
    elif selected_menu == 4: # Send Log
        print("Send Log " + selected_server)         
    elif selected_menu == 5: # Update Server
        update_server()          
        show_main_menu(selected_server)  
    elif selected_menu == 6: # Delete Server
        delete = bool(pick.pick(["No", "Yes"], title + "Delete server " + selected_server + "?: ", indicator='-> ')[1])
        if delete:
            server_stop(selected_server)
            os.system("sudo rm -r /etc/zk-data-server/data/" + selected_server)
            show_select_server()
        else:
            show_main_menu(selected_server)
    elif selected_menu == 7: # Exit
        show_select_server()
        
def update_server():
    os.system("sudo apt clean && sudo apt update && sudo apt install -y zk-data-server")
    
def server_extras_get_log(selected_server):
    log_file = "/tmp/zk-data-server-extras-" + selected_server + ".log"
    os.system("sudo journalctl -n 100 -u \"zk-data-server-extras-" + selected_server + "\" > " + log_file + " && chmod 777 " + log_file)
    log = open(log_file, "r").read()
    return log
    
def server_get_log(selected_server):
    log_file = "/tmp/zk-data-server-" + selected_server + ".log"
    os.system("sudo journalctl -n 100 -u \"zk-data-server-" + selected_server + "\" > " + log_file + " && chmod 777 " + log_file)
    log = open(log_file, "r").read()
    return log
    
def server_send_log(selected_server):
    log_file = "/tmp/zk-data-server-" + selected_server + ".log"
    os.system("sudo journalctl -n 100 -u \"zk-data-server-" + selected_server + "\" > " + log_file + " && chmod 777 " + log_file)
    libs.sendmail.sendmail("Error Log ZK-DATA SERVER", "Error Log ZK-DATA SERVER","zk-data@" + selected_server, "kenny@zaunz.org", log_file)
    
    
def server_start(selected_server):
    print("server_start(" + selected_server + ")")
    print("Start main server")
    os.system("sudo systemctl start zk-data-server-" + selected_server)
    time.sleep(2)
    print("Start extras")
    os.system("sudo systemctl start zk-data-server-extras-" + selected_server)
    
def server_stop(selected_server):
    print("server_stop(" + selected_server + ")")
    print("Stop extras")
    os.system("sudo systemctl stop zk-data-server-extras-" + selected_server)
    time.sleep(2)
    print("Stop main server")
    os.system("sudo systemctl stop zk-data-server-" + selected_server)

def server_status(selected_server):
    #print("server_status(" + selected_server + ")")
    status_data = os.popen("systemctl status zk-data-server-" + selected_server).read()
    
    if "Active: active (running)" in status_data:
        status = True
    else:
        status = False
    return status
 
def write_service_file(selected_server):
    print("write_service_file(" + selected_server + ")")
    
    os.system("sudo rm /etc/systemd/system/zk-data-server*")
    
    # https://wiki.debian.org/systemd/Services
    file = "/etc/systemd/system/zk-data-server-" + selected_server + ".service"
    os.system("sudo sh -c \' echo \"[Unit]\" > \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Description=ZK DATA Server - " + selected_server + "\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"After=network.target\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"[Service]\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Type=simple\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Restart=always\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"ExecStart=/usr/bin/zk-data-server " + selected_server + "\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"[Install]\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"WantedBy=multi-user.target\" >> \"" + file + "\"\'")
    
    file = "/etc/systemd/system/zk-data-server-extras-" + selected_server + ".service"
    os.system("sudo sh -c \' echo \"[Unit]\" > \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Description=ZK DATA Server Extras - " + selected_server + "\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"After=network.target\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"[Service]\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Type=simple\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"Restart=always\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"ExecStart=python3 /etc/zk-data-server/Server-extras " + selected_server + "\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"[Install]\" >> \"" + file + "\"\'")
    os.system("sudo sh -c \' echo \"WantedBy=multi-user.target\" >> \"" + file + "\"\'")    
    
    os.system("sudo systemctl daemon-reload && sudo systemctl enable zk-data-server-" + selected_server + ".service")
    os.system("sudo systemctl daemon-reload && sudo systemctl enable zk-data-server-extras-" + selected_server + ".service")
    
def show_version():
    version = open("/etc/zk-data-server/version", "r").read().rstrip()
    print("version: " + version)
    return version
    
    
    
if len(sys.argv) == 1:
    show_select_server()
else:
    if "restart_if_started" in sys.argv:
        server_list = os.listdir("/etc/zk-data-server/data/")
        for server in server_list:
            if server_status(server):
                print("server " + server + " is running, restart it after Update ")
                server_stop(server)
                server_start(server)
    elif "send_log" in sys.argv:
        server_list = os.listdir("/etc/zk-data-server/data/")
        for server in server_list:
            server_send_log(server)
    elif "restart" in sys.argv:
        print("restarting all server")
        server_list = os.listdir("/etc/zk-data-server/data/")
        for server in server_list:
            server_stop(server)
            server_start(server)
            time.sleep(3)
    elif "new" in sys.argv:
        print("create new server")
        show_create_new_server()
    elif "stop" in sys.argv:
        print("stoping all server")
        server_list = os.listdir("/etc/zk-data-server/data/")
        for server in server_list:
            server_stop(server)
            time.sleep(1)
    
# betrag_check = pick.pick(["NEIN", "JA"], "Betrag der aus der Kasse genommen wurde: " + betrag, indicator='-> ')[0]

