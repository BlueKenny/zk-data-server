#!/usr/bin/env python3
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import threading
import os
import ssl
import json
import importlib
import sys
sys.path.append("./functions/")

try: import pick
except:
    os.system("pip3 install --user pick")
    import pick
    
USE_HTTPS = False
if USE_HTTPS:
    if not os.path.exists("key.pem") or not os.path.exists("cert.pem"):
        os.system("openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365")

if len(sys.argv) == 1:  
    title = "ZK-DATA Server \n\n"
    server_list = os.listdir("/etc/zk-data-server/data/")
    if len(server_list) == 1:
        server_name = server_list[0]
    else:
        server_name = pick.pick(server_list, title + "Server auswählen: ", indicator='-> ')[0]
else:  
    server_name = sys.argv[1]
   
server_version = str(open("/etc/zk-data-server/version", "r").read()).rstrip() 
    
data_dir = "/etc/zk-data-server/data/" + server_name + "/"
print("data_dir: " + data_dir)

try:
    SERVER_PORT = libs.BlueFunc.BlueLoad("SERVER_PORT", data_dir)
    SERVER_PORT = int(SERVER_PORT)
except:
    SERVER_PORT = 51515
    
    
class handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print("do_GET()")
        
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        
        message = "get request not implement !"
        self.wfile.write(bytes(message, "utf8"))
                
    def do_POST(self):
        print("do_POST()")
        
        try: mode = str(self.headers["mode"])
        except: mode = ""
        
        headers = self.headers
        headers["server_name"] = server_name
        headers["server_version"] = server_version
        
        if mode == "": # No mode header defined !
            print("mode " + mode + " is not defined")
            self.send_response(400)
            self.send_header('Content-type','text/html')
            self.end_headers()
            message = "No mode header defined !"
            self.wfile.write(bytes(message, "utf8"))
        else: # mode is set
            print("mode: " + mode)
            if os.path.exists("./functions/" + mode + ".py"):
                print("mode: " + mode)
                
                mode_function = importlib.import_module(mode)
                answer = mode_function.run(headers)
                
                self.send_response(200)
                self.send_header('Content-type','text/json')
                self.end_headers()
                
                json_string = json.dumps(answer)
                self.wfile.write(json_string.encode(encoding='utf_8'))
                                
            else: # mode doesn't exists !
                print("mode " + mode + " not found")
                self.send_response(400)
                self.send_header('Content-type','text/html')
                self.end_headers()
                message = "mode doesn't exists !"
                self.wfile.write(bytes(message, "utf8"))
        

class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


def run():
    server = ThreadingSimpleServer(('127.0.0.1', 51515), handler)
    if USE_HTTPS:
        server.socket = ssl.wrap_socket(server.socket, keyfile='./key.pem', certfile='./cert.pem', server_side=True)
        print("Server started with https support")
    else:
        print("Server started with http only")
        
    server.serve_forever()

if __name__ == '__main__':
    run()
