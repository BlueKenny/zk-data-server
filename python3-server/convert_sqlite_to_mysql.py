#!/usr/bin/env python3

from peewee import *
from playhouse.migrate import *
from playhouse.shortcuts import model_to_dict, dict_to_model

from peewee import MySQLDatabase, SqliteDatabase
from playhouse.reflection import Introspector

import pick
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.BlueFunc
import os
        
if len(sys.argv) == 1:
    print("need server name as argument !")
    
    title = "ZK-DATA Server \n\n"
    server_list = os.listdir("/etc/zk-data-server/data/")
    if len(server_list) == 0:
        os.system("zk-data-server-config new")
        server_list = os.listdir("/etc/zk-data-server/data/")
    
    if len(server_list) == 1:
        server_name = server_list[0]
    else:
        server_name = pick.pick(server_list, title + "Server auswählen: ", indicator='-> ')[0]
else:  
    server_name = sys.argv[1]
print("server_name: " + str(server_name))    

data_dir = "/etc/zk-data-server/data/" + server_name + "/"
print("data_dir: " + data_dir)

server_mysql_database = libs.BlueFunc.BlueLoad("SERVER_MYSQL_DATABASE", data_dir)
server_mysql_user = libs.BlueFunc.BlueLoad("SERVER_MYSQL_USER", data_dir)
server_mysql_password = libs.BlueFunc.BlueLoad("SERVER_MYSQL_PASSWORD", data_dir)
server_mysql_ip = libs.BlueFunc.BlueLoad("SERVER_MYSQL_IP", data_dir)
server_mysql_port = int(libs.BlueFunc.BlueLoad("SERVER_MYSQL_PORT", data_dir))

print("server_mysql_database: " + str(server_mysql_database))
print("server_mysql_user: " + str(server_mysql_user))
print("server_mysql_password: " + str(server_mysql_password))
print("server_mysql_ip: " + str(server_mysql_ip))
print("server_mysql_port: " + str(server_mysql_port))

mysql_db = MySQLDatabase(server_mysql_database, user=server_mysql_user, password=server_mysql_password, host=server_mysql_ip, port=server_mysql_port )
mysql_db.connect()     

databases = []
for database in os.listdir(data_dir):
    if ".db" in database:
        databases.append(database)
        
for database_name in databases:
    
    sqlite_db = SqliteDatabase(data_dir + database_name)
    sqlite_db.connect()
        
    print("Starting Convert " + database_name + " from sqlite to mysql database")
        
    introspector = Introspector.from_database(sqlite_db)
    models = introspector.generate_models()
    
    for model in models.values():
        print("model: " + str(model))
        q = model.select()
        
        list_of_objects = []
        for i in q:
            list_of_objects.append(model_to_dict(i))
            
        model._meta.database = mysql_db
    
        mysql_db.create_tables(list(models.values() ))
           
        for a in list_of_objects:    
            if not "pdf" in a:
                obj = dict_to_model(model, a)            
                try:
                    new_entry = model.create(**a)
                except:
                    obj.save()
                         
    print(database_name + " converted")
    
    sqlite_db.close()

####

class Rechnung_e(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    lieferant_id = CharField()
    lieferant_name = CharField()
    datum = CharField()
    dokument_id = CharField()
    dokument_datum = CharField()
    datum_todo = CharField()
    
    total = FloatField()
    total_steuern = FloatField()
    total_htva = FloatField()
    
    ware = FloatField()
    unkosten = FloatField()
    investition = FloatField()
    privat = FloatField()
    
    steuern_privat = FloatField() ## privater anteil von total_steuern
    steuern_abziebar = FloatField() ## Rest von total_steuern

    info = CharField()
    
    rest = FloatField()
    
    created_by = CharField()
        
class Rechnung_e_pdf(Model):
    identification = CharField(primary_key = True)
    pdf = BlobField()
        
rechnung_e_db = SqliteDatabase(data_dir + "rechnung_e.db")
rechnung_e_pdf_db = SqliteDatabase(data_dir + "rechnung_e_pdf.db")

rechnung_e_db.connect()
rechnung_e_pdf_db.connect() 

Rechnung_e._meta.database = rechnung_e_db
Rechnung_e_pdf._meta.database = rechnung_e_pdf_db

for invoice_e in Rechnung_e.select():
    #print("extract invoice_e " + str(invoice_e.identification) + " pdf to /tmp/")

    date_as_list = str(invoice_e.datum).split("-")
    
    path = "/etc/zk-data-server/data/" + server_name + "/files/invoice_e/" + date_as_list[0] + "/" + date_as_list[1] + "/" + date_as_list[2] + "/"
    if not os.path.exists(path): os.system("mkdir -p " + path)
    
    filename = "RE" + str(invoice_e.identification) + ".pdf"
    print(filename)
    
    if not os.path.exists(path + filename):
    
        invoice_e_pdf = Rechnung_e_pdf.get(Rechnung_e_pdf.identification == invoice_e.identification)
        open(path + filename, "wb").write(invoice_e_pdf.pdf)
    
rechnung_e_db.close()
rechnung_e_pdf_db.close()






mysql_db.close()
