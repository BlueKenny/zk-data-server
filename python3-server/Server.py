#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
sys.path.append("/etc/zk-data-libs/")

import os
import datetime
import time
import libs.ServerTools
import libs.BlueFunc
import libs.get_image_online
import libs.barcode
import libs.upload
import libs.RoundUp
import libs.check_vat
import json
import csv
import random
import os.path, time
import requests
from threading import Thread 
import base64

import importlib
import urllib

from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import threading
import socket

import pick
    
BYTE_LENGHT = 25000

if os.path.exists("../test"): debug = True
else: debug = False

from peewee import *
from playhouse.migrate import *
from playhouse.shortcuts import model_to_dict, dict_to_model

import asyncio
import websockets
# apt install python3-websockets

if os.path.exists("/etc/zk-data-ai-helper"):
    ai_helper = True
    sys.path.append("/etc/zk-data-ai-helper/")
    import ai_helper
else: ai_helper = False

USE_HTTPS = False
if USE_HTTPS: import ssl
# openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365

if len(sys.argv) == 1:
    print("need server name as argument !")
    
    title = "ZK-DATA Server \n\n"
    server_list = os.listdir("/etc/zk-data-server/data/")
    if len(server_list) == 0:
        os.system("zk-data-server-config new")
        server_list = os.listdir("/etc/zk-data-server/data/")
    
    if len(server_list) == 1:
        server_name = server_list[0]
    else:
        server_name = pick.pick(server_list, title + "Server auswählen: ", indicator='-> ')[0]
else:  
    server_name = sys.argv[1]
    
    if "debug" in sys.argv: debug = True
    
print("server_name: " + str(server_name))    

data_dir = "/etc/zk-data-server/data/" + server_name + "/"
print("data_dir: " + data_dir)

server_mysql_enabled = libs.BlueFunc.BlueLoad("SERVER_MYSQL_ENABLED", data_dir)
if server_mysql_enabled == "True":
    server_mysql_enabled = True
else:
    server_mysql_enabled = False
libs.BlueFunc.BlueSave("SERVER_MYSQL_ENABLED", server_mysql_enabled, data_dir)
print("server_mysql_enabled: " + str(server_mysql_enabled))

## MySQL DATA
server_mysql_database = libs.BlueFunc.BlueLoad("SERVER_MYSQL_DATABASE", data_dir)
if server_mysql_database == "None":
    server_mysql_database = server_name + "_database"
    libs.BlueFunc.BlueSave("SERVER_MYSQL_DATABASE", server_mysql_database, data_dir)
print("server_mysql_database: " + str(server_mysql_database))
    
server_mysql_user = libs.BlueFunc.BlueLoad("SERVER_MYSQL_USER", data_dir)
if server_mysql_user == "None":
    server_mysql_user = server_name + "_user"
    libs.BlueFunc.BlueSave("SERVER_MYSQL_USER", server_mysql_user, data_dir)
print("server_mysql_user: " + str(server_mysql_user))

server_mysql_password = libs.BlueFunc.BlueLoad("SERVER_MYSQL_PASSWORD", data_dir)
if server_mysql_password == "None":
    server_mysql_password = random.randint(99999999999, 9999999999999999)
    libs.BlueFunc.BlueSave("SERVER_MYSQL_PASSWORD", server_mysql_password, data_dir)
print("server_mysql_password: [" + str(server_mysql_password) + "]")

server_mysql_ip = libs.BlueFunc.BlueLoad("SERVER_MYSQL_IP", data_dir)
if server_mysql_ip == "None":
    server_mysql_ip = "127.0.0.1"
    libs.BlueFunc.BlueSave("SERVER_MYSQL_IP", server_mysql_ip, data_dir)
print("server_mysql_ip: " + str(server_mysql_ip))

try:
    server_mysql_port = int(libs.BlueFunc.BlueLoad("SERVER_MYSQL_PORT", data_dir))
except:
    server_mysql_port = 3306
    libs.BlueFunc.BlueSave("SERVER_MYSQL_PORT", server_mysql_port, data_dir)
print("server_mysql_port: " + str(server_mysql_port))

mysql_db = MySQLDatabase(server_mysql_database, user=server_mysql_user, password=server_mysql_password, host=server_mysql_ip, port=server_mysql_port)
print("start connection to mysql database")
try:
    database = mysql_db.connection()
    print("mysql database connected")
except:
    print("mysql database not connected !!!")
    print("Creating new database")
    os.system("sudo /etc/zk-data-server/mysql.py " + server_name)
    print("print restart server !")
    quit()
#####

if server_mysql_enabled:
    mysql_migrator = MySQLMigrator(mysql_db)
else:
    stock_db = SqliteDatabase(data_dir + "stock.db")
    local_migrator = SqliteMigrator(stock_db)

    user_db = SqliteDatabase(data_dir + "user.db")
    user_migrator = SqliteMigrator(user_db)

    kunde_db = SqliteDatabase(data_dir + "kunde.db")
    kunde_migrator = SqliteMigrator(kunde_db)

    lieferschein_db = SqliteDatabase(data_dir + "lieferschein.db")
    lieferschein_migrator = SqliteMigrator(lieferschein_db)

    inventar_db = SqliteDatabase(data_dir + "inventar.db")
    inventar_migrator = SqliteMigrator(inventar_db)

    rechnung_db = SqliteDatabase(data_dir + "rechnung.db")
    rechnung_migrator = SqliteMigrator(rechnung_db)

    rechnung_e_db = SqliteDatabase(data_dir + "rechnung_e.db")
    rechnung_e_migrator = SqliteMigrator(rechnung_e_db)
    
    konten_db = SqliteDatabase(data_dir + "konten.db")
    konten_migrator = SqliteMigrator(konten_db)

    transaktion_db = SqliteDatabase(data_dir + "transaktion.db")
    transaktion_migrator = SqliteMigrator(transaktion_db)

    stats_db = SqliteDatabase(data_dir + "stats.db")
    stats_migrator = SqliteMigrator(stats_db)

    categorie_db = SqliteDatabase(data_dir + "categorie.db")
    categorie_migrator = SqliteMigrator(categorie_db)

    log_db = SqliteDatabase(data_dir + "log.db")
    log_migrator = SqliteMigrator(log_db)

    chat_db = SqliteDatabase(data_dir + "chat.db")
    chat_migrator = SqliteMigrator(chat_db)

    mwst_db = SqliteDatabase(data_dir + "mwst.db")
    mwst_migrator = SqliteMigrator(mwst_db)

    check_mwst_db = SqliteDatabase(data_dir + "check_mwst.db")
    check_mwst_migrator = SqliteMigrator(check_mwst_db)
    
    delivery_e_db = SqliteDatabase(data_dir + "delivery_e.db")
    delivery_e_migrator = SqliteMigrator(delivery_e_db)
    
    tag_db = SqliteDatabase(data_dir + "tag.db")
    tag_migrator = SqliteMigrator(tag_db)
    tag_position_db = SqliteDatabase(data_dir + "tag_position.db")
    tag_position_migrator = SqliteMigrator(tag_position_db)

FreeID = str(libs.BlueFunc.BlueLoad("FreeID", data_dir))
try: int(FreeID)
except: FreeID = 0

class Tag(Model):
    identification = AutoField()
    tag = CharField()
    sn = CharField()
    alarm = BooleanField()
    name = CharField()
    image = CharField()
    article_identification = IntegerField()

    class Meta: database = tag_db
    
class Tag_position(Model):
    identification = AutoField()
    tag_identification = IntegerField()
    tag = CharField()
    position = CharField()
    date = CharField()
    time = CharField()
    timestamp = CharField()

    class Meta: database = tag_position_db

class Delivery_e(Model):
    identification = AutoField()
    date = CharField()
    date_document = CharField()
    client_identification = IntegerField()
    client_name = CharField()
    lines = CharField()
    status = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = delivery_e_db
        
class Delivery_e_line(Model):
    identification = AutoField()
    delivery_e_identification = IntegerField()
    article_identification = IntegerField()
    article_quantity = FloatField()
    date = CharField()
    user_identification = IntegerField()
    status = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = delivery_e_db
        
class Chat(Model):
    identification = AutoField()
    title = CharField()
    userlist = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = chat_db
        
class Message(Model):
    identification = AutoField()
    chat = IntegerField()
    user = IntegerField()
    text = CharField()
    date = CharField()
    time = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = chat_db
        
class Log(Model):
    identification = AutoField()
    timestamp = CharField()
    date = CharField()
    mode = CharField()
    function = CharField()
    document_id = CharField()
    username = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = log_db
        
class Categorie(Model):
    identification = AutoField()#IntegerField(primary_key = True)
    name = CharField()
    enable = BooleanField()
    tva = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = categorie_db
        
class Mwst(Model):
    identification = FloatField(primary_key = True)
    enable = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = mwst_db
        
class Check_Mwst(Model):
    identification = CharField(primary_key = True)
    status = BooleanField()
    status_text = CharField()
    timestamp = CharField()
    client_identification = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = check_mwst_db

class Stats_Stock(Model):
    identification = AutoField()#IntegerField(primary_key = True)
    date = CharField()
    categorie = CharField()
    wert = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = stats_db

class Stats_WorkProfit(Model):
    identification = AutoField()#IntegerField(primary_key = True)
    date = CharField()
    article = CharField()
    wert = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = stats_db

class Stats_CategorieProfit(Model):
    identification = AutoField()#IntegerField(primary_key = True)
    date = CharField()
    categorie = CharField()
    wert = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = stats_db

class Konten(Model):
    identification = AutoField()#IntegerField(primary_key = True)
    name = CharField()
    nutzung_kasse = BooleanField()
    enable = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = konten_db

class Transaktion(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    datum = CharField()
    uhrzeit = CharField()
    betrag = FloatField()
    user = CharField()
    konto_in = IntegerField()
    konto_out = CharField()
    mitteilung = CharField()
    rechnung = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = transaktion_db

class RechnungVorlage(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    kunde_id = CharField()
    kunde_name = CharField()
    lieferscheine = CharField()
    rechnung = CharField()
    total_htva = FloatField()
    total_ohne_steuern = CharField()
    steuern = CharField()
    total = FloatField()
    lastchange = CharField()
    datum = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = rechnung_db

class Kasse(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    kunde_id = CharField()
    kunde_name = CharField()
    lieferscheine = CharField()
    vorlage = CharField()
    total_htva = FloatField()
    total_ohne_steuern = CharField()
    steuern = CharField()
    total = FloatField()
    created_by = CharField()
    ref = CharField()
    rest = FloatField()
    datum = CharField()
    datum_todo = CharField()
    rappel = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = rechnung_db

class Rechnung(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    kunde_id = CharField()
    kunde_name = CharField()
    lieferscheine = CharField()
    vorlage = CharField()
    total_htva = FloatField()
    total_ohne_steuern = CharField()
    steuern = CharField()
    total = FloatField()
    created_by = CharField()
    ref = CharField()
    rest = FloatField()
    datum = CharField()
    datum_todo = CharField()
    rappel = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = rechnung_db

class Rechnung_e(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    lieferant_id = CharField()
    lieferant_name = CharField()
    datum = CharField()
    dokument_id = CharField()
    dokument_datum = CharField()
    datum_todo = CharField()
    
    total = FloatField()
    total_steuern = FloatField()
    total_htva = FloatField()
    
    ware = FloatField()
    unkosten = FloatField()
    investition = FloatField()
    privat = FloatField()
    
    steuern_privat = FloatField() ## privater anteil von total_steuern
    steuern_abziebar = FloatField() ## Rest von total_steuern

    info = CharField()
    
    rest = FloatField()
    
    created_by = CharField()
    
    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = rechnung_e_db

class Inventar(Model):
    identification = AutoField() #IntegerField(primary_key = True)
    bcode = IntegerField()
    anzahl = FloatField()
    tag = CharField()
    monat = CharField()
    jahr = CharField()
    user = CharField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = inventar_db

class User(Model):
    identification = AutoField(primary_key = True)
    name = CharField() #CharField(primary_key = True)
    color = CharField()
    pin = CharField()
    admin = BooleanField()
    session = CharField()
    secure_key = CharField()
    timeout = IntegerField()
    permission_delivery = IntegerField()
    permission_delivery_e = IntegerField()
    permission_invoice = IntegerField()
    permission_invoice_e = IntegerField()
    permission_article = IntegerField()
    permission_client = IntegerField()
    permission_inventory = IntegerField()
    permission_stats = IntegerField()
    permission_cashbook = IntegerField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = user_db

class Lieferschein(Model):
    identification = AutoField()#CharField(primary_key = True)
    kunde_id = CharField()#default="0")
    kunde_name = CharField()
    datum = CharField()#default=str(Date()))
    datum_todo = CharField()#default=str(Date()))
    lastchange = CharField()
    linien = CharField()#default="")
    anzahl = CharField()#default="")
    bcode = CharField()#default="")
    name = CharField()#default="")
    preis = CharField()#default="")
    fertig = BooleanField()#default=False)
    user = CharField()#default = "")
    total = FloatField()                    # total von Lieferschein inkl MwSt
    info = CharField()#default = "")
    maschine = CharField()
    tva = CharField()#default="")           # MwSt setze pro Linie
    preis_htva = CharField()                # Preise ohne MwSt pro Linie
    steuern = CharField()               # Preise ohne MwSt pro Linie
    total_ohne_steuern = CharField()
    rechnung = CharField()
    total_htva = FloatField()
    angebot = BooleanField()
    bestellung = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = lieferschein_db

class Kunde(Model):
    identification = AutoField()
    name = CharField()
    alias = CharField()
    land = CharField()
    adresse = CharField()
    plz = CharField()
    ort = CharField()
    email1 = CharField()
    email2 = CharField()
    email3 =CharField()
    tel1 = CharField()
    tel2 = CharField()
    tel3 = CharField()
    tel4 = CharField()
    tel1_beschreibung = CharField()
    tel2_beschreibung = CharField()
    tel3_beschreibung = CharField()
    tel4_beschreibung = CharField()
    tva = CharField()
    sprache_de = BooleanField()
    sprache_fr = BooleanField()
    sprache_nl = BooleanField()
    lastchange = CharField()
    creation = CharField()
    info = TextField()
    warning = TextField()
    dealer = BooleanField()
    supplier = BooleanField()
    sepa = BooleanField()
    foto = TextField()
    disabled = BooleanField()
    rappel = FloatField()
    payment_deadline = IntegerField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = kunde_db

class Bewegung(Model):
    identification = AutoField()# CharField(primary_key = True)
    bcode = CharField()#null = False)
    datum = CharField()#null = False, default=str(Date()))
    start = FloatField()#null = False, default=0)
    end = FloatField()#null = False, default=0)
    preisek = FloatField()#null = False, default = 0.0)
    preisvkh = FloatField()#null = False, default = 0.0)
    preisvk = FloatField()#null = False, default = 0.0)
    mode = CharField()#null = False)
    user = CharField()#null = False)

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = stock_db

class Artikel(Model):# All Str are upper()
    identification = AutoField()
    name_de = CharField()
    name_fr = CharField()
    artikel = CharField()
    artikel2 = CharField()
    artikel3 = CharField()
    artikel4 = CharField()
    artikel5 = CharField()
    artikel6 = CharField()
    barcode = CharField()
    lieferant = CharField()#default="")
    preisek = FloatField()#default = 0.0)
    preisvkh = FloatField()#default = 0.0)
    preisvk = FloatField()#default = 0.0)
    promo = FloatField()#default = 0.0)
    anzahl = FloatField()#default = 0.0)
    ort = CharField()#default = "")
    lastchange = CharField()
    creation = CharField()#default=str(Date()))
    minimum = FloatField()#default = 0.0)
    categorie = IntegerField()#default = "")
    bild = CharField()
    image_nextcheck_timestamp = FloatField()
    beschreibung_de = TextField()#default = "")
    beschreibung_fr = TextField()#default = "")
    baujahr = IntegerField()#default = 0)
    web = BooleanField()#default = False)
    farbe = CharField()#default = "")
    groesse = CharField()
    tva = FloatField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = stock_db
             
class Categorie(Model):
    identification = AutoField()# IntegerField(primary_key = True)
    name = CharField()
    enable = BooleanField()

    class Meta:
        if server_mysql_enabled: database = mysql_db
        else: database = categorie_db

if server_mysql_enabled:
    # use mysql
    print("using MySQL database !")
    
    try:mysql_db.connect()
    except: True
    
    mysql_db.create_tables([Log, Categorie, Mwst, Check_Mwst, Stats_Stock, Stats_WorkProfit, Stats_CategorieProfit, Konten, Transaktion, RechnungVorlage, Kasse, Rechnung, Rechnung_e, Inventar, User, Lieferschein, Kunde, Bewegung, Artikel, Categorie, Chat, Message])
    
    #mysql_db.execute_sql('ALTER TABLE lieferschein AUTO_INCREMENT = 0;')
    mysql_db.execute_sql('ALTER TABLE kunde AUTO_INCREMENT = 0;')
    mysql_db.execute_sql('ALTER TABLE categorie AUTO_INCREMENT = 0;')
    
    migrate(mysql_migrator.alter_column_type("artikel", "barcode", CharField() ))
    
    query = Kunde.select().where(Kunde.identification == 0)
    if not query.exists():
        new_client = Kunde.create(name="BARKUNDE", alias="", land="BE", adresse="", plz="0", ort="", email1="", email2="", email3="", tel1="", tel2="", tel3="", tel4="", tel1_beschreibung="", tel2_beschreibung="", tel3_beschreibung="", tel4_beschreibung="", tva="", sprache_de=False, sprache_fr=False, sprache_nl=False, lastchange="0", creation=str(libs.BlueFunc.Date()), info="", warning="", dealer=False, supplier=False, disabled=False, foto="", rappel=0.0, sepa=False, payment_deadline=15)
        mysql_db.execute_sql('UPDATE kunde SET identification = 0 WHERE identification = ' + str(new_client.identification) + ';')
        
    query = Categorie.select().where(Categorie.identification == 0)
    if not query.exists():
        new_cat = Categorie.create(name = "Teile", enable = True)
        mysql_db.execute_sql('UPDATE categorie SET identification = 0 WHERE identification = ' + str(new_cat.identification) + ';')
        
    query = Categorie.select().where(Categorie.identification == 1)
    if not query.exists():
        new_cat = Categorie.create(name = "Arbeit", enable = True)
        mysql_db.execute_sql('UPDATE categorie SET identification = 1 WHERE identification = ' + str(new_cat.identification) + ';')
        
    query = Mwst.select()
    if len(Mwst.select()) == 0:
        Mwst.create(identification = 0.0, enable = True)
        Mwst.create(identification = 6.0, enable = True)
        Mwst.create(identification = 7.0, enable = False)
        Mwst.create(identification = 12.5, enable = False)
        Mwst.create(identification = 19.0, enable = False)
        Mwst.create(identification = 20.0, enable = False)
        Mwst.create(identification = 21.0, enable = True)
        
else:
    # use sqlite
    print("using sqlite database !")
    
    delivery_e_db.connect()
    try: delivery_e_db.create_tables([Delivery_e])
    except: True
    try: delivery_e_db.create_tables([Delivery_e_line])
    except: True
    delivery_e_db.close()  
    
    chat_db.connect()
    try: chat_db.create_tables([Chat])
    except: True
    try: chat_db.create_tables([Message])
    except: True
    chat_db.close()  
    
    tag_db.connect()
    try: tag_db.create_tables([Tag])
    except: True
    tag_db.close()
    
    tag_position_db.connect()
    try: tag_position_db.create_tables([Tag_position])
    except: True
    tag_position_db.close()
    
    categorie_db.connect()
    try:
        categorie_db.create_tables([Categorie])
        new_cat = Categorie.create(identification = 0, name = "Teile", enable = True)
        new_cat = Categorie.create(identification = 1, name = "Arbeit", enable = True)
    except: True
    try: migrate(local_migrator.add_column("Categorie", "tva", FloatField(21.0)))
    except: True
    categorie_db.close()  

    stats_db.connect()
    try: stats_db.create_tables([Stats_Stock])
    except: True
    try: stats_db.create_tables([Stats_WorkProfit])
    except: True
    try: stats_db.create_tables([Stats_CategorieProfit])
    except: True
    stats_db.close()   

    log_db.connect()
    try: log_db.create_tables([Log])
    except: True
    log_db.close()    

    mwst_db.connect()
    try: mwst_db.create_tables([Mwst])
    except: True
    mwst_db.close()    
    if len(Mwst.select()) == 0:
        Mwst.create(identification = 0.0, enable = True)
        Mwst.create(identification = 6.0, enable = True)
        Mwst.create(identification = 7.0, enable = False)
        Mwst.create(identification = 12.5, enable = False)
        Mwst.create(identification = 19.0, enable = False)
        Mwst.create(identification = 21.0, enable = True)

    check_mwst_db.connect()
    try: check_mwst_db.create_tables([Check_Mwst])
    except: True
    try: migrate(check_mwst_migrator.add_column("Check_Mwst", "client_identification", CharField(default="")))
    except: True
    check_mwst_db.close() 

    try:stock_db.connect()
    except: True
    try: stock_db.create_tables([Artikel])
    except: True
    try: migrate(local_migrator.add_column("Artikel", "groesse", CharField()))
    except: True
    try: migrate(local_migrator.add_column("Artikel", "promo", FloatField(default="1.0")))
    except: True
    try: migrate(local_migrator.add_column("Artikel", "tva", FloatField(default="21.0")))
    except: True
    try: migrate(local_migrator.drop_column("Artikel", "preisversand"))
    except: True
    try: migrate(local_migrator.add_column("Artikel", "artikel5", CharField(default="")))
    except: True
    try: migrate(local_migrator.add_column("Artikel", "artikel6", CharField(default="")))
    except: True
    try: migrate(local_migrator.alter_column_type("Artikel", "barcode", CharField()))
    except: True
    try: migrate(local_migrator.add_column("Artikel", "image_nextcheck_timestamp", FloatField(default="0.0")))
    except: True
    stock_db.close()

    user_db.connect()
    try: user_db.create_tables([User])
    except: True
    try: migrate(user_migrator.alter_column_type("User", "name", CharField()))
    except: True
    try: migrate(user_migrator.add_column("User", "identification", IntegerField(default=0)))
    except: True
    try: migrate(user_migrator.alter_column_type("User", "identification", AutoField()))
    except: True        
    try: migrate(user_migrator.add_column("User", "color", CharField(default="red")))
    except: True
    try: migrate(user_migrator.add_column("User", "pin", CharField(default="1234")))
    except: True
    try: migrate(user_migrator.drop_column("User", "sign"))
    except: True
    try: migrate(user_migrator.add_column("User", "admin", BooleanField(default=False)))
    except: True
    try: migrate(user_migrator.add_column("User", "session", CharField(default="")))
    except: True
    try: migrate(user_migrator.add_column("User", "secure_key", CharField(default="None")))
    except: True
    try: migrate(user_migrator.add_column("User", "timeout", IntegerField(default=3600)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_delivery", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_invoice", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_invoice_e", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_article", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_client", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_inventory", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_stats", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_cashbook", IntegerField(default=2)))
    except: True
    try: migrate(user_migrator.add_column("User", "permission_delivery_e", IntegerField(default=2)))
    except: True
    user_db.close()

    inventar_db.connect()
    try: inventar_db.create_tables([Inventar])
    except: True
    try: migrate(inventar_migrator.add_column("Inventar", "user", CharField(default="NONAME")))
    except: True
    try: migrate(inventar_migrator.drop_column("Inventar", "preisekp"))
    except: True
    inventar_db.close()

    kunde_db.connect()
    try: kunde_db.create_tables([Kunde])
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "land", CharField()))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "info", TextField()))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "warning", TextField(default="")))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "foto", TextField(default="")))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "alias", CharField(default="")))
    except: True

    try: migrate(kunde_migrator.drop_column("Kunde", "tel1_breschreibung"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "tel2_breschreibung"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "tel3_breschreibung"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "tel4_breschreibung"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "solde_2015"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "solde_2016"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "solde_2017"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "solde_2018"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "credit"))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "debit"))
    except: True

    try:
        migrate(kunde_migrator.alter_column_type('Kunde', 'tel1', TextField()))
        migrate(kunde_migrator.alter_column_type('Kunde', 'tel2', TextField()))
        migrate(kunde_migrator.alter_column_type('Kunde', 'tel3', TextField()))
    except: True
    try: migrate(kunde_migrator.add_column('Kunde', 'tel4', TextField(default="")))
    except: True
    try:
        migrate(kunde_migrator.add_column('Kunde', 'tel1_beschreibung', TextField(default="")))
        migrate(kunde_migrator.add_column('Kunde', 'tel2_beschreibung', TextField(default="")))
        migrate(kunde_migrator.add_column('Kunde', 'tel3_beschreibung', TextField(default="")))
        migrate(kunde_migrator.add_column('Kunde', 'tel4_beschreibung', TextField(default="")))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "dealer", BooleanField(default=False)))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "supplier", BooleanField(default=False)))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "sepa", BooleanField(default=False)))
    except: True
    try: migrate(kunde_migrator.drop_column("Kunde", "invoice"))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "disabled", BooleanField(default=False)))
    except: True
    try: migrate(kunde_migrator.add_column("Kunde", "rappel", FloatField(default=0.0)))
    except: True
    try:
        migrate(kunde_migrator.alter_column_type("Kunde", "identification", IntegerField()))
        migrate(kunde_migrator.alter_column_type("Kunde", "identification", AutoField()))
    except: True
    try: migrate(kunde_migrator.add_column('Kunde', 'payment_deadline', IntegerField(default=15)))
    except: True

    kunde_db.close()

    lieferschein_db.connect()
    try: lieferschein_db.create_tables([Lieferschein])
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "fertig", BooleanField(default="0")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "user", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "total", FloatField(default="0.0")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "kunde_name", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "info", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "maschine", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "datum_todo", CharField(default="2000-01-01")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "tva", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "preis_htva", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "steuern", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "rechnung", CharField(default="")))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "total_htva", FloatField(default=0.0)))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "angebot", BooleanField(default=False)))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "bestellung", BooleanField(default=False)))
    except: True
    try: migrate(lieferschein_migrator.add_column("Lieferschein", "total_ohne_steuern", CharField(default="0;0.0")))
    except: True
    try:
        migrate(lieferschein_migrator.alter_column_type("Lieferschein", "identification", IntegerField()))
        migrate(lieferschein_migrator.alter_column_type("Lieferschein", "identification", AutoField()))
    except: True
    lieferschein_db.close()

    stock_db.connect()
    try: stock_db.create_tables([Bewegung])
    except: True
    try: migrate(local_migrator.alter_column_type("Bewegung", "identification", AutoField()))
    except: True
    stock_db.close()

    rechnung_db.connect()
    try: rechnung_db.create_tables([RechnungVorlage])
    except: True
    try: rechnung_db.create_tables([Kasse])
    except: True
    try: rechnung_db.create_tables([Rechnung])
    except: True

    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "rechnung", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "lastchange", CharField(default="0")))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "total_htva", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "datum", CharField(default=Date())))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "total", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "steuern", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "total_ohne_steuern", CharField(default="0;0.0")))
    except: True
    try: migrate(rechnung_migrator.add_column("RechnungVorlage", "kunde_name", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "vorlage", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "created_by", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "ref", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "rest", FloatField(default=9999999)))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "total_htva", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "datum", CharField(default=Date())))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "datum_todo", CharField(default=TodayAddDays(30) )))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "total", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "steuern", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "total_ohne_steuern", CharField(default="0;0.0")))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "kunde_name", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "vorlage", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "created_by", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "ref", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "rest", FloatField(default=9999999)))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "total_htva", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "datum", CharField(default=Date())))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "datum_todo", CharField(default=TodayAddDays(30) )))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "total", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "steuern", CharField(default="")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "total_ohne_steuern", CharField(default="0;0.0")))
    except: True
    try: migrate(rechnung_migrator.add_column("Rechnung", "kunde_name", CharField(default="")))
    except: True

    try: migrate(rechnung_migrator.add_column("Rechnung", "rappel", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_migrator.add_column("Kasse", "rappel", FloatField(default=0.0)))
    except: True

    try: rechnung_db.close()
    except: True

    try: konten_db.connect()
    except: True
    
    try:
        konten_db.create_tables([Konten])        
        names = ["Barzahlung", "Bancontact", "Ausgleich / Gratis"]
        for identification in range(1, 4):
            query = Konten.select().where(Konten.identification == identification)
            if not query.exists():
                k = Konten.create(name = names[identification], nutzung_kasse = 1)                   
                k.save()     
    except: True
    try: migrate(konten_migrator.add_column("Konten", "enable", BooleanField(default=1)))
    except: True
    konten_db.close()

    try: transaktion_db.connect()
    except: True
    try: transaktion_db.create_tables([Transaktion])
    except: True
    try: migrate(transaktion_migrator.add_column("Transaktion", "datum", CharField(default="")))
    except: True
    try: transaktion_db.close()
    except: True

    rechnung_e_db.connect()
    try: rechnung_e_db.create_tables([Rechnung_e])
    except: True
    try: migrate(rechnung_e_migrator.add_column("Rechnung_e", "dokument_datum", CharField(default="")))
    except: True
    try: migrate(rechnung_e_migrator.add_column("Rechnung_e", "rest", FloatField(default=0.0)))
    except: True
    try: migrate(rechnung_e_migrator.add_column("Rechnung_e", "lieferant_name", CharField(default="")))
    except: True
    try: migrate(rechnung_e_migrator.add_column("Rechnung_e", "datum_todo", CharField(default="")))
    except: True
    try: migrate(rechnung_e_migrator.add_column("Rechnung_e", "created_by", CharField(default="")))
    except: True
    try: rechnung_e_db.close()
    except: True
    
server_version = str(open("/etc/zk-data-server/version", "r").read()).rstrip()
    
if os.path.exists("/tmp/" + server_name): os.system("rm -f -r /tmp/" + server_name)
os.system("mkdir /tmp/" + server_name + " && chmod 777 /tmp/" + server_name)

# import promo modules
promo_modules = {}
if not os.path.exists("/etc/zk-data-server/data/" + server_name + "/promo/enabled"):
    os.system("cp -r /etc/zk-data-server/default_files/promo /etc/zk-data-server/data/" + server_name + "/")
    os.system("chmod -R 777 /etc/zk-data-server/data/" + server_name + "/promo")
sys.path.append("/etc/zk-data-server/data/" + server_name + "/promo/enabled/")
for promo_file in os.listdir("/etc/zk-data-server/data/" + server_name + "/promo/enabled/"):
    if ".py" in promo_file:
        print("loading promo_module: " + promo_file)
        promo_modules[promo_file.replace(".py", "")] = importlib.import_module(promo_file.replace(".py", ""))

def get_all_mwst():
    print("get_all_mwst()")
    mwst_liste = []
    query = Mwst.select().order_by(Mwst.identification)
    for mwst in query:
        mwst_liste.append(str(float(mwst.identification)))
    return mwst_liste

def get_mwst():
    print("get_mwst()")
    mwst_liste = []
    query = Mwst.select().where(Mwst.enable == True).order_by(Mwst.identification)
    for mwst in query:
        mwst_liste.append(str(float(mwst.identification)))
    return mwst_liste
    
try: standart_tva = float(libs.BlueFunc.BlueLoad("standart_tva", data_dir))
except: standart_tva = 999.0
if not str(standart_tva) in get_mwst():
    print("tva " + str(standart_tva) + " not in " + str(get_mwst()))
    standart_tva = get_mwst()[-1]
    libs.BlueFunc.BlueSave("standart_tva", standart_tva, data_dir)


def search_tag(Dict):
    print("search_tag(" + str(Dict) + ")")
    
    where_condition = []
    Antwort = []
    
    try: tag_db.connect()
    except: True
    
    if "limit" in Dict:
        try: IndexLimit = int(Dict["limit"])
        except: IndexLimit = 250
    else:
        IndexLimit = 250

    if "identification" in Dict:
        try:
            Dict["identification"] = int(Dict["identification"])
            where_condition.append(Tag.identification == Dict["identification"])
        except: True
        
    if "tag" in Dict:
        if not str(Dict["tag"]) == "":
            Dict["tag"] = str(Dict["tag"]).upper()
            where_condition.append(Tag.tag == Dict["tag"])

    if "sn" in Dict:
        Dict["sn"] = str(Dict["sn"]).upper()
        where_condition.append(Tag.sn.contains(Dict["sn"]))
        
    if "alarm" in Dict:
        try:
            Dict["alarm"] = bool(Dict["alarm"])
            where_condition.append(Tag.alarm == Dict["alarm"])
        except: True
        
    if "name" in Dict:
        Dict["name"] = str(Dict["name"]).upper()
        where_condition.append(Tag.name.contains(Dict["name"]))
        
    if "article_identification" in Dict:
        try:
            Dict["article_identification"] = int(Dict["article_identification"])
            where_condition.append(Tag.article_identification == Dict["article_identification"])
        except: True
    
    NichtSuchen = False
    if len(where_condition) == 0: NichtSuchen = True
    
    while True:
        if NichtSuchen: break
        Count = 1
        for this_tag in Tag.select().where(*where_condition).order_by(Tag.tag):
            Antwort.append(model_to_dict(this_tag))  #(str(tag.identification))
            if Count == IndexLimit: break
            else: Count += 1
        break

    try: tag_db.close()
    except: True
    
    return Antwort
    
def set_tag_position(tag, position, timestamp):
    print("set_tag_position(" + str(tag) + ", " + str(position) + ", " + str(timestamp) + ")")

    tag = str(tag).upper()
    position = str(position).upper()
    try: timestamp = str(float(timestamp))
    except: timestamp = str(float(libs.BlueFunc.Timestamp()))

    try: tag_db.connect()
    except: True
    # find tag_identification
    query_for_tag = Tag.select().where(Tag.tag == tag)
    if query_for_tag.exists():
        print("Existing Tag")
        tag_identification = query_for_tag[0].identification
    else:
        print("New Tag")
        new_tag = Tag.create(tag = tag, sn = "", alarm = True, name = "", image = "", article_identification = 0)
        new_tag.save()
        tag_identification = new_tag.identification
    print("tag_identification: " + str(tag_identification))
    try: tag_db.close()
    except: True
    
    try: tag_position_db.connect()
    except: True
    
    date_and_hours = str(libs.BlueFunc.timestamp_to_date_and_hours(float(timestamp)))
    date = date_and_hours.split(" ")[0]
    time = date_and_hours.split(" ")[1]
    print("date_and_hours: " + str(date_and_hours))
    query_for_tag_position = Tag_position.select().where(Tag_position.tag == tag, Tag_position.position == position, Tag_position.date == date, Tag_position.time == time)
    if query_for_tag_position.exists():
        print("Position exist already")
    else:
        new_tag_position = Tag_position.create(tag_identification = tag_identification, tag = tag, position = position, date = date, time = time, timestamp = str(timestamp))
        new_tag_position.save()
        
    try: tag_position_db.close()
    except: True

def pre_printing_documents():
    print("pre_printing_documents()")
    
    invoices = []
    try: rechnung_db.connect()
    except: True
    query_ini = Rechnung.select()
    if query_ini.exists():
        for q in query_ini: invoices.append([q.datum, "R" + str(q)])
    query_ini = Kasse.select()
    if query_ini.exists():
        for q in query_ini: invoices.append([q.datum, "K" + str(q)])
    query_ini = RechnungVorlage.select()
    if query_ini.exists():
        for q in query_ini: invoices.append([q.datum, "RV" + str(q)])
    try: rechnung_db.close()
    except: True
    
    deliverys = []
    try: lieferschein_db.connect()
    except: True
    query_ini = Lieferschein.select()
    if query_ini.exists():
        for q in query_ini: deliverys.append([q.datum, "L" + str(q)])
    try: lieferschein_db.close()
    except: True
    
    invoices_e = []
    try: invoice_e_db.connect()
    except: True
    query_ini = Rechnung_e.select()
    if query_ini.exists():
        for q in query_ini: invoices_e.append([q.datum, "RE" + str(q)])
    try: invoice_e_db.close()
    except: True
    
    ## Cashbook
    date = get_cashbook_date_after(1, "2020-01-01")
    print("date: " + str(date))
    last_date = get_cashbook_date_last(1)
    while True:
        date_list = date.split("-")
        new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/cashbook/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/0_" + str(date) + ".pdf"
        if not os.path.exists(new_pdf_path):
            pdf_url = get_cashbook_pdf(1, date)
    
        date = get_cashbook_date_after(1, date)
        if date == last_date: break
    
    # <TODO>
    
def get_username_from_key(secure_key):
    try: user_db.connect()
    except: True
    
    query = User.select().where(User.secure_key == secure_key)
    if query.exists():
        user_dict = model_to_dict(User.get(secure_key=secure_key))
        username = str(user_dict["name"])
    else:
        username = ""
        
    try: user_db.close()
    except: True
    
    return username

def get_logs(document_type, document_id):
    print("get_logs(" + str(document_type) + ", " + str(document_id) + ")")
    
    document_type = str(document_type).lower()
    document_id = str(document_id).lower()
    
    try: log_db.connect()
    except: True
    
    link = str(Timestamp()) + ".csv"
    file = "/var/www/html/zk-data/file/files/" + link
    
    logs = Log.select().where(Log.mode == document_type).where(Log.document_id == document_id)
    datalines = []
    
    for log in logs:
        datalines.append(str(log.identification) + ";" + str(log.date) + ";" + str(log.function).replace("\n", " -> ") + ";" + str(log.username))
    
    open(file, "w").write("\n".join(datalines))
    
    try: log_db.close()
    except: True
    
    return "/zk-data/file/files/" + link
    
    
def set_log(mode, function, document_id, secure_key):
    print("set_log(" + str(mode) + ", " + str(function) + ", " + str(document_id) + ", " + str(secure_key) + ")")
    
    try: log_db.connect()
    except: True
    
    log = Log.create(   timestamp = Timestamp(),
                        date = Date(),
                        mode = str(mode),
                        function = str(function),
                        document_id = str(document_id),
                        username = get_username_from_key(secure_key)
                    )
                
    log.save() 
    
    try: log_db.close()
    except: True
    
    
def check_vat(vatnumber, client_identification):
    print("check_vat(" + str(vatnumber) + ", " + str(client_identification) + ")")
    
    try: check_mwst_db.connect()
    except: True
        
    vatnumber = str(vatnumber).upper().rstrip().replace(".", "").replace(" ", "").replace(",", "").replace(" ", "")
    query = Check_Mwst.select().where(Check_Mwst.identification == vatnumber)
    if query.exists():
        print("Check_Mwst exists")
        timestamp = time.time()
        print("Check_Mwst timestamp: " + str(timestamp))
        if str(query[0].timestamp) > str(timestamp): # Valid
            answer = model_to_dict(query[0])               
        else: # Data are to old
            answer = libs.check_vat.check_tva_online(vatnumber)            
            query[0].status = answer["status"]
            query[0].status_text = answer["status_text"]
            query[0].client_identification = str(client_identification)
            query[0].timestamp = answer["timestamp"]
            query[0].save()
    else: # No Entry in Database
        print("Check_Mwst don't exist")
        answer = libs.check_vat.check_tva_online(vatnumber)          
        new_entry = Check_Mwst.create(
                                    identification = answer["identification"],
                                    status = answer["status"],
                                    status_text = answer["status_text"],
                                    client_identification = str(client_identification),
                                    timestamp = answer["timestamp"]
                                    )
        new_entry.save()
    
    try: check_mwst_db.close()
    except: True
    
    print("vat checked")
    
    return answer

def check_all_vat():
    print("check_all_vat()")
    
    try: check_mwst_db.connect()
    except: True
    
    timestamp = time.time()
    
    dict_of_old_entrys = {}
    query = Check_Mwst.select().where(Check_Mwst.timestamp < timestamp)
    if query.exists():
        for entry in query:
            dict_of_old_entrys[str(entry.client_identification)] = entry.identification
            
    try: check_mwst_db.close()
    except: True
    
    print("found " + str(len(dict_of_old_entrys)) + " old entries")
       
    if len(dict_of_old_entrys) > 0:
        #for tva in dict_of_old_entrys:
        client_identification = "0"
        tva = dict_of_old_entrys[client_identification]
        tva_status = check_vat(tva, client_identification)
        print("tva_status: " + str(tva_status))
    
        #TODO Check CLIENTS !!!
    
    return True

def get_transaktion(identification): # get Transaktion, return Dict
    print("get_transaktion(" + str(identification) + ")")

    identification = str(identification)

    if not identification.rstrip() == "":
        try: transaktion_db.connect()
        except: True

        query = Transaktion.select().where(Transaktion.identification == identification)

        if query.exists():
            answer_dict = Transaktion.get(Transaktion.identification == identification)
            answer_dict = model_to_dict(answer_dict)

            ## add konto name
            answer_dict["konto_name"] = get_konto(int(answer_dict["konto_in"]) - 1)["name"]
        else:
            return -1

        try: transaktion_db.close()
        except: True

        return answer_dict
    else: return -1


def create_transaktion(konto, betrag, rechnung, pin_code):# return Bool of sucess
    print("create_transaktion(" + str(konto) + ", " + str(betrag) + ", " + str(rechnung) + ", " + str(pin_code) + ")")
    
    konto = int(konto)
    betrag = float(betrag)
    rechnung = str(rechnung)
    pin_code = str(pin_code).upper()
    
    try: user_db.connect()
    except: True
    try: konten_db.connect()
    except: True
    try: rechnung_db.connect()
    except: True
    try: rechnung_e_db.connect()
    except: True
    try: transaktion_db.connect()
    except: True

    query_user = User.select().where(User.pin == pin_code)
    if query_user.exists():
        user = query_user[0].name

        query_konto = Konten.select().where(Konten.identification == konto)
        if query_konto.exists():

            if "RE" in rechnung:
                print("RE in Rechnung")
                query = Rechnung_e.select().where(Rechnung_e.identification == rechnung.replace("RE", ""))
            else:
                if "R" in rechnung:    
                    print("R in Rechnung")            
                    query = Rechnung.select().where(Rechnung.identification == rechnung.replace("R", ""))
                    if query.exists():
                        query[0].datum_todo = TodayAddDays(20)
                        query[0].save()
                        
                        delete_invoice_pdf(rechnung)                     
                else:
                    if "K" in rechnung:
                        print("K in Rechnung")
                        query = Kasse.select().where(Kasse.identification == rechnung.replace("K", ""))
                        if query.exists():
                            query[0].datum_todo = TodayAddDays(20)
                            query[0].save()
                            
                            delete_invoice_pdf(rechnung)    
                    else:
                        print("Error " + rechnung + " not valid for create_transaktion")
                        return False

            if query.exists():
                transaktion = Transaktion.create(
                                        datum = Date(),
                                        uhrzeit = time.strftime("%H:%M"),
                                        betrag = betrag,
                                        user = user,
                                        konto_in = konto,
                                        konto_out = "",
                                        mitteilung = "",
                                        rechnung = rechnung
                                    )
                transaktion.save()                
            else:
                print("ERROR " + rechnung + " not found !")

    else:
        print("ERROR There is no user with this pin !")
        return False


    set_rechnung_rest(rechnung)
    
    try: user_db.close()
    except: True
    try: konten_db.close()
    except: True
    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    try: transaktion_db.close()
    except: True

    print("Sucess")
    return True

def search_transaktion_by_date(datum, index): # search Transaktion by Date, return answer bey index
    print("search_transaktion_by_date(" + str(datum) + ", " + str(index) + ")")

    datum = str(datum)
    
    try: index = int(index)
    except: index = 0

    try: transaktion_db.connect()
    except: True

    query = Transaktion.select().where(Transaktion.datum.contains(datum))

    if query.exists():
        try:
            answer = query[index]        
            answer = model_to_dict(answer)
            
        except: answer = -1
    else:
        answer = -1    
   
    try: transaktion_db.close()
    except: True

    return answer

def search_transaktion_by_rechnung(rechnung): # search Transaktion by Rechnung_id, return list of transaktion ids
    print("search_transaktion_by_rechnung(" + str(rechnung) + ")")

    rechnung = str(rechnung)

    try: transaktion_db.connect()
    except: True

    query = Transaktion.select().where(Transaktion.rechnung == rechnung)

    answer_list = []
    if query.exists():
        for trans in query:
            answer_list.append(str(trans.identification))
   
    try: transaktion_db.close()
    except: True

    answer_list = "|".join(answer_list)

    print("answer_list: " + answer_list)
    return answer_list


def get_konto(index):# get List of kontos and return the item with this index, -1 if not existing
    print("get_konto(" + str(index) + ")")

    index = int(index)

    try: konten_db.connect()
    except: True

    alle_konten = Konten.select()
    
    try:
        antwort = alle_konten[index]
        antwort = model_to_dict(antwort)

    except: antwort = -1

    try: konten_db.close()
    except: True

    return antwort

def create_transaktion_free(datum, uhrzeit, betrag, username, konto_in, konto_out, mitteilung):# return 1 on sucess, 0 if exists and -1 on error
    print("create_transaktion_free(" + str(datum) + ", " + str(uhrzeit) + ", " + str(betrag) + ", " + str(username) + ", " + str(konto_in) + ", " + str(konto_out) + ", " + str(mitteilung) + ")")

    try: transaktion_db.connect()
    except: True
    try: konten_db.connect()
    except: True
    try: rechnung_db.connect()
    except: True

    datum = datum.replace("/", "-")
    if not "-" in datum:
        print("Datum im falschen format: " + str(datum)) 
        return -1
    else:
        if not len(datum.split("-")) == 3:
            print("Datum im falschen format: " + str(datum)) 
            return -1

    #print("len: " + str(len(datum.split("-")[-1])))
    if len(datum.split("-")[-1]) == 4:
        datum = datum.split("-")[2] + "-" + datum.split("-")[1] + "-" + datum.split("-")[0]
        print("changed datum to: " + str(datum))
        
    uhrzeit = str(uhrzeit)
    if uhrzeit == "": uhrzeit = "00:00"         
    
    try:
        betrag = str(betrag)
        if "." in betrag and "," in betrag:
            betrag = betrag.replace(".", "")

        betrag = betrag.replace(",", ".")
        betrag = float(betrag)
    except: 
        print("Betrag im falschen format: " + str(betrag))
        return -1

    try:
        konto_in = int(konto_in)
        query = Konten.select().where(Konten.identification == konto_in)
        if not query.exists():
            print("konto_in existiert nicht: " + str(konto_in))
            return -1
    except:
        print("Fehler beim prüfen vom Konto_in: " + str(konto_in))
        return -1

    konto_out = str(konto_out)
    mitteilung = str(mitteilung).rstrip().replace("\n", "").replace("\r", "").replace('"', "").replace("'", "")

    rechnung = ""

    query = Transaktion.select().where(Transaktion.konto_in == konto_in)
    query = query.where(Transaktion.datum == datum)
    if not query.exists(): print("datum ist anders")
    query = query.where(Transaktion.uhrzeit == uhrzeit)
    if not query.exists(): print("uhrzeit ist anders")
    query = query.where(Transaktion.betrag == betrag)
    if not query.exists(): print("betrag ist anders")
    query = query.where(Transaktion.konto_out == konto_out)
    if not query.exists(): print("konto_out ist anders")
    query = query.where(Transaktion.mitteilung.contains(mitteilung))
    if not query.exists(): print("mitteilung ist anders")

    if query.exists(): 
        print("Transaktion existiert schon")
        return 0
    else:
        query = Rechnung.select().where(Rechnung.rest != 0.0)
        if query.exists():
            query = query.where(Rechnung.ref == "+++" + mitteilung + "+++")
            if query.exists():
                rechnung = "R" + str(query[0].identification)
                mitteilung = "+++" + mitteilung + "+++"
            else:
                if "Communication:" in mitteilung and "***" in mitteilung:
                    test_ref_part = mitteilung.split("***")[1].rstrip()
                    print("test_ref_part: " + str(test_ref_part))
                    
                    query = Rechnung.select().where(Rechnung.rest != 0.0)
                    query = query.where(Rechnung.ref == "+++" + test_ref_part + "+++")
                    if query.exists():
                        rechnung = "R" + str(query[0].identification)
                        mitteilung = "+++" + test_ref_part + "+++"
                print("rechnung: " + str(rechnung) )
        else:
            query = Kasse.select().where(Kasse.rest != 0.0)
            if query.exists():
                query = query.where(Kasse.ref == mitteilung)
                if query.exists():
                    rechnung = "K" + str(query[0].identification)

        neue_transaktion = Transaktion.create( datum = datum,
                            uhrzeit = uhrzeit,
                            betrag = betrag,
                            user = username,
                            konto_in = konto_in,
                            konto_out = konto_out,
                            mitteilung = mitteilung,
                            rechnung = rechnung
                        )
        neue_transaktion.save()
        
        if not rechnung == "":
            set_rechnung_rest(rechnung)

    try: transaktion_db.close()
    except: True
    try: konten_db.close()
    except: True
    try: rechnung_db.close()
    except: True

    return 1

def create_invoice_e(lieferant_id, datum, dokument_id, total, pdf_link, secure_key):
    print("create_invoice_e(" + str(lieferant_id) + ", " + str(datum) + ", " + str(dokument_id) + ", " + str(total) + ", " + str(pdf_link) + ", " + str(secure_key) + ")")
    try: rechnung_e_db.connect()
    except: True
    try: kunde_db.connect()
    except: True
    try: user_db.connect()
    except: True
    
    lieferant_id = str(lieferant_id).replace(".0", "")
    datum = str(datum)
    dokument_id = str(dokument_id)
    try: total = float(total)
    except: total = 0.0
    
    pdf_link = "/tmp/" + pdf_link
    secure_key = str(secure_key)
       
    query_user = User.select().where(User.secure_key == secure_key)
    if query_user.exists():
        created_by = str(query_user[0].name).upper()
              
        if not os.path.exists(pdf_link): 
            print("PDF datei existiert nicht !")
            print(str(pdf_link) + " nicht gefunden !")
            return {}
        
        query = Kunde.select().where(Kunde.identification == str(lieferant_id))
        if not query.exists(): 
            print("Lieferant existiert nicht...")
            return {}
        
        lieferant_name = query[0].name
        
        if "-" in datum:
            datum_list = datum.split("-")
            if len(datum_list) == 3:
                datum_year = datum_list[0]
                datum_month = datum_list[1]
                datum_day = datum_list[2]
                
                if not len(datum_year) == 4: datum_year = Date().split("-")[0]
                if not len(datum_month) == 2: datum_month = Date().split("-")[1]
                if not len(datum_day) == 2: datum_day = Date().split("-")[2]
                
                datum = datum_year + "-" + datum_month + "-" + datum_day
            else:
                datum = libs.BlueFunc.Date()
        else:
            datum = libs.BlueFunc.Date()
        
        print("Rechnung wird erstelt")
        rechnung = Rechnung_e.create(
                lieferant_id = lieferant_id,
                lieferant_name = lieferant_name,
                dokument_datum = datum,
                datum = libs.BlueFunc.Date(),
                datum_todo = libs.BlueFunc.Date(),
                dokument_id = dokument_id,
                total = total,
                total_steuern = 0.0,
                total_htva = 0.0,
                ware = 0.0,
                unkosten = 0.0,
                investition = 0.0,
                privat = 0.0,
                steuern_privat = 0.0,
                steuern_abziebar = 0.0,
                info = "",
                created_by = created_by,
                rest = total
            )
        rechnung.save()
        print("Rechnung gespeichert")

        print("PDF wird erstelt")
        date_list = libs.BlueFunc.Date().split("-")
        new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/invoice_e/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
        if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
        os.system("mv " + pdf_link + " " + new_pdf_path + "RE" + str(rechnung.identification) + ".pdf")
        
        #rechnung_pdf = Rechnung_e_pdf.create(
        #        identification = rechnung.identification,
        #        pdf = open(pdf_link, "rb").read()
        #    )
        #rechnung_pdf.save()
        print("PDF gespeichert")
        
        answer_dict = model_to_dict(rechnung)
    else:
        answer_dict = {}
        
    try: rechnung_e_db.close()
    except: True
    try: unde_db.close()
    except: True
    try: user_db.close()
    except: True 
    
    return answer_dict
           
                
def create_invoice(rechnung_mode, vorlage_id, pin_code):# return rechnung id or -1 on error
    print("create_invoice(" + str(rechnung_mode) + ", " + str(vorlage_id) + ", " + str(pin_code) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True
    try: user_db.connect()
    except: True

    rechnung_mode = str(rechnung_mode).upper() # R = Rechnung oder K = Kasse
    vorlage_id = str(vorlage_id).replace("RV", "")
    pin_code = str(pin_code).upper()

    if not rechnung_mode == "K" and not rechnung_mode == "R":
        print("ERROR rechnung_mode is not valid !")
        return -1

    query = User.select().where(User.pin == pin_code)
    if query.exists():
        query_vorlage = RechnungVorlage.select().where(RechnungVorlage.identification == vorlage_id)        
        if query_vorlage.exists():
            kunde = get_client(query_vorlage[0].kunde_id)
        else:           
            print("ERROR Vorlage existiert nicht")
            return -1  
        
        if len(kunde["tva"]) > 0:
            #tva_valid = libs.BlueFunc.check_tva(kunde["tva"])
            #if tva_valid:                
            created_by = str(query[0].name).upper()
            #else:
            #    print("ERROR TVA: " + str(kunde["tva"]) + " ist nichgt gültig")
            #    return -1                
        else: # Nur admin darf Rechnungen machen
            if rechnung_mode == "K":
                created_by = str(query[0].name).upper()            
            else:
                if query[0].admin:
                    created_by = str(query[0].name).upper()
                else:
                    print("ERROR benutzer ist kein admin!")
                    return -1
        
    else:
        print("ERROR benutzer existiert nicht !")
        return -1

    query = RechnungVorlage.select().where(RechnungVorlage.identification == vorlage_id)

    if query.exists():
        vorlage = query[0]
        if vorlage.rechnung == "":

            if rechnung_mode == "R":
                rechnung = Rechnung.create(kunde_id = vorlage.kunde_id,
                                        kunde_name = kunde["name"],
                                        lieferscheine = vorlage.lieferscheine,
                                        vorlage = "RV" + str(vorlage.identification),
                                        total_htva = vorlage.total_htva,
                                        steuern = vorlage.steuern,
                                        total_ohne_steuern = vorlage.total_ohne_steuern,
                                        total = vorlage.total,
                                        created_by = created_by,
                                        ref = "",
                                        rappel = 0,
                                        rest = vorlage.total,
                                        datum = Date(),
                                        datum_todo = TodayAddDays(14)
                                    )

                # REF +++010/8191/47402+++
                ref = str(rechnung.identification)
                while len(ref) < 8:
                    ref = "0" + ref
                year = str(datetime.datetime.now().strftime("%y"))
                ref = year + ref
                checksum = int(ref)%97
                if len(str(checksum)) == 1: checksum = "0" + str(checksum)
                ref = "+++" + ref[0] + ref[1] + ref[2] + "/" + ref[3] + ref[4] + ref[5] + ref[6] + "/" + ref[7] + ref[8] + ref[9] + str(checksum) + "+++"
                rechnung.ref = ref

            else:
                rechnung = Kasse.create(kunde_id = vorlage.kunde_id,
                                        kunde_name = kunde["name"],
                                        lieferscheine = vorlage.lieferscheine,
                                        vorlage = "RV" + str(vorlage.identification),
                                        total_htva = vorlage.total_htva,
                                        steuern = vorlage.steuern,
                                        total_ohne_steuern = vorlage.total_ohne_steuern,
                                        total = vorlage.total,
                                        created_by = created_by,
                                        ref = "",
                                        rappel = 0,
                                        rest = vorlage.total,
                                        datum = Date(),
                                        datum_todo = TodayAddDays(0)
                                    )
                rechnung.ref = rechnung_mode + str(rechnung.identification)
            
            rechnung.save()
            rechnung_id = rechnung_mode + str(rechnung.identification)
            

            lieferscheine = vorlage.lieferscheine.split("|")
            for lieferschein_id in lieferscheine:
                if not lieferschein_id == "":
                    print("lieferschein_id: " + str(lieferschein_id))                    
                    set_delivery_finish(lieferschein_id, True)
                    
                    
                    lieferschein_object = Lieferschein.get(Lieferschein.identification == lieferschein_id)
                    lieferschein_object.rechnung = str(rechnung_id)
                    lieferschein_object.save()

            vorlage.rechnung = str(rechnung_id)
            vorlage.save()
        else: 
            print("Diese Vorlage ist bereits in der Rechnung/Kasse " + str(vorlage.rechnung))
            return -1
            
        set_client_rappel(vorlage.kunde_id)
        
    else: 
        print("Vorlage " + str(vorlage_id) + " existiert nicht")
        return -1

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    try: user_db.close()
    except: True

    return rechnung_id

def get_invoice_e_pdf(invoice_id): # return Link or False
    invoice_id = str(invoice_id).replace(".0", "")
    answer = False
    
    try: rechnung_e_db.connect()
    except: True
    
    query = Rechnung_e.select().where(Rechnung_e.identification == int(invoice_id.replace("RE", "")))
    if query.exists():
        date_list = query[0].datum.split("-")
        pdf_source_path = "/etc/zk-data-server/data/" + server_name + "/files/invoice_e/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/RE" + str(invoice_id) + ".pdf"
        answer = libs.ServerTools.export_to_web_tmp(pdf_source_path)
    else:
        print("Invoice " + str(invoice_id) + " doesn't exists! -> return FALSE")
        answer = False
    
    try: rechnung_e_db.close()
    except: True
    
    #if not os.path.exists("/var/www/html/zk-data/Rechnung/" + server_name):
    #    os.system("mkdir -p  /var/www/html/zk-data/Rechnung/" + server_name)
    #    os.system("chmod -R 777 /var/www/html/zk-data/Rechnung/" + server_name)
    
    #pdf_file = "/var/www/html/zk-data/Rechnung/" + server_name + "/RE" + invoice_id + ".pdf"
    #answer = "/zk-data/Rechnung/" + server_name + "/RE" + invoice_id + ".pdf"
    
    #try: rechnung_e_pdf_db.close()
    #except: True

    #query = Rechnung_e_pdf.select().where(Rechnung_e_pdf.identification == invoice_id)
    #if query.exists():
    #    rechnung_pdf = query[0]
    #    if not os.path.exists(pdf_file):
    #        open(pdf_file, "wb").write(rechnung_pdf.pdf)    
    #else:
    #    print("Invoice " + str(invoice_id) + " doesn't exists! -> return FALSE")
    #    answer = False
    
    #try: rechnung_e_pdf_db.close()
    #except: True
    
    return answer
    
def get_file_length(identification):
    print("get_file_length(" + str(identification) + ")")
    identification = str(identification)
    if "zk_data_tmp_file_" in identification:
        file = "/tmp/" + identification
        if os.path.exists(file):
            file_bytes = open(file, "rb").read()
            file_as_string = base64.b64encode(file_bytes)         
            parts_list = libs.BlueFunc.file_split(file_as_string, 5000)        
            length = len(parts_list)
        else:
            length = 0    
    else:
        length = 0     
    print("length: " + str(length))  
    return length
    
def get_file_part(identification, index):
    print("get_file_part(" + str(identification) + ", " + str(index) + ")")
    identification = str(identification)
    index = int(index)    
    if "zk_data_tmp_file_" in identification:
        file = "/tmp/" + identification
        if os.path.exists(file):
            file_bytes = open(file, "rb").read()
            file_as_string = base64.b64encode(file_bytes)         
            parts_list = libs.BlueFunc.file_split(file_as_string, 5000)        
            if len(parts_list) > index:
                part = parts_list[index].decode('utf8')
            else:
                print("FILE_END")
                part = "FILE_END"
        else:
            part = ""        
    else:
        part = ""        
    return part

def send_file_part(identification, index, data_as_string):
    identification = str(identification)
    index = int(index)
    data_as_string = str(data_as_string)
    
    if index == 0: # new file
        timestamp = str(Timestamp())
        data = data_as_string.encode('utf8')
        data = base64.b64decode(data)
        filename = "zk_data_tmp_file_" + timestamp + "_" + identification
        open("/tmp/" + filename, "wb").write(data)
    else:
        if "zk_data_tmp_file_" in identification:
            filename = identification                        
            old_data = open("/tmp/" + filename, "rb").read()
            new_data = data_as_string.encode('utf8')
            new_data = base64.b64decode(new_data)
            open("/tmp/" + filename, "wb").write(old_data + new_data)            
        else:
            filename = "ERROR"       
    return filename
    
def copy_to_tmp(path_of_file_to_copy):
    timestamp = str(Timestamp())
    filename = path_of_file_to_copy.split("/")[-1]
    new_filename = "zk_data_tmp_file_" + timestamp + "_" + filename
    os.system("cp /tmp/" + new_filename + " " + new_path)
    return new_filename
    
def set_rechnung_vorlage_total(rechnung_id):
    print("set_rechnung_vorlage_total(" + str(rechnung_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    rechnung_id = str(rechnung_id).replace("RV", "")
    rechnung_vorlage = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
    rechnung_vorlage_total_copy = rechnung_vorlage.total
    print("rechnung_vorlage old total: " + str(rechnung_vorlage_total_copy))

    rechnung_vorlage.total_ohne_steuern = {0: 0.0}
    rechnung_vorlage.steuern = {}
    rechnung_vorlage.total_htva = 0.0
    rechnung_vorlage.total = 0.0

    for lieferschein_id in rechnung_vorlage.lieferscheine.split("|"):
        if not lieferschein_id == "":
            lieferschein = Lieferschein.get(Lieferschein.identification == lieferschein_id)

            #total_ohne_steuern
            for linie in lieferschein.total_ohne_steuern.split("|"):
                if not linie == "":
                    tva_satz = float(linie.split(";")[0])
                    tva_wert = float(linie.split(";")[1])      
                    if not int(tva_satz) in rechnung_vorlage.total_ohne_steuern: rechnung_vorlage.total_ohne_steuern[tva_satz] = 0.0                                  
                    rechnung_vorlage.total_ohne_steuern[tva_satz] = float(rechnung_vorlage.total_ohne_steuern[tva_satz]) + float(tva_wert)
                    rechnung_vorlage.total_ohne_steuern[tva_satz] = round(rechnung_vorlage.total_ohne_steuern[tva_satz], 4)

            #total_htva
            rechnung_vorlage.total_htva = rechnung_vorlage.total_htva + lieferschein.total_htva
            #total
            rechnung_vorlage.total = rechnung_vorlage.total + lieferschein.total_htva
            
    #steuern  
    for tva_satz in rechnung_vorlage.total_ohne_steuern:
        tva_satz = float(tva_satz)
        rechnung_vorlage.steuern[tva_satz] = float(rechnung_vorlage.total_ohne_steuern[tva_satz])*tva_satz/100   
        rechnung_vorlage.steuern[tva_satz] = rechnung_vorlage.steuern[tva_satz] 
        rechnung_vorlage.steuern[tva_satz] = round( rechnung_vorlage.steuern[tva_satz], 2)
    
        #total
        rechnung_vorlage.total = rechnung_vorlage.total + rechnung_vorlage.steuern[tva_satz]

    rechnung_vorlage.total_htva = round( rechnung_vorlage.total_htva, 2) 
    rechnung_vorlage.total = round( rechnung_vorlage.total, 2)
    print("rechnung_vorlage new total: " + str(rechnung_vorlage.total))
      
    # convert to 0;0|0;0
    preis_total_ohne_steuern = ""
    for st in rechnung_vorlage.total_ohne_steuern:
        if preis_total_ohne_steuern == "":
            preis_total_ohne_steuern = str(st) + ";" + str(rechnung_vorlage.total_ohne_steuern[st])
        else:
            preis_total_ohne_steuern = preis_total_ohne_steuern + "|" + str(st) + ";" + str(rechnung_vorlage.total_ohne_steuern[st])
    rechnung_vorlage.total_ohne_steuern = preis_total_ohne_steuern
         
    # convert to 0;0|0;0 
    preis_steuern = ""
    for st in rechnung_vorlage.steuern:
        if preis_steuern == "":
            preis_steuern = str(st) + ";" + str(rechnung_vorlage.steuern[st])
        else:
            preis_steuern = preis_steuern + "|" + str(st) + ";" + str(rechnung_vorlage.steuern[st])
    rechnung_vorlage.steuern = preis_steuern
              
    if rechnung_vorlage.total == rechnung_vorlage_total_copy:
        print("Total not changed")   
    else:
        print("Total changed")                     
        rechnung_vorlage.lastchange = Timestamp()
        rechnung_vorlage.save()    

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    
def set_rechnung_vorlage_total_old(rechnung_id):
    print("set_rechnung_vorlage_total_old(" + str(rechnung_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    rechnung_id = str(rechnung_id).replace("RV", "")
    rechnung_vorlage = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
    rechnung_vorlage_total_copy = rechnung_vorlage.total

    rechnung_vorlage.total_htva = 0.0
    rechnung_vorlage.total = 0.0

    rechnung_vorlage.steuern = ""
    for lieferschein_id in rechnung_vorlage.lieferscheine.split("|"):
        if not lieferschein_id == "":
            lieferschein = Lieferschein.get(Lieferschein.identification == lieferschein_id)

            rechnung_vorlage.total_htva = round(rechnung_vorlage.total_htva + lieferschein.total_htva, 2)
            rechnung_vorlage.total = round(rechnung_vorlage.total + lieferschein.total, 2)

            steuern_liste = {}
            for tva_line_r in rechnung_vorlage.steuern.split("|"):
                if ";" in tva_line_r:
                    tva_satz_r = int(tva_line_r.split(";")[0])
                    tva_wert_r = float(tva_line_r.split(";")[1])

                    steuern_liste[tva_satz_r] = tva_wert_r

            for tva_line_l in lieferschein.steuern.split("|"):
                if ";" in tva_line_l:
                    tva_satz_l = int(float(tva_line_l.split(";")[0]))
                    tva_wert_l = float(tva_line_l.split(";")[1])

                    if tva_satz_l in steuern_liste:
                        steuern_liste[tva_satz_l] = steuern_liste[tva_satz_l] + tva_wert_l
                    else:
                        steuern_liste[tva_satz_l] = tva_wert_l

            rechnung_vorlage.steuern = ""
            for steuer_key in steuern_liste.keys():
                if rechnung_vorlage.steuern == "":
                    rechnung_vorlage.steuern = str(steuer_key) + ";" + str(round(steuern_liste[steuer_key], 2))
                else:
                    rechnung_vorlage.steuern = rechnung_vorlage.steuern + "|" + str(steuer_key) + ";" + str(round(steuern_liste[steuer_key], 2))
       
    if rechnung_vorlage.total == rechnung_vorlage_total_copy:
        print("Total not changed") 
    else:
        print("Total changed")                     
        rechnung_vorlage.lastchange = Timestamp()
        
    rechnung_vorlage.save()    

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True



def create_rechnung_vorlage(lieferschein_id):
    print("create_rechnung_vorlage(" + str(lieferschein_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    lieferschein_id = str(lieferschein_id)

    lieferschein = Lieferschein.get(Lieferschein.identification == lieferschein_id)
    kunde_id = str(lieferschein.kunde_id)
    kunde = get_client(kunde_id)
    
    if ai_helper:
        total = float(lieferschein.total)
        if total > 1000.0: total = int(float(total) / 1000) * 1000    
        elif total > 100.0: total = int(float(total) / 100) * 100                    
        else: total = int(float(total) / 25) * 25          
        if total < 0: total = 0
        
        ai_helper.add_to_prediction("predict_delivery_cost", str(total), lieferschein.info + " " + lieferschein.maschine)
        
        ai_helper.add_to_prediction("predict_delivery_worker", str(lieferschein.user), lieferschein.info + " " + lieferschein.maschine)
    
    rechnung = str(lieferschein.rechnung)
    print("kunde_id: " + str(kunde_id))
    print("rechnung: " + str(rechnung))

    #query = Lieferschein.select().where(Lieferschein.rechnung.contains(lieferschein_id))
    
    if rechnung == "" and not lieferschein.angebot and not lieferschein.bestellung:
        neue_rechnung_vorlage = RechnungVorlage.create( 
                                                    kunde_id = kunde_id,
                                                    kunde_name = kunde["name"],
                                                    lieferscheine = str(lieferschein_id),
                                                    rechnung = "",
                                                    total_htva = lieferschein.total_htva,
                                                    steuern = lieferschein.steuern,
                                                    total_ohne_steuern = lieferschein.total_ohne_steuern,
                                                    total = lieferschein.total,
                                                    lastchange = Timestamp(),
                                                    datum = Date()
                                                )  
        
        lieferschein.rechnung = "RV" + str(neue_rechnung_vorlage.identification)
        lieferschein.lastchange = Timestamp()
        rechnung = lieferschein.rechnung        
        lieferschein.save()    
        

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True

    print("return " + str(rechnung))
    return rechnung

def rechnung_vorlage_check_kunde(rechnung_id):
    print("rechnung_vorlage_check_kunde(" + str(rechnung_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    rechnung_id = str(rechnung_id).replace("RV", "")    

    rechnung_vorlage = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)

    lieferscheineListe = rechnung_vorlage.lieferscheine.split("|")

    if not lieferscheineListe[0] == "":
        print("Lieferscheine in dieser vorlage: " + str(lieferscheineListe))

        ## Check jeden kunden
        kunden_sind_identisch = True
        erster_kunde = Lieferschein.get(Lieferschein.identification == lieferscheineListe[0]).kunde_id
        for l in lieferscheineListe:
            if not Lieferschein.get(Lieferschein.identification == l).kunde_id == erster_kunde:
                kunden_sind_identisch = False 

        if kunden_sind_identisch:
            rechnung_vorlage.kunde_id = erster_kunde
        else:
            rechnung_vorlage.kunde_id = 0
        print("rechnung_vorlage.kunde_id: " + str(rechnung_vorlage.kunde_id))
    else:
        rechnung_vorlage.kunde_id = 0

    rechnung_vorlage.save()
        
    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True

    #print("return " + str(Antwort))
    #return Antwort

def add_lieferschein_to_vorlage(lieferschein_id, rechnung_id):# return Bool of sucess
    print("add_lieferschein_to_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    lieferschein_id = str(lieferschein_id).replace("L", "")
    rechnung_id = str(rechnung_id).replace("RV", "")

    try: lieferschein = Lieferschein.get(Lieferschein.identification == lieferschein_id)
    except: return False      

    if lieferschein.rechnung == "":
        rechnung_vorlage = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
            
        lieferscheineListe = rechnung_vorlage.lieferscheine.split("|")
        print("Lieferscheine in dieser vorlage: " + str(lieferscheineListe))
        if lieferscheineListe[0] == "":
            lieferscheineListe[0] = lieferschein.identification
        else:
            lieferscheineListe.append(lieferschein.identification)
        print("Lieferscheine in dieser vorlage: " + str(lieferscheineListe))

        ## Check jeden kunden
        kunden_sind_identisch = True
        erster_kunde = Lieferschein.get(Lieferschein.identification == lieferscheineListe[0]).kunde_id
        for l in lieferscheineListe:
            if not Lieferschein.get(Lieferschein.identification == l).kunde_id == erster_kunde:
                kunden_sind_identisch = False 
        if kunden_sind_identisch:
            rechnung_vorlage.kunde_id = erster_kunde
        else:
            rechnung_vorlage.kunde_id = 0
        ####

        lieferscheineListe = [int(x) for x in lieferscheineListe]
        lieferscheineListe = sorted(lieferscheineListe)
        lieferscheineListe = [str(x) for x in lieferscheineListe]

        rechnung_vorlage.lieferscheine = "|".join(lieferscheineListe)

        rechnung_vorlage.lastchange = Timestamp()

        rechnung_vorlage.save()

        lieferschein.rechnung = "RV" + rechnung_id
        lieferschein.lastchange = Timestamp()
        lieferschein.save()

        Antwort = True
    else:
        print("Dieser Lieferschein " + str(lieferschein_id) + " ist bereits in einer Vorlage / Rechnung")
        Antwort = False
        
    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    
    delete_invoice_pdf("RV" + rechnung_id)

    print("return " + str(Antwort))
    return Antwort

def remove_lieferschein_from_vorlage(lieferschein_id, rechnung_id):
    print("remove_lieferschein_from_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ")")
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    lieferschein_id = str(lieferschein_id).replace("L", "")
    rechnung_id = str(rechnung_id).replace("RV", "")

    try: lieferschein = Lieferschein.get(Lieferschein.identification == lieferschein_id)
    except: return False      

    if lieferschein.rechnung == "RV" + rechnung_id:
        rechnung_vorlage = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
            
        lieferscheineListe = rechnung_vorlage.lieferscheine.split("|")
        print("Lieferscheine in dieser vorlage: " + str(lieferscheineListe))
        del lieferscheineListe[lieferscheineListe.index(lieferschein_id)]
                
        if len(lieferscheineListe) > 0:
            ## Check jeden kunden
            kunden_sind_identisch = True
            erster_kunde = Lieferschein.get(Lieferschein.identification == lieferscheineListe[0]).kunde_id
            for l in lieferscheineListe:
                if not Lieferschein.get(Lieferschein.identification == l).kunde_id == erster_kunde:
                    kunden_sind_identisch = False 
            if kunden_sind_identisch:
                rechnung_vorlage.kunde_id = erster_kunde
            else:
                rechnung_vorlage.kunde_id = 0
            ####

            lieferscheineListe = [int(x) for x in lieferscheineListe]
            lieferscheineListe = sorted(lieferscheineListe)
            lieferscheineListe = [str(x) for x in lieferscheineListe]

        rechnung_vorlage.lieferscheine = "|".join(lieferscheineListe)

        rechnung_vorlage.lastchange = Timestamp()

        rechnung_vorlage.save()

        lieferschein.rechnung = ""
        lieferschein.lastchange = Timestamp()
        lieferschein.save()

        Antwort = True
    else:
        #print("Dieser Lieferschein " + str(lieferschein_id) + " ist bereits in einer Vorlage / Rechnung")
        Antwort = False
        
    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True

    delete_invoice_pdf("RV" + rechnung_id)
    
    print("return " + str(Antwort))
    return Antwort

def set_client_rappel(client_id):# return number
    print("set_client_rappel(" + str(client_id) + ")")

    try: kunde_db.connect
    except: True
    try: rechnung_db.connect
    except: True
    
    query = Kunde.select().where(Kunde.identification == client_id)
    rappel = 0
    
    if query.exists():
        kunde = Kunde.get(Kunde.identification == client_id)
        for r in Rechnung.select().where(Rechnung.kunde_id == client_id):
            if (r.rest > 0.0):
                if r.rappel > rappel:
                    rappel = r.rappel
                    
        for r in Kasse.select().where(Kasse.kunde_id == client_id):
            if (r.rest > 0.0):
                if r.rappel > rappel:
                    rappel = r.rappel                      
            
        kunde.rappel = rappel
        kunde.save()
    
    try: kunde_db.close()
    except: True
    try: rechnung_db.close()
    except: True

    return float(rappel)

def set_invoice_date_todo(identification, date):# return Bool of sucess
    print("set_invoice_date_todo(" + str(identification) + ", " + str(date) + ")")

    try: rechnung_db.connect
    except: True

    if not "-" in date: date = Date()
    else:
        if not len(date.split("-")) == 3:
            date = Date()

    if "RV" in identification:
            print("ERROR " + str(identification) + " cannot set date_todo")
            return False
    else:
        if "K" in identification:
            rechnung = Kasse.get(Kasse.identification == identification.replace("K", ""))
        else:
            rechnung = Rechnung.get(Rechnung.identification == identification.replace("R", ""))
    
    rechnung.datum_todo = date
    rechnung.save()  

    try: rechnung_db.close()
    except: True

    return True

def set_invoice_rappel(identification):# return Bool of sucess
    print("set_invoice_rappel(" + str(identification) + ")")

    try: rechnung_db.connect
    except: True

    if "R" in identification:
        rechnung = Rechnung.get(Rechnung.identification == identification.replace("R", ""))
    else:
        if "K" in identification:
            rechnung = Kasse.get(Kasse.identification == identification.replace("K", ""))
        else:
            print("ERROR " + str(identification) + " is not valid for Rappel Calculation")
            return False
    
    rechnung.datum_todo = TodayAddDays(20)
    rechnung.rappel = float(rechnung.rappel + 1)
    rechnung.save()  

    try: rechnung_db.close()
    except: True
    
    set_client_rappel(rechnung.kunde_id)
    
    return True

def set_rechnung_rest(identification):# return Bool of sucess
    print("set_rechnung_rest(" + str(identification) + ")")

    try: rechnung_db.connect
    except: True
    try: rechnung_e_db.connect
    except: True
    try: transaktion_db.connect()
    except: True

    if "RE" in identification:
        rechnung = Rechnung_e.get(Rechnung_e.identification == identification.replace("RE", ""))
    else:
        if "R" in identification:
            rechnung = Rechnung.get(Rechnung.identification == identification.replace("R", ""))
        else:
            if "K" in identification:
                rechnung = Kasse.get(Kasse.identification == identification.replace("K", ""))
            else:
                print("ERROR " + str(identification) + " is not valid for Rest Calculation")
                return False
    
    rechnung.rest = rechnung.total
    list_of_transaktions = Transaktion.select().where(Transaktion.rechnung == identification)
    for transaktion in list_of_transaktions:
        if "RE" in identification:
            rechnung.rest = round(rechnung.rest + transaktion.betrag, 2)
        else:
            rechnung.rest = round(rechnung.rest - transaktion.betrag, 2)
        
    rechnung.save()

    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    try: transaktion_db.close()
    except: True

    return True

def get_invoice(identification):
    print("get_invoice(" + str(identification) + ")")

    try: rechnung_db.connect()
    except: True

    rechnung_id = str(identification)

    if "RV" in rechnung_id:            
        rechnung_id = rechnung_id.replace("RV", "")
        rechnung = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
        if rechnung.rechnung == "": 
            rechnung_vorlage_check_kunde(identification)
            set_rechnung_vorlage_total(rechnung_id)
            rechnung = RechnungVorlage.get(RechnungVorlage.identification == rechnung_id)
            rechnung.identification = "RV" + str(rechnung.identification)        
        else: # ist einer Rechnung
            rechnung_id = rechnung.rechnung
            if "KK" in rechnung_id:
                rechnung_id = rechnung_id.replace("KK", "K")
            if "RR" in rechnung_id:
                rechnung_id = rechnung_id.replace("RR", "R")
                               
            if "K" in rechnung_id:                
                rechnung_id = rechnung_id.replace("K", "")
                rechnung = Kasse.get(Kasse.identification == rechnung_id)
                rechnung.identification = "K" + str(rechnung.identification)
            else:
                if "R" in rechnung_id:
                    rechnung_id = rechnung_id.replace("R", "")
                    rechnung = Rechnung.get(Rechnung.identification == rechnung_id)
                    rechnung.identification = "R" + str(rechnung.identification)
                else: return {}
    else:
        if "K" in rechnung_id:
            rechnung_id = rechnung_id.replace("K", "")
            rechnung = Kasse.get(Kasse.identification == rechnung_id)
            rechnung.identification = "K" + str(rechnung.identification)
        else:
            if "R" in rechnung_id:
                rechnung_id = rechnung_id.replace("R", "")
                rechnung = Rechnung.get(Rechnung.identification == rechnung_id)
                rechnung.identification = "R" + str(rechnung.identification)
            else: return {}

    try: rechnung_db.close()
    except: True

    antwort = model_to_dict(rechnung)

    print("return " + str(antwort))
    return antwort
    
def set_invoice_e(rechnung_dict):
    print("set_invoice_e(" + str(rechnung_dict) + ")")

    try: rechnung_e_db.connect()
    except: True

    rechnung_id = str(rechnung_dict["identification"])

    if "pdf" in rechnung_dict:
        del rechnung_dict["pdf"]        
    if "datum" in rechnung_dict:
        del rechnung_dict["datum"]        
    if "pcname" in rechnung_dict:
        del rechnung_dict["pcname"]        
    if "user" in rechnung_dict:
        del rechnung_dict["user"]     
    if "secure_key" in rechnung_dict:
        del rechnung_dict["secure_key"]
    
    query = Rechnung_e.select().where(Rechnung_e.identification == rechnung_id)
    
    if not query.exists(): 
        print("Error " + str(rechnung_id) + " doesn't exists")
        return False
    
    
    rechnung = dict_to_model(Rechnung_e, rechnung_dict)
    rechnung.save()

    try: rechnung_e_db.close()
    except: True
    
    print("return True")
    return True
    
def get_invoice_e(identification):
    print("get_invoice_e(" + str(identification) + ")")

    try: rechnung_e_db.connect()
    except: True

    rechnung_id = str(identification)

    query = Rechnung_e.select().where(Rechnung_e.identification == rechnung_id)
    if not query.exists(): return {}
    
    rechnung = Rechnung_e.get(Rechnung_e.identification == rechnung_id)
        
    antwort = model_to_dict(rechnung)

    try: rechnung_e_db.close()
    except: True
    
    print("return " + str(antwort))
    return antwort

def add_inventar(Dict):# return Bool of sucess
    print("add_inventar(" + str(Dict) + ")")
    try: inventar_db.connect()
    except: True

    Dict["bcode"] = str(Dict["bcode"]).rstrip()
    Dict["anzahl"] = float(Dict["anzahl"])
    Dict["categorie"] = int(Dict["categorie"])
    Dict["tag"] = str(Dict["tag"])
    Dict["monat"] = str(Dict["monat"])
    Dict["jahr"] = str(Dict["jahr"])
    Dict["user"] = str(Dict["user"]).upper()
    
    if "pcname" in Dict: del Dict["pcname"]
    if "secure_key" in Dict: del Dict["secure_key"]

    print("len(Dict[bcode]) = " + str(len(str(Dict["bcode"]))))

    # Return ERROR if its not an digit
    if not Dict["bcode"].isdigit(): return False

    if len(str(Dict["bcode"])) > 10: 
        ArtDict = get_article({"barcode": Dict["bcode"]})
    else: 
        ArtDict = get_article({"identification": Dict["bcode"]})

    if not ArtDict == {}:
        if not int(ArtDict["categorie"]) == int(Dict["categorie"]):
            set_article({"identification": ArtDict["identification"], "categorie": Dict["categorie"]})
    
        #id = getFreieInventarID()

        Dict["bcode"] = ArtDict["identification"]
        del Dict["categorie"]
        CreateThis = dict_to_model(Inventar, Dict)
        CreateThis.save()

        inventar_db.close()

        return True
    else:
        return False

def print_article_stock(datum): # write file with list of article and Price
    print("print_article_stock  START")
    
    try: stock_db.connect()
    except: True

    if not os.path.exists("/var/www/html/zk-data/print_article_stock/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/print_article_stock/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/print_article_stock/" + server_name)
    
    file = open("/var/www/html/zk-data/print_article_stock/" + server_name + "/print_article_stock.csv", "w")    
    answer = "/zk-data/print_article_stock/" + server_name + "/print_article_stock.csv"
    
    file_data = ["identification;name;categorie;anzahl;preis;gesamt"]
    data = {}
    query = Artikel.select().order_by(Artikel.categorie)
    for artikel in query:    
        identification = str(artikel.identification)
        
        name = str(artikel.name_de).rstrip()
        if name == "": name = "?"
        
        categorie = str(artikel.categorie)
        if categorie == "": categorie = "0"
        
        anzahl = float(artikel.anzahl)
        
        preis = float(artikel.preisek)
        
        gesamt = anzahl * preis
        gesamt = float(int(float(gesamt) * 100))/100
        
        data[identification] = {"name": name, "categorie": categorie, "anzahl": anzahl, "preis": preis, "gesamt": gesamt}
        
    moves = Bewegung.select().where(Bewegung.datum > datum).order_by(-Bewegung.identification) 
    for move in moves:
        print(move.bcode)
        try: data[move.bcode]["anzahl"] = float(move.start)
        except: move.start = 0.0
        try: data[move.bcode]["preis"] = float(move.preisek)
        except: move.preisek = 0.0
        
        gesamt = float(move.start) * float(move.preisek)
        gesamt = float(int(float(gesamt) * 100))/100
        
        try: data[move.bcode]["gesamt"] = gesamt
        except: True
    
    for d in data:
        if not data[d]["anzahl"] == 0.0:
            #file_data.append(identification + ";" + name + ";" + categorie + ";" + quantity + ";" + price + ";" + value_ek)
            file_data.append(str(d) + ";" + str(data[d]["name"]) + ";" + str(data[d]["categorie"]) + ";" + str(data[d]["anzahl"]).replace(".", ",") + ";" + str(data[d]["preis"]).replace(".", ",") + ";" + str(data[d]["gesamt"]).replace(".", ","))
  
    file.write("\n".join(file_data))
    
    try: stock_db.close()
    except: True

    print("print_article_stock  END")
    
    return answer

def search_invoice(identification, kunde, rest, datum, index):# search Rechnung with id, client name/id, only with open rest, return ID answer by index
    print("search_invoice(" + str(identification) + ", " + str(kunde) + ", " + str(rest) + ", " + str(datum) + ", " + str(index) + ")")

    identification = str(identification).upper()
    kunde = str(kunde)
    rest = bool(rest)
    datum = str(datum)
    index = int(index)
    
    antwort = -1
    where_condition = []

    try: rechnung_db.connect()
    except: True
    try: kunde_db.connect()
    except: True

    antwort = []
           
    """            
    if not kunde == "":
        if not str(kunde).isdigit():
            clients = []
            query_kunde = Kunde.select().where(Kunde.name.contains(kunde))
            if query_kunde.exists():
                for k in query_kunde:
                    clients.append(k.identification)
     """                           

    if "RV" in identification:
        query = RechnungVorlage.select().where(RechnungVorlage.identification.contains(identification.replace("RV", ""))).order_by(RechnungVorlage.datum)
        if not kunde == "":
            if str(kunde).isdigit():
                query = query.where(RechnungVorlage.kunde_id == kunde)#.order_by(RechnungVorlage.datum)            
            else:
                query = query.where(RechnungVorlage.kunde_name.contains(kunde))#.order_by(RechnungVorlage.datum)        
        if rest:
            return -1
        if not datum == "":
            query = query.where(RechnungVorlage.datum.contains(datum))#.order_by(RechnungVorlage.datum)

        for obj in query: antwort.insert(0, "RV" + str(obj.identification))
            
    else:
        if "R" in identification:
            query = Rechnung.select().where(Rechnung.identification.contains(identification.replace("R", ""))).order_by(Rechnung.datum)            
            if not kunde == "":
                if str(kunde).isdigit():
                    query = query.where(Rechnung.kunde_id == kunde)#.order_by(Rechnung.datum)            
                else:
                    query = query.where(Rechnung.kunde_name.contains(kunde))#.order_by(Rechnung.datum)  
            if rest:
                query = query.where(Rechnung.rest != 0.0)#.order_by(Rechnung.datum)
            if not datum == "":
                query = query.where(Rechnung.datum.contains(datum))#.order_by(Rechnung.datum)

            for obj in query: antwort.insert(0, "R" + str(obj.identification))
                                 
        else:
            if "K" in identification:
                query = Kasse.select().where(Kasse.identification.contains(identification.replace("K", ""))).order_by(Kasse.datum)              
                if not kunde == "":
                    if str(kunde).isdigit():
                        query = query.where(Kasse.kunde_id == kunde)#.order_by(Kasse.datum)            
                    else:
                        query = query.where(Kasse.kunde_name.contains(kunde))#.order_by(Kasse.datum)  
                if rest:
                    query = query.where(Kasse.rest != 0.0)#.order_by(Kasse.datum)
                if not datum == "":
                    query = query.where(Kasse.datum.contains(datum))#.order_by(Kasse.datum)

                for obj in query: antwort.insert(0, "K" + str(obj.identification))
                  
            else:
                if identification == "":
                    query = RechnungVorlage.select().order_by(RechnungVorlage.datum)
                    query2 = Rechnung.select().order_by(Rechnung.datum)
                    query3 = Kasse.select().order_by(Kasse.datum)
                    if not kunde == "":
                        if str(kunde).isdigit():
                            query = query.where(RechnungVorlage.kunde_id == kunde)#.order_by(RechnungVorlage.datum)  
                            query2 = query2.where(Rechnung.kunde_id == kunde)#.order_by(Rechnung.datum)  
                            query3 = query3.where(Kasse.kunde_id == kunde)#.order_by(Kasse.datum)            
                        else:
                            query = query.where(RechnungVorlage.kunde_name.contains(kunde))#.order_by(RechnungVorlage.datum)  
                            query2 = query2.where(Rechnung.kunde_name.contains(kunde))#.order_by(Rechnung.datum) 
                            query3 = query3.where(Kasse.kunde_name.contains(kunde))#.order_by(Kasse.datum)                   
                    if rest:
                        query = query.where(RechnungVorlage.identification == -1)## = none
                        query2 = query2.where(Rechnung.rest != 0.0)#.order_by(Rechnung.datum)
                        query3 = query3.where(Kasse.rest != 0.0)#.order_by(Kasse.datum)
                    if not datum == "":
                        query = query.where(RechnungVorlage.datum.contains(datum))#.order_by(RechnungVorlage.datum)
                        query2 = query2.where(Rechnung.datum.contains(datum))#.order_by(Rechnung.datum)
                        query3 = query3.where(Kasse.datum.contains(datum))#.order_by(Kasse.datum)                             
                    
                    for obj in query: antwort.insert(0, "RV" + str(obj.identification))                       
                            
                    for obj in query2: antwort.insert(0, "R" + str(obj.identification))                      
                            
                    for obj in query3: antwort.insert(0, "K" + str(obj.identification))
                        
                else:
                    query = RechnungVorlage.select().where(RechnungVorlage.identification.contains(identification.replace("RV", ""))).order_by(RechnungVorlage.datum)
                    query2 = Rechnung.select().where(Rechnung.identification.contains(identification.replace("R", ""))).order_by(Rechnung.datum)
                    query3 = Kasse.select().where(Kasse.identification.contains(identification.replace("K", ""))).order_by(Kasse.datum)         
                    if not kunde == "":
                        if str(kunde).isdigit():
                            query = query.where(RechnungVorlage.kunde_id == kunde)#.order_by(RechnungVorlage.datum)
                            query2 = query2.where(Rechnung.kunde_id == kunde)#.order_by(Rechnung.datum)
                            query3 = query3.where(Kasse.kunde_id == kunde)#.order_by(Kasse.datum)
                        else:
                            query = query.where(RechnungVorlage.kunde_name.contains(kunde))#.order_by(RechnungVorlage.datum)
                            query2 = query2.where(Rechnung.kunde_name.contains(kunde))#.order_by(Rechnung.datum)
                            query3 = query3.where(Kasse.kunde_name.contains(kunde))#.order_by(Kasse.datum)                        
                    if rest:
                        query = query.where(RechnungVorlage.identification == -1)## = none
                        query2 = query2.where(Rechnung.rest != 0.0)#.order_by(Rechnung.datum)
                        query3 = query3.where(Kasse.rest != 0.0)#.order_by(Kasse.datum)
                                        
                    for obj in query: antwort.insert(0, "RV" + str(obj.identification))                      
                            
                    for obj in query2: antwort.insert(0, "R" + str(obj.identification))                      
                            
                    for obj in query3: antwort.insert(0, "K" + str(obj.identification))
                       
    try:
        antwort = antwort[index]
    except:
        antwort = -1
    
    try: rechnung_db.close()
    except: True
    try: kunde_db.close()
    except: True

    print("antwort: " + str(antwort))
    
    return antwort

def get_sn_data(sn):
    print("get_sn_data(" + str(sn) + ")")
    answer_list = []
    
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True
    
    query = Lieferschein.select().where(Lieferschein.name.contains("SN:" + sn))
    for delivery in query:
        if delivery.angebot:
            delivery_id = "A" + str(delivery.identification)
        elif delivery.bestellung:
            delivery_id = "B" + str(delivery.identification)
        else:
            delivery_id = "L" + str(delivery.identification)
            
        if not delivery.rechnung == "" and not "RV" in delivery.rechnung:
            if "K" in delivery.rechnung:
                invoice = Kasse.get(Kasse.identification == delivery.rechnung.replace("K",""))            
            else:
                invoice = Rechnung.get(Rechnung.identification == delivery.rechnung.replace("R",""))
                                
            new_data = {"delivery": delivery_id, "date_delivery": delivery.datum, "invoice": str(delivery.rechnung), "date_invoice": invoice.datum, "client_id": invoice.kunde_id, "client_name": invoice.kunde_name}
                    
            answer_list.append(new_data)
                    

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True

    return answer_list

def search_sn_by_dates(date_from, date_to):
    print("search_sn_by_dates(" + str(date_from) + ", " + str(date_to) + ")")
    
    sn_list = []
    date_from = str(date_from)
    date_to = str(date_to)
    
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    selection = Rechnung.select().where(Rechnung.datum.between(date_from, date_to)) + Kasse.select().where(Kasse.datum.between(date_from, date_to))
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)        
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/sn_from_" + date_from + "_to_" + date_to
    answer = "/zk-data/file/files/" + server_name + "/sn_from_" + date_from + "_to_" + date_to
       
    fehler = []
    for invoice in selection:
        #print("invoice: " + str(invoice.identification) + " " + str(invoice.datum))
        for delivery_id in invoice.lieferscheine.split("|"):
            if not delivery_id == "":
                #print("delivery_id: " + str(delivery_id))
                try:
                    delivery = Lieferschein.get(Lieferschein.identification == delivery_id)
                    for line in delivery.name.split("|"):
                        if "SN:" in line:
                            sn = line.split("SN:")[-1]
                            if not sn in sn_list:
                                sn_list.append(sn)
                except:
                    fehler.append(str(invoice.identification) + ": " + str(delivery_id))
                            

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    
    open("/tmp/fehler", "w").write("\n".join(fehler))

    open(pdf_path, "w").write(json.dumps(sn_list))   
    
    return answer
    
def search_invoice_e(identification, dokument_id, lieferant_id, rest, datum, index):# search Rechnung with id, client name/id return ID answer by index
    print("search_invoice_e(" + str(identification) + ", " + str(dokument_id) + ", " + str(lieferant_id) + ", " + str(rest) + ", " + str(datum) + ", " + str(index) + ")")

    identification = str(identification).upper().replace("RE", "")
    dokument_id = libs.BlueFunc.ultra_simple_text(str(dokument_id))
    lieferant_id = str(lieferant_id)
    rest = bool(rest)
    datum = str(datum)
    index = int(index)
    
    antwort = -1
    where_condition = []

    try: rechnung_e_db.connect()
    except: True
    try: kunde_db.connect()
    except: True

    antwort = []

    query = Rechnung_e.select().where(Rechnung_e.identification.contains(identification)).order_by(-Rechnung_e.identification)
    #print("search in " + str(len(query)) + " invoices")
    
    if not dokument_id == "":
        print("Search with dokument_id: " + str(dokument_id))
        query = query.where(Rechnung_e.dokument_id.contains(dokument_id))#.order_by(-Rechnung_e.identification)
        
    if not datum == "":
        print("Search with datum: " + str(datum))
        query = query.where(Rechnung_e.datum.contains(datum))#.order_by(-Rechnung_e.identification)

    if not lieferant_id == "":
        print("Search with lieferant_id: " + str(lieferant_id))
        #clients = []
        if lieferant_id.isdigit():
            query = query.where(Rechnung_e.lieferant_id.contains(lieferant_id))#.order_by(-Rechnung_e.identification)
            #clients.append(lieferant_id)
        else:
            query = query.where(Rechnung_e.lieferant_name.contains(lieferant_id))#.order_by(-Rechnung_e.identification)
            """
            clients_test = Kunde.select().where(Kunde.supplier == True)
            clients_test = clients_test.where(Kunde.name.contains(lieferant_id))
            
            clients_test2 = []
            for k in clients_test: clients_test2.append(k.identification)            
            
            if clients_test2 == []: 
                clients_test2 = [0]
                clients = [0]
            
            for q_rechnung_e in query:
                if q_rechnung_e.lieferant_id in clients_test2:
                    clients.append(q_rechnung_e.lieferant_id)
            """
    if rest:
        print("Search with rest: " + str(rest))
        query = query.where(Rechnung_e.rest != 0.0)#.order_by(-Rechnung_e.identification)
   
    try:
        antwort = query[index].identification #antwort[index]
    except:
        antwort = -1

    try: rechnung_e_db.close()
    except: True
    try: kunde_db.close()
    except: True

    print("antwort: " + str(antwort))
    return antwort
    
def update_delivery_calendar():
    print("update_delivery_calendar()")
    
    try: lieferschein_db.connect()
    except: True
    
    dtstamp = libs.BlueFunc.Date().replace("-", "") + "T" + libs.BlueFunc.uhrzeit().replace(":", "") + "00Z"
    calendar_data = {}
    #calendar_data = {
                    #"KENNY": {
                        #"20240215T000000": {"UID": "date_created+id@zkdata"}
                    #},
                    #{"JEROME": {
                        #11: {}
                    #}
                #}
    date = libs.BlueFunc.TodayAddDays(-10) # 2024-02-04
    query = Lieferschein.select().where(Lieferschein.datum_todo > date)
    if query.exists():
        for delivery in query:
            #print(delivery.datum_todo)
            
            date = delivery.datum_todo.replace("-", "")
            date_created = delivery.datum.replace("-", "")
            
            for user in delivery.user.split("|"):
                if not user == "":
                    #print(user)
                    
                    if not user in calendar_data: calendar_data[user] = {}
                    
                    hour = 0
                    while True:
                        if len(str(hour)) == 1:
                            dtstart = date + "T0" + str(hour) + "0000"
                        else: dtstart = date + "T" + str(hour) + "0000"
                        
                        if dtstart in calendar_data[user]:
                            hour += 1
                        else:
                            calendar_data[user][dtstart] = {}
                            break

                    end_hour = hour + 1
                    if len(str(end_hour)) == 1:
                        dend = date + "T0" + str(end_hour) + "0000"
                    else: dend = date + "T" + str(end_hour) + "0000"
                        
                            
                    calendar_data[user][dtstart]["UID"] = date_created + str(delivery.identification) + "@zk-data"
                    calendar_data[user][dtstart]["SUMMARY"] = str(delivery.identification) + ": " + str(delivery.maschine) + " [" + str(delivery.kunde_name) + "]"
                    calendar_data[user][dtstart]["DESCRIPTION"] = str(delivery.info)
                    calendar_data[user][dtstart]["CLASS"] = "PUBLIC"
                    calendar_data[user][dtstart]["DESCRIPTION"] = str(delivery.info)
                    calendar_data[user][dtstart]["DTSTART"] = str(dtstart) + "Z"
                    calendar_data[user][dtstart]["DTEND"] = str(dend) + "Z"
                    #calendar_data[user][dtstart]["DTSTAMP"] = str(date_created) + "T000000Z"
                    calendar_data[user][dtstart]["DTSTAMP"] = dtstamp
                 
    os.system("rm /var/www/html/zk-data/file/files/" + server_name + "/calendar-*.ics")
        
    for user in calendar_data:
        print("create calendar for " + user)
        if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
            os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        
        file = open("/var/www/html/zk-data/file/files/" + server_name + "/calendar-" + user + ".ics", "w")
        file_data = []
        file_data.append("BEGIN:VCALENDAR")
        file_data.append("VERSION:2.0")
        file_data.append("METHOD:PUBLISH")
        
        for event in calendar_data[user]:
            file_data.append("BEGIN:VEVENT")
            for index_name in calendar_data[user][event]:
                file_data.append(index_name + ":" + calendar_data[user][event][index_name])
            file_data.append("END:VEVENT")
        
        file_data.append("END:VCALENDAR")
        file.write("\n".join(file_data))
    
    try: lieferschein_db.close()
    except: True

#update_delivery_calendar()

def get_inventar(Dict):# return Dict of Inventar
    print("get_inventar(" + str(Dict) + ")")
    try: inventar_db.connect()
    except: True

    Dict["tag"] = str(Dict["tag"])
    Dict["monat"] = str(Dict["monat"])
    Dict["jahr"] = str(Dict["jahr"])
    Dict["start"] = int(Dict["start"])

    where_condition = []
    Antwort = []

    if not Dict["tag"] == "0":
        where_condition.append(Inventar.tag == Dict["tag"])
    if not Dict["monat"] == "0":
        where_condition.append(Inventar.monat == Dict["monat"])

    where_condition.append(Inventar.jahr == Dict["jahr"])
    where_condition.append(Inventar.identification > Dict["start"])

    while True:
        for object in Inventar.select().where(*where_condition).order_by(Inventar.identification):
            Antwort = model_to_dict(object)
            if not Antwort == {}:
                ArtDict = get_article({"identification": Antwort["bcode"]})
                if not ArtDict == {}:
                    Antwort["name_de"] = ArtDict["name_de"]
                    Antwort["name_fr"] = ArtDict["name_fr"]
                    Antwort["preisek"] = round(ArtDict["preisek"], 2)
                    Antwort["preisvk"] = round(ArtDict["preisvk"], 2)
                    Antwort["categorie"] = ArtDict["categorie"]
                else:
                    Antwort["name_de"] = "?"
                    Antwort["preisek"] = 0.0
                    Antwort["preisvk"] = 0.0
                    Antwort["categorie"] = 0
            break
        break

    inventar_db.close()
    return Antwort



def print_inventar(start_jahr, start_monat, start_tag, end_jahr, end_monat, end_tag, anwenden):
    inventar = {}
    tag = int(start_tag)
    monat = int(start_monat)
    jahr = int(start_jahr)
    
    anwenden = bool(anwenden)
    index = 0
    
    while True:
        print(str(tag) + "/" + str(monat) + "/" + str(jahr))

        dic = get_inventar({"tag": tag, "monat": monat, "jahr": jahr, "start": index})
	
        if dic == []:
            if tag == int(end_tag) and monat == int(end_monat) and jahr == int(end_jahr): break
            else:
                tag += 1
                index = 0
        else:
            index = int(dic["identification"])
            art_identification = str(dic["bcode"])
            
            #art_dict = get_article({"identification": art_identification})
            #if not "name_de" in art_dict:
            #    art_dict["name_de"] = ""
                    
            if not art_identification in inventar:
                inventar[art_identification] = {"anzahl": float(dic["anzahl"]), "name": dic["name_de"], "preisek": float(dic["preisek"]), "categorie": dic["categorie"]}
            else:            
                inventar[art_identification]["anzahl"] = float(dic["anzahl"]) + float(inventar[art_identification]["anzahl"])
                
                
        if tag == 32:
            tag = 1
            monat += 1
        if monat == 13:
            monat = 1
            jahr += 1

    
    #print(inventar)

    sep = ";"
    file_first_line = "bcode" + sep + "categorie" + sep + "name" + sep + "anzahl" + sep + "preis" + sep + "total"
    
    #file_data = []
    random_int = Timestamp()
    #categories = []
    
    file = "/var/www/html/zk-data/file/files/" + "inventar_" + str(random_int) + ".csv"
    answer = "/zk-data/file/files/" + "inventar_" + str(random_int) + ".csv"
    open(file, "a").write(file_first_line + "\n")
    data_line = []
    
    for art_identification in inventar:        
        bcode = str(art_identification)
        #dic = get_article(bcode)
        
        name = str(inventar[art_identification]["name"]).replace(sep, "")
        anzahl = str(inventar[art_identification]["anzahl"]).replace(".", ",")
        preis = str(inventar[art_identification]["preisek"]).replace(".", ",")
        categorie = str(inventar[art_identification]["categorie"])
        if categorie == '': categorie = "0"
        
        #file = "/var/www/html/zk-data/file/files/" + "inventar_" + str(random_int) + ".csv" #+ "_" + str(categorie) + ".csv"
        
        if True:#if not preis == "0,0" and not anzahl == "0,0":    
            total = str(float(preis.replace(",", "."))*float(anzahl.replace(",", ".")) ).replace(".", ",")
            data = str(bcode) + sep + categorie + sep + name + sep + anzahl + sep + preis + sep + total

            data_line.append(data)
            
            #if not categorie in categories: categories.append(categorie)
    
    open(file, "a").write("\n".join(data_line))
    
    #categories = sorted(categories)
    #categories = "|".join(categories)
    
    try: stock_db.connect()
    except: True

    if anwenden:        
        query = Artikel.select().order_by(Artikel.identification)        
        alle_artikel = []
        
        for object in query:
            alle_artikel.append(model_to_dict(object))  
                      
        try: stock_db.close()
        except: True
    
        print(str(len(alle_artikel)) + " articles")
        #print(str(art_list))
        for art_dict in alle_artikel:
            alte_anzahl = art_dict["anzahl"]
            neue_anzahl = 0.0
            identification = str(art_dict["identification"])
            
            if identification in inventar:
                print(str(identification) + " im Inventar")
                neue_anzahl = inventar[identification]["anzahl"]
            
            diff = neue_anzahl - alte_anzahl            
            if not diff == 0.0:
                add_article({"identification": identification, "user": "INVENTAR " + str(end_jahr) + "-" + str(end_monat) + "-" + str(end_tag), "price": 0.0, "add": diff})
        
    return answer

def create_user(username, pin, secure_key, admin):
    print("create_user(" + str(username) + ", " + str(pin) + ", " + str(secure_key) + ", " +  str(admin) + ")")
    try: user_db.connect()
    except: True

    username = str(username).upper()
    pin = str(pin).upper()
    secure_key = str(secure_key)
    try: admin = bool(admin)
    except: admin = False

    query = User.select().where(User.name == str(username))

    if not query.exists():
        print("Create USER: " + str(username))

        CreateThis = User.create(   name = username,
                                    color = "red",
                                    pin = pin,
                                    secure_key = secure_key,
                                    admin = admin,
                                    session = "",
                                    timeout = 3600,
                                    permission_delivery = 2,
                                    permission_delivery_e = 2,
                                    permission_invoice = 2,
                                    permission_invoice_e = 2,
                                    permission_article = 2,
                                    permission_client = 2,
                                    permission_inventory = 2,
                                    permission_stats = 2,
                                    permission_cashbook = 2
                                )
                      
    
    #CreateThis.save()

    try: user_db.close()
    except: True

    return True

def getUserList():
    print("getUserList()")
    try: user_db.connect()
    except: True

    query = User.select()

    antwort = []
    for eachUser in query:
        antwort.append(eachUser.name.upper())

    try: user_db.close()
    except: True
    
    return sorted(antwort)

def getUserData_from_key(key):
    print("getUserData(" + str(key) + ")")
    try: user_db.connect()
    except: True

    try:
        antwort = model_to_dict(User.get(secure_key=key))
    except: antwort = {}

    try: user_db.close()
    except: True
    
    return antwort
    
def getUserData(user_name):
    print("getUserData(" + str(user_name) + ")")
    try: user_db.connect()
    except: True

    user_name = str(user_name).upper()

    try:
        antwort = model_to_dict(User.get(name=user_name))
        
    except: antwort = {}

    try: user_db.close()
    except: True
    
    return antwort

def setUserData(Dict): # NOT USED
    print("setUserData(" + str(Dict) + ")")
    try: user_db.connect()
    except: True

    try:
        ThisUser = dict_to_model(User, Dict)
        ThisUser.save()
        antwort = True
    except: antwort = False

    print("setUserData(" + str(Dict) + "): " + str(antwort))
    
    try: user_db.close()
    except: True
    
    return antwort

def set_user_page_settings(username, Dict):
    print("set_user_page_settings(" + str(username) + ", " + str(Dict) + ")")
    username = str(username)
    if "username" in Dict:
        del Dict["username"]
    for key in Dict:
        content = Dict[key]
        libs.BlueFunc.set_data(username + "_" + str(key), content, "/etc/zk-data-libs/database/server_user_page_settings.db")

    return Dict
            
def get_user_page_settings(username, Dict):
    print("get_user_page_settings(" + str(username) + ", " + str(Dict) + ")")
    username = str(username)
    if "username" in Dict:
        del Dict["username"]
    for key in Dict:
        content = libs.BlueFunc.get_data(username + "_" + str(key), "/etc/zk-data-libs/database/server_user_page_settings.db")
        Dict[key] = content
            
    return Dict
            
def print_rappel(client):
    print("print_rappel(" + str(client) + ")")
    
    invoices = []
    timestamp = libs.BlueFunc.timestamp()
    
    client_dict = get_client(client)
    if client == {}: # Error Client doesn't exists !
        client_dict["sprache_de"] = False
        client_dict["sprache_fr"] = True
        client_dict["identification"] = client
        client_dict["name"] = ""
        client_dict["adresse"] = ""
        client_dict["land"] = ""
        client_dict["ort"] = ""
        client_dict["plz"] = ""
        client_dict["tel1"] = ""
        client_dict["tel2"] = ""
        client_dict["tva"] = ""
        
    tmp_dir = "/tmp/zk-data_server_" + str(timestamp)
    if not os.path.exists(tmp_dir): os.mkdir(tmp_dir)
    
    if client_dict["sprache_de"]: # Deutsch
        if not os.path.exists("/tmp/" + server_name + "/VorlageRappel_DE.ods"):
            os.system("unzip -q " + data_dir + "VorlageRappel_DE.ods -d /tmp/" + server_name + "/VorlageRappel_DE.ods/")
        os.system("cp -r /tmp/" + server_name + "/VorlageRappel_DE.ods/* " + tmp_dir)
    else: # Français
        if not os.path.exists("/tmp/" + server_name + "/VorlageRappel_FR.ods"):
            os.system("unzip -q " + data_dir + "VorlageRappel_FR.ods -d /tmp/" + server_name + "/VorlageRappel_FR.ods/")
        os.system("cp -r /tmp/" + server_name + "/VorlageRappel_FR.ods/* " + tmp_dir)
        
        
    page_data = open(tmp_dir + "/content.xml", "r").read()
    last_line_number = 0
    while "@" + str(last_line_number + 1) + "_identification@" in page_data: 
        last_line_number = last_line_number + 1
    
    for x in range(0, last_line_number + 1):
        invoices.append({})
        
        invoice_id = search_invoice("", client, True, "", x)
        
        if not str(invoice_id) == "-1":        
            invoice_dict = get_invoice(invoice_id)
            txt = "datum"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", libs.BlueFunc.date_reversed(invoice_dict[txt]))  
            txt = "identification"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", str(invoice_dict[txt]))  
            txt = "rappel"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", str(int(invoice_dict[txt])))
            
            r_date = str(DateAddDays(invoice_dict["datum"], 20))
            reverse_date = libs.BlueFunc.date_reversed(r_date)   
            page_data = page_data.replace("@" + str(x) + "_datum_todo@", reverse_date)
                                                    
            txt = "total"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", str(invoice_dict[txt]))
            txt = "rest"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", str(invoice_dict[txt]))
            
            ## wenn fällig
            if libs.BlueFunc.TodayAddDays(1) > invoice_dict["datum_todo"]:
                set_invoice_rappel(invoice_id)
        else:
            txt = "datum"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            txt = "identification"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            txt = "rappel"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            txt = "datum_todo"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            txt = "total"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            txt = "rest"; page_data = page_data.replace("@" + str(x) + "_" + txt + "@", "")
            
    txt = "date"
    if txt in page_data: page_data = page_data.replace("@" + txt + "@", libs.BlueFunc.date_reversed(libs.BlueFunc.Date()))            
    
    page_data = page_data.replace("@kunde_identification@", str(client_dict["identification"]))
    page_data = page_data.replace("@kunde_name@", client_dict["name"])
    page_data = page_data.replace("@kunde_adr@", client_dict["adresse"])
    page_data = page_data.replace("@kunde_land@", client_dict["land"])
    page_data = page_data.replace("@kunde_plz@", client_dict["plz"])
    page_data = page_data.replace("@kunde_ort@", client_dict["ort"])
    page_data = page_data.replace("@kunde_tva@", client_dict["tva"])
      
    open(tmp_dir + "/content.xml", "w").write(page_data)
    os.system("cd " + tmp_dir + " && zip -r -q dokument.ods * && libreoffice --convert-to pdf:writer_pdf_Export dokument.ods && mv dokument.pdf /var/www/html/zk-data/file/files/" + str(timestamp) + ".pdf && rm -r " + tmp_dir)
                  
    return str(timestamp) + ".pdf"

def create_delivery_pdf(identification):
    print("create_delivery_pdf(" + str(identification) + ")")
    
    delivery = get_delivery(identification)
    client = get_client(delivery["kunde_id"])
    
    if bool(client["sprache_de"]): default_ods_file = "/etc/zk-data-server/data/" + server_name + "/VorlageLieferschein_DE.ods"
    else: default_ods_file = "/etc/zk-data-server/data/" + server_name + "/VorlageLieferschein_FR.ods"  

    # get new file data
    data = libs.ServerTools.get_ods_content(server_name, default_ods_file)
    
    max_index_on_page = 0    
    while "@" + str(max_index_on_page) + "_name@" in data: max_index_on_page +=1  
    
    max_index_on_delivery = len(delivery["bcode"].split("|")) 
    
    pages = round((max_index_on_delivery) / max_index_on_page, 1)
    if str(pages).split(".")[-1] == "0": pages = int(pages)
    else: pages = int(pages) + 1
        
    list_of_files = []
    for page in range(1, pages+1):        
        # get new file data
        data = libs.ServerTools.get_ods_content(server_name, default_ods_file)
    
        # Kopfzeile
        
        data = data.replace("@kunde_identification@", str(client["identification"]))
        data = data.replace("@kunde_name@", client["name"])
        data = data.replace("@kunde_adr@", client["adresse"])
        data = data.replace("@kunde_land@", client["land"])
        data = data.replace("@kunde_ort@", client["ort"])
        data = data.replace("@kunde_plz@", client["plz"])
        data = data.replace("@kunde_tel1@", client["tel1"])
        data = data.replace("@kunde_tel2@", client["tel2"])
        data = data.replace("@kunde_tva@", client["tva"])
        
        if bool(client["sprache_de"]):
            data = data.replace("@kunde_sprache_de@", "DE")
        else:
            data = data.replace("@kunde_sprache_de@", "")
            
        if bool(client["sprache_fr"]):
            data = data.replace("@kunde_sprache_fr@", "FR")
        else:
            data = data.replace("@kunde_sprache_fr@", "")
        
        if bool(client["sprache_nl"]):
            data = data.replace("@kunde_sprache_nl@", "NL")
        else:
            data = data.replace("@kunde_sprache_nl@", "")
        
        if pages == 1: data = data.replace("@seite@", "")        
        else: data = data.replace("@seite@", str(page) + "/" + str(pages))
        data = data.replace("@identification@", str(identification))  
        
        # info        
        info_line_for_libreoffice = ""
        for new_info_line in delivery["info"].split("\n"):
            info_line_for_libreoffice = info_line_for_libreoffice + "<text:p>" + new_info_line + "</text:p>"
        info_line_for_libreoffice = info_line_for_libreoffice[8:] #remove first <text:p>
        info_line_for_libreoffice = info_line_for_libreoffice[:-9] #remove last <text:p>
        data = data.replace("@info@", info_line_for_libreoffice)            
        
            
        data = data.replace("@maschine@", delivery["maschine"])
        data = data.replace("@date@", libs.BlueFunc.date_reversed(delivery["datum"])) 
        
        data = data.replace("@total@", str(libs.RoundUp.RoundUp00_as_string(delivery["total"])).replace(".", ","))
        data = data.replace("@total_htva@", str(delivery["total_htva"]).replace(".", ","))
        
        for part_of_steuern in delivery["steuern"].split("|"):
            if ";" in part_of_steuern:
                tva_satz = part_of_steuern.split(";")[0]
                tva_wert = part_of_steuern.split(";")[1].replace(".", ",")
                data = data.replace("@total_tva_" + tva_satz + "@", tva_wert)                
        for tva_satz in get_all_mwst(): data = data.replace("@total_tva_" + str(tva_satz) + "@", "0.0")
                
        
        if bool(client["sprache_de"]): # DE
            if bool(delivery["angebot"]): data = data.replace("@name@", "Angebot")
            elif bool(delivery["bestellung"]): data = data.replace("@name@", "Bestellschein")
            else: data = data.replace("@name@", "Lieferschein")
            
            if bool(delivery["fertig"]): data = data.replace("@fertig@", "")
            else: data = data.replace("@fertig@", "--++ NICHT FERTIG ++--")
        else: # FR
            if bool(delivery["angebot"]): data = data.replace("@name@", "Offre")
            elif bool(delivery["bestellung"]): data = data.replace("@name@", "Bon de commande")
            else: data = data.replace("@name@", "Bon de livraison")
            
            if bool(delivery["fertig"]): data = data.replace("@fertig@", "")
            else: data = data.replace("@fertig@", "--++ PAS FINI ++--")
        
        for index_on_page in range(0, max_index_on_page):         
            index_on_delivery = index_on_page + ((page-1)*max_index_on_page)
                            
            def write_part(data, key):
                try: value = str(delivery[key].split("|")[index_on_delivery])
                except: value = ""                
                if key in ["anzahl", "preis", "preis_htva", "tva"]: value = str(value).replace(".", ",")
                #fix for LibreOffice
                value = str(value).replace("<", "").replace(">", "").replace("&", "")
                
                data = data.replace("@" + str(index_on_page) + "_" + key + "@", value)
                return data
                              
            for i in ["bcode", "name", "anzahl", "preis", "preis_htva", "tva"]:
                data = write_part(data, i)
                
            try:
                value = round(float(delivery["anzahl"].split("|")[index_on_delivery]) * float(delivery["preis_htva"].split("|")[index_on_delivery]), 2)
                value = str(value).replace(".", ",")
            except: value = ""
            data = data.replace("@" + str(index_on_page) + "_preistotal_htva@", value)
        
        # write file
        new_file = libs.ServerTools.write_ods_file(server_name, default_ods_file, data, [])
        list_of_files.append(new_file)
       
    new_pdf = libs.ServerTools.convert_ods_to_pdf(list_of_files)
    for f in list_of_files: os.system("rm " + f)
    print("new_pdf: " + str(new_pdf))

    return new_pdf

def remove_unused_articles():
    print("remove_unused_articles()")
    
    # WARNING ALSO DISABLE STATS ???
    
    try: stock_db.connect()
    except: True
    
    timestamp = Timestamp() - 86400 # older than 1 day
    counter = 0
    
    query = Artikel.select().where(Artikel.name_de == "").where(Artikel.name_fr == "").where(Artikel.lieferant == "").where(Artikel.anzahl == 0).where(Artikel.artikel == "").where(Artikel.artikel2 == "").where(Artikel.lastchange < timestamp)
    if query.exists():
        for unused_article in query:
            print("remove unused_article: " + str(unused_article.identification))
            unused_article.delete_instance()
            counter += 1
            
    print("deleted " + str(counter) + " unused articles")
    try: stock_db.close()
    except: True

def remove_unused_deliverys():
    print("remove_unused_deliverys()")
    
    try: lieferschein_db.connect()
    except: True
    
    datum = Date() # not today
    counter = 0
    
    query = Lieferschein.select().where(Lieferschein.rechnung == "").where(Lieferschein.total == 0).where(Lieferschein.bcode == "").where(Lieferschein.name == "").where(Lieferschein.info == "").where(Lieferschein.kunde_id == 0).where(Lieferschein.maschine == "").where(Lieferschein.datum < datum)
    if query.exists():
        for unused_delivery in query:
            print("remove unused_delivery: " + str(unused_delivery.identification))
            unused_delivery.delete_instance()
            counter += 1
            
    # [DELETE]
    query = Lieferschein.select().where(Lieferschein.name.contains("[DELETE]"))#.where(Lieferschein.rechnung == "").
    if query.exists():
        for unused_delivery in query:
            print("remove unused_delivery: " + str(unused_delivery.identification))
            unused_delivery.delete_instance()
            counter += 1
            
    print("deleted " + str(counter) + " unused delvierys")
    
    try: lieferschein_db.close()
    except: True
    
    return str(counter)

def create_invoice_pdf(identification):
    print("create_invoice_pdf(" + str(identification) + ")")
    
    invoice = get_invoice(identification)
    client = get_client(invoice["kunde_id"])
    if client == {}: # Error Client doesn't exists !
        client["sprache_de"] = False
        client["sprache_fr"] = True
        client["identification"] = invoice["kunde_id"]
        client["name"] = ""
        client["adresse"] = ""
        client["land"] = ""
        client["ort"] = ""
        client["plz"] = ""
        client["tel1"] = ""
        client["tel2"] = ""
        client["tva"] = ""
        client["payment_deadline"] = 0
    
    if not "rest" in invoice: invoice["rest"] = invoice["total"]
    if not "ref" in invoice: invoice["ref"] = str(identification) + "/" + str(client["identification"])

    deliverys = {}
    for i in ["bcode", "name", "anzahl", "preis", "preis_htva", "preistotal_htva", "tva"]: deliverys[i] = []
            
    for delivery_id in invoice["lieferscheine"].split("|"):
        print("delivery_id: " + str(delivery_id))  
        
        delivery_dict = get_delivery(delivery_id)
        if not delivery_dict == {}:
            #if not "datum" in delivery_dict: delivery_dict["datum"] = "?"
            #if not "maschine" in delivery_dict: delivery_dict["maschine"] = "?"
            
            deliverys["bcode"].append("")
            deliverys["name"].append("--- L" + delivery_id + " --- " + delivery_dict["datum"] + " --- " + delivery_dict["maschine"] + " ---")
            deliverys["anzahl"].append("")
            deliverys["preis"].append("")
            deliverys["preis_htva"].append("")
            deliverys["preistotal_htva"].append("")
            deliverys["tva"].append("")
                                    
            for a in range(0, len(delivery_dict["bcode"].split("|"))):
                for i in ["bcode", "name", "anzahl", "preis", "preis_htva", "preistotal_htva", "tva"]:
                    # empty artikel ?
                    if delivery_dict["bcode"].split("|")[a] == "" and delivery_dict["name"].split("|")[a] == "":
                        deliverys[i].append("")                
                    else:
                        if i == "preistotal_htva":
                            value = round(float(delivery_dict["anzahl"].split("|")[a]) * float(delivery_dict["preis_htva"].split("|")[a]), 2)
                            deliverys[i].append(value)                    
                        else:
                            deliverys[i].append(delivery_dict[i].split("|")[a])
    print("deliverys: " + str(deliverys))  
    
    if bool(client["sprache_de"]): default_ods_file = "/etc/zk-data-server/data/" + server_name + "/VorlageRechnung_DE.ods"
    else: default_ods_file = "/etc/zk-data-server/data/" + server_name + "/VorlageRechnung_FR.ods"

    # get new file data
    data = libs.ServerTools.get_ods_content(server_name, default_ods_file)
    
    max_index_on_page = 0    
    while "@" + str(max_index_on_page) + "_name@" in data: max_index_on_page +=1  
    
    max_index_on_invoice = len(deliverys["bcode"]) 
    
    pages = round((max_index_on_invoice) / max_index_on_page, 1)
    if str(pages).split(".")[-1] == "0": pages = int(pages)
    else: pages = int(pages) + 1
        
    image_list = []
    # QR Codes
    if "@image_zk_invoice@" in data:
        path_of_old_image = data.split("@image_zk_invoice@")[1]
        path_of_old_image = path_of_old_image.split('xlink:href="')[1]
        path_of_old_image = path_of_old_image.split('" ')[0]
        # create QR Code
        path_of_new_image = "/tmp/image_zk_invoice" + str(client["identification"]) + ".png"
        if True:#if not os.path.exists(path_of_new_image):
            if bool(client["sprache_de"]):
                language = "de"
            else:
                language = "fr"
            if client["email3"] == "":
                mail = ""
                send_mail = "no"
            else:
                mail = client["email3"]
                send_mail = "yes"
                
            print("Create QR Code for client " + str(client["identification"]))
            os.system("qrencode -s 6 -l H -o \"" + path_of_new_image + "\" \"https://zk-data.zaunz.org/invoice?mail_to=rechnung@burkardt.be&client_id=" + str(client["identification"]) + "&client_name=" + client["name"] + "&language=" + language + "&mail=" + mail + "&send_mail=" + send_mail + "\"")
        image_list.append([path_of_old_image, path_of_new_image])
        
        
    if "@image_zk_invoice_payment@" in data:
        path_of_old_image = data.split("@image_zk_invoice_payment@")[1]
        path_of_old_image = path_of_old_image.split('xlink:href="')[1]
        path_of_old_image = path_of_old_image.split('" ')[0]
        # create QR Code
        path_of_new_image = "/tmp/image_zk_invoice_payment" + str(identification) + ".png"
        if True:#if not os.path.exists(path_of_new_image):                
            print("Create QR Code for invoice " + str(identification))
            #<TODO>
            name = "BURKARDT SRL"
            iban = "BE29733062810964"
            bic = "KREDBEBB"
            
            os.system("qrencode -s 6 -l H -o \"" + path_of_new_image + "\" \"BCD\n001\n1\nSCT\n" + bic + "\n" + name + "\n" + iban + "\nEUR" + str(invoice["rest"]) + "\n\nQR " + str(invoice["ref"]) + "\"")

        image_list.append([path_of_old_image, path_of_new_image])
    
            
    list_of_files = []
    for page in range(1, pages+1):        
        # get new file data
        data = libs.ServerTools.get_ods_content(server_name, default_ods_file)
    
        # Kopfzeile
        data = data.replace("@kunde_identification@", str(client["identification"]))
        data = data.replace("@kunde_name@", client["name"])
        data = data.replace("@kunde_adr@", client["adresse"])
        data = data.replace("@kunde_land@", client["land"])
        data = data.replace("@kunde_ort@", client["ort"])
        data = data.replace("@kunde_plz@", client["plz"])
        data = data.replace("@kunde_tel1@", client["tel1"])
        data = data.replace("@kunde_tel2@", client["tel2"])
        data = data.replace("@kunde_tva@", client["tva"])
        
        if pages == 1: data = data.replace("@seite@", "")        
        else: data = data.replace("@seite@", str(page) + "/" + str(pages))
        data = data.replace("@identification@", str(identification))        
        
        if "RV" in identification:
            data = data.replace("@datum_todo@", "")
        else: 
            data = data.replace("@datum_todo@", libs.BlueFunc.date_reversed(invoice["datum_todo"]))
            
        data = data.replace("@rest@", str(invoice["rest"]))
        data = data.replace("@ref@", invoice["ref"])
            
        data = data.replace("@date@", libs.BlueFunc.date_reversed(invoice["datum"])) 
        
        data = data.replace("@total@", str(libs.RoundUp.RoundUp00_as_string(invoice["total"])).replace(".", ","))
        data = data.replace("@total_htva@", str(libs.RoundUp.RoundUp00_as_string(invoice["total_htva"])).replace(".", ","))
                           
        try: payment_deadline = libs.BlueFunc.DateAddDays(invoice["datum"], int(client["payment_deadline"]))
        except: payment_deadline = invoice["datum"]
        payment_deadline = libs.BlueFunc.date_reversed(payment_deadline)
        data = data.replace("@payment_deadline@", payment_deadline)
        
        # info
        info = ""
        ## Innergemeinschaftliche Lieferung ohne Mehrwertsteuer nach Artikel 39 dem code der Mehrwertsteuer
        if not client["tva"] == "" and not "BE" in client["tva"]:
            if not invoice["total"] == "0.0" and not invoice["total"] == invoice["total_htva"]:
                print("Innergemeinschaftliche Lieferung ohne Mehrwertsteuer nach Artikel 39 dem code der Mehrwertsteuer")
                print("sprache_de " + str(client["sprache_de"]))
                if client["sprache_de"] == False or client["sprache_de"] == 0:## = Français
                    info = info + "\n##########################################################"
                    info = info + "\n#                   Livraison intracommunautaire exempt de TVA                   #"
                    info = info + "\n#                          suite à l'article 39 du code de la TVA                           #"
                    info = info + "\n##########################################################\n"
                else:
                    info = info + "\n##########################################################"
                    info = info + "\n#           Innergemeinschaftliche Lieferung ohne Mehrwertsteuer           #"
                    info = info + "\n#              nach Artikel 39 dem code der Mehrwertsteuer                       #"
                    info = info + "\n##########################################################\n"
        ####################################################################################################
        
        transaktion_list = search_transaktion_by_rechnung(identification)
        transaktion_list = transaktion_list.split("|")
        print("transaktion_list: " + str(transaktion_list))
        if not transaktion_list[0] == "":
            for transaktion_id in transaktion_list:
                transaktion = get_transaktion(transaktion_id)
                info = info + "T" + str(transaktion_id) + ": " + transaktion["datum"] + " - " + transaktion["uhrzeit"] + " " + transaktion["konto_name"] + " " + str(transaktion["betrag"]) + "€\n"
            info = info + "-----------------------------------------------\n"
            if bool(client["sprache_de"]) == True:
                info = info + "Restbetrag: " + str(invoice["rest"]) + " €\n"    
            else:            
                info = info + "Reste à payé: " + str(invoice["rest"]) + " €\n" 
        print("info: " + info)                        
        info_line_for_libreoffice = ""
        for new_info_line in info.split("\n"):
            info_line_for_libreoffice = info_line_for_libreoffice + "<text:p>" + new_info_line + "</text:p>"
        info_line_for_libreoffice = info_line_for_libreoffice[8:] #remove first <text:p>
        info_line_for_libreoffice = info_line_for_libreoffice[:-9] #remove last <text:p>
        data = data.replace("@info@", info_line_for_libreoffice)  
        
        for part_of_steuern in invoice["steuern"].split("|"):
            if ";" in part_of_steuern:
                tva_satz = str(float(part_of_steuern.split(";")[0]))
                tva_wert = part_of_steuern.split(";")[1].replace(".", ",")
                data = data.replace("@total_tva_" + tva_satz + "@", tva_wert)
        for tva_satz in get_all_mwst(): data = data.replace("@total_tva_" + str(tva_satz) + "@", "0.0")
        
        for part_of_steuern in invoice["total_ohne_steuern"].split("|"):
            if ";" in part_of_steuern:
                tva_satz = str(float(part_of_steuern.split(";")[0]))
                tva_wert = part_of_steuern.split(";")[1].replace(".", ",")
                data = data.replace("@total_ohne_steuern_" + tva_satz + "@", tva_wert)
        for tva_satz in get_all_mwst(): data = data.replace("@total_ohne_steuern_" + str(tva_satz) + "@", "0.0")
                
              
        if "RV" in identification:     
            if bool(client["sprache_de"]): data = data.replace("@name@", "Rechnungsvorlage")
            else: data = data.replace("@name@", "Préfacture")
        else:
            if float(invoice["total"]) < 0.0: # Gutschrift
                if bool(client["sprache_de"]): data = data.replace("@name@", "Gutschrift")
                else: data = data.replace("@name@", "Note de crédit")            
            else:
                if "R" in identification:
                    if bool(client["sprache_de"]): data = data.replace("@name@", "Rechnung")
                    else: data = data.replace("@name@", "Facture")
                else:
                    if bool(client["sprache_de"]): data = data.replace("@name@", "Kassenverkauf")
                    else: data = data.replace("@name@", "Vente")
        
        for index_on_page in range(0, max_index_on_page):         
            index_on_invoice = index_on_page + ((page-1)*max_index_on_page)
                            
            def write_part(data, key):
                #try: value = str(deliverys[key][index_on_page])
                try: value = str(deliverys[key][index_on_invoice])
                except: value = ""                
                if key in ["anzahl", "preis", "preis_htva", "preistotal_htva", "tva"]: value = str(value).replace(".", ",")
                data = data.replace("@" + str(index_on_page) + "_" + key + "@", value)
                return data                
            
            for i in ["bcode", "name", "anzahl", "preis", "preis_htva", "preistotal_htva", "tva"]:            
                data = write_part(data, i)
        
        # write file
        new_file = libs.ServerTools.write_ods_file(server_name, default_ods_file, data, image_list)
        list_of_files.append(new_file)
       
    # VorlageVerkaufsbedingugen.pdf
    list_of_files.append("/etc/zk-data-server/data/" + server_name + "/VorlageVerkaufsbedingungen.pdf")
       
    new_pdf = libs.ServerTools.convert_ods_to_pdf(list_of_files)
    for f in list_of_files:
        if not "VorlageVerkaufsbedingungen.pdf" in f:
            os.system("rm " + f)
    print("new_pdf: " + str(new_pdf))
    return new_pdf
    
def get_invoice_pdf(identification):
    print("get_invoice_pdf(" + str(identification) + ")")

    try: rechnung_db.connect()
    except: True

    identification = str(identification).replace(".0", "")
    
    if "RV" in identification:
        query = RechnungVorlage.select().where(RechnungVorlage.identification == int(identification.replace("RV", "")))
    elif "K" in identification:
        query = Kasse.select().where(Kasse.identification == int(identification.replace("K", "")))
    else:
        query = Rechnung.select().where(Rechnung.identification == int(identification.replace("R", "")))
    
    if query.exists():
        datum = query[0].datum
        date_list = datum.split("-")

        new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/invoice/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
        if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
        new_pdf_filename = new_pdf_path + str(identification) + ".pdf"
        
        if not os.path.exists(new_pdf_filename):
            print("PDF Doesn't exist -> create it")
            new_tmp_pdf_path = create_invoice_pdf(identification) # create pdf in /tmp
            os.system("mv " + new_tmp_pdf_path + " " + new_pdf_filename)
            
        client_destination_path = libs.ServerTools.export_to_web_tmp(new_pdf_filename)
        answer = client_destination_path
    else:
        answer = False
             
    try: rechnung_db.close()
    except: True
    
    '''
    try: rechnung_pdf_db.connect()
    except: True
    try: rechnung_db.connect()
    except: True
    
    if not os.path.exists("/var/www/html/zk-data/Rechnung/" + server_name):
        os.system("mkdir -p  /var/www/html/zk-data/Rechnung/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/Rechnung/" + server_name)

    pdf_path = "/var/www/html/zk-data/Rechnung/" + server_name + "/" + str(identification) + ".pdf"
    answer = "/zk-data/Rechnung/" + server_name + "/" + str(identification) + ".pdf"
    
    if "RV" in str(identification):
        query_invoice = RechnungVorlage.select().where(RechnungVorlage.identification == str(identification).replace("RV", ""))
    elif "K" in str(identification):
        query_invoice = Kasse.select().where(Kasse.identification == str(identification).replace("K", ""))
    else:
        query_invoice = Rechnung.select().where(Rechnung.identification == str(identification).replace("R", ""))
        
    if query_invoice.exists():
        query_pdf = Rechnung_pdf.select().where(Rechnung_pdf.identification == str(identification))    
        if query_pdf.exists():
            print("PDF exist")
            if not os.path.exists(pdf_path):
                open(pdf_path, "wb").write(query_pdf[0].pdf)
        else:
            print("PDF Doesn't exist -> create it")
            new_pdf_path = create_invoice_pdf(identification) # create pdf in /tmp
            pdf_blob = open(new_pdf_path, "rb").read() # read pdf from /tmp
            new_pdf = Rechnung_pdf.create(identification = identification, pdf = pdf_blob) # write it to database
            open(pdf_path, "wb").write(query_pdf[0].pdf) # write pdf to /var/www/html/zk-data/Lieferscheine/
    else:
        answer = ""
        
    try: rechnung_db.close()
    except: True
    try: rechnung_pdf_db.close()
    except: True
    '''
    
    return answer

def get_delivery_pdf(identification):
    print("get_delivery_pdf(" + str(identification) + ")")

    try: lieferschein_db.connect()
    except: True

    identification = int(str(identification).replace(".0", ""))
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        datum = query[0].datum
        date_list = datum.split("-")

        new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/delivery/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
        if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
        new_pdf_filename = new_pdf_path + "L" + str(identification) + ".pdf"
        
        if not os.path.exists(new_pdf_filename):
            print("PDF Doesn't exist -> create it")
            new_tmp_pdf_path = create_delivery_pdf(identification) # create pdf in /tmp
            os.system("mv " + new_tmp_pdf_path + " " + new_pdf_filename)
            
        client_destination_path = libs.ServerTools.export_to_web_tmp(new_pdf_filename)
        answer = client_destination_path
    else:
        answer = False
             
    try: lieferschein_db.close()
    except: True
    


    
    '''
    try: lieferschein_pdf_db.connect()
    except: True
    
    identification = str(identification).replace(".0", "")
    
    if not os.path.exists("/etc/zk-data-server/web/tmp/Lieferscheine/" + server_name):
        os.system("mkdir -p  /etc/zk-data-server/web/tmp/Lieferscheine/" + server_name)
        os.system("chmod -R 777 /etc/zk-data-server/web/tmp/Lieferscheine/" + server_name)

    pdf_path = "/etc/zk-data-server/web/tmp/Lieferscheine/" + server_name + "/L" + str(identification) + ".pdf"
    answer = "/tmp/Lieferscheine/" + server_name + "/L" + str(identification) + ".pdf"
    
    query_pdf = Lieferschein_pdf.select().where(Lieferschein_pdf.identification == str(identification))    
    if query_pdf.exists():
        print("PDF exist")
        if not os.path.exists(pdf_path):
            open(pdf_path, "wb").write(query_pdf[0].pdf)           
    else:
        print("PDF Doesn't exist -> create it")
        new_pdf_path = create_delivery_pdf(identification) # create pdf in /tmp
        pdf_blob = open(new_pdf_path, "rb").read() # read pdf from /tmp
        new_pdf = Lieferschein_pdf.create(identification = identification, pdf = pdf_blob) # write it to database
        open(pdf_path, "wb").write(query_pdf[0].pdf) # write pdf to /var/www/html/zk-data/Lieferscheine/
        
    try: lieferschein_pdf_db.close()
    except: True
    '''
    
    return answer

def delete_delivery_pdf(identification):
    print("delete_delivery_pdf(" + str(identification) + ")")

    identification = int(identification)
    
    try: lieferschein_db.connect()
    except: True
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        datum = query[0].datum
        date_list = datum.split("-")

        pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/delivery/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
        pdf_file = pdf_path + "L" + str(identification) + ".pdf"
        
        if os.path.exists(pdf_file): os.system("rm " + pdf_file)
       
    try: lieferschein_db.close()
    except: True
    
    '''
    try: lieferschein_db.connect()
    except: True
    try: lieferschein_pdf_db.connect()
    except: True
    
    delivery = Lieferschein.get(Lieferschein.identification == identification)
    if "RV" in delivery.rechnung:
        delete_invoice_pdf(delivery.rechnung)
    
    pdf_path = "/var/www/html/zk-data/Lieferschein/" + server_name + "/L" + identification + ".pdf"
    
    query_pdf = Lieferschein_pdf.select().where(Lieferschein_pdf.identification == str(identification))      
    if query_pdf.exists():
        query_pdf[0].delete_instance()
        
    if os.path.exists(pdf_path): os.system("rm " + pdf_path)
    
    try: lieferschein_db.close()
    except: True
    try: lieferschein_pdf_db.close()
    except: True
    '''
    
    return True
    
def delete_invoice_pdf(identification):
    print("delete_invoice_pdf(" + str(identification) + ")")

    identification = str(identification)
    
    try: invoice_db.connect()
    except: True
    
    if "RV" in identification:
        query = RechnungVorlage.select().where(RechnungVorlage.identification == int(identification.replace("RV", "")))    
    elif "K" in identification:
        query = Kasse.select().where(Kasse.identification == int(identification.replace("K", "")))     
    else:
        query = Rechnung.select().where(Rechnung.identification == int(identification.replace("R", ""))) 
        
    if query.exists():
        datum = query[0].datum
        date_list = datum.split("-")

        pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/invoice/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
        pdf_file = pdf_path + str(identification) + ".pdf"
        
        if os.path.exists(pdf_file): os.system("rm " + pdf_file)
    
    try: invoice_db.close()
    except: True
    
    '''
    try: rechnung_pdf_db.connect()
    except: True
    
    query_pdf = Rechnung_pdf.select().where(Rechnung_pdf.identification == str(identification))      
    if query_pdf.exists():
        query_pdf[0].delete_instance()
        
    pdf_path = "/var/www/html/zk-data/Rechnung/" + server_name + "/" + identification + ".pdf"
    if os.path.exists(pdf_path): os.system("rm " + pdf_path)
    
    try: rechnung_pdf_db.close()
    except: True
    '''
    
    return True

"""
def getFreieBewegungID():
    print("getFreieBewegungID()")
    global FreieBewegungID
    try: stock_db.connect()
    except: True

    start_timestamp = Timestamp()

    if FreieBewegungID == 0: # Speedup first Start
        last_move = Bewegung.select()
        if len(last_move) > 0:
            last_move = last_move.order_by(Bewegung.datum.desc()).get()
            print("last_move: (by date)" + str(last_move.identification))
            FreieBewegungID = int(last_move.identification)
        else:
            FreieBewegungID = 0

    while True:
        query = Bewegung.select().where(Bewegung.identification == str(FreieBewegungID))
        if not query.exists(): break
        FreieBewegungID = FreieBewegungID + 1
    stock_db.close()

    end_timestamp = Timestamp()
    diff_timestamp = int(end_timestamp - start_timestamp)
    print("Found answer: " + str(FreieBewegungID) + " after " + str(diff_timestamp) + "s")

    return FreieBewegungID

def getFreieInventarID():
    print("getFreieInventarID()")
    global FreieInventarID

    try: inventar_db.connect()
    except: True

    start_timestamp = Timestamp()

    #while True:
        #query = Inventar.select().where(Inventar.identification == str(FreieInventarID))
    query = Inventar.select().order_by(-Inventar.identification).limit(1)        
        #if not query.exists(): break

    if len(query) > 0:
        FreieInventarID = query[0].identification + 1
    else:
        FreieInventarID = 0
    inventar_db.close()

    end_timestamp = Timestamp()
    diff_timestamp = int(end_timestamp - start_timestamp)
    print("Found answer: " + str(FreieInventarID) + " after " + str(diff_timestamp) + "s")

    return FreieInventarID
"""

def DateAddDays(date, number):
    date = str(date)
    number = int(number)
    date_object = datetime.datetime.strptime(date,"%Y-%m-%d")
    date_object = date_object + datetime.timedelta(days=number)
    date_object = str(date_object).split(" ")[0]
    return str(date_object)

def TodayAddDays(number):
    number = int(number)
    return datetime.date.today() + datetime.timedelta(days=number)
    
def Date():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d")

def Timestamp():
    return time.time()
 
def get_worker_total(days): # Return total ....
    print("get_worker_total(" + str(days) + ")")

    try: stock_db.connect()
    except: True    
    try: rechnung_db.connect()
    except: True

    answer = {}
    worker_list = []
    
    query_article = Artikel.select().where(Artikel.categorie == 1)
    if query_article.exists():
        for article in query_article:
            #print("article.identification: " + str(article.identification))
            #answer[str(article.identification)] = {}
            worker_list.append(str(article.identification))
    
    list_of_dates = []
    date_today = Date()
    for x in range(0, int(days)+1):
        #print("date - " + str(x))
        date = str(DateAddDays(date_today, -x))
        
        for w_i in worker_list: 
            query_rechnung = Rechnung.select().where(Rechnung.datum.contains(date))
            if query_rechnung.exists():
                for rechnung in query_rechnung:
                    lieferscheine = str(rechnung.lieferscheine).split("|")
                    for l_id in lieferscheine:                        
                        query_lieferschein = Lieferschein.select().where(Lieferschein.identification == l_id)
                        query_lieferschein = query_lieferschein.where(Lieferschein.bcode.contains(w_i))
                        if query_lieferschein.exists():
                            l = query_lieferschein[0]                            
                            summe = float(l.total_htva)
                            
                            if not w_i in answer:
                                answer[w_i] = {}
                            
                            if not "total" in answer[w_i]:
                                answer[w_i]["total"] = 0.0 
                                
                            if not date in answer[w_i]:
                                answer[w_i][date] = 0.0
                            
                            answer[w_i][date] = round(answer[w_i][date] + summe, 2)                            
                            answer[w_i]["total"] = round(answer[w_i]["total"] + summe, 2)
        
    for i in answer:
        print(str(i) + ": " + str(answer[i]) + "\n")
        
    
    try: stock_db.close()
    except: True
    try: rechnung_db.close()
    except: True
    
    return answer
    
def get_best_clients(max_quantity, days): # Return best clients
    print("get_best_clients(" + str(max_quantity) + ", " + str(days) +")")

    try: rechnung_db.connect()
    except: True
    try: kunde_db.connect()
    except: True
    
    clients = {}
    invoice_quantity = {}
    name = {}
    firma = {}
    
    try: days = int(days)
    except: days = 730
    
    try: max_quantity = int(max_quantity)
    except: max_quantity = 20
    
    if max_quantity > 100: max_quantity = 100
    
    date = Date()
    smallest_date = DateAddDays(date, -days)
    
    query = Rechnung.select().where(Rechnung.datum > smallest_date).where(Rechnung.kunde_id != 0)
    for rechnung in query:
        client_id = int(rechnung.kunde_id)
        if client_id in clients:
            clients[client_id] = clients[client_id] + rechnung.total
            invoice_quantity[client_id] = invoice_quantity[client_id] + 1
        else:
            clients[client_id] = round(rechnung.total, 2)
            invoice_quantity[client_id] = 1
            
            query = Kunde.select().where(Kunde.identification == client_id)
            if query.exists():
                name[client_id] = query[0].name         
                if query[0].tva.rstrip() == "": firma[client_id] = "PRIVAT"           
                else: firma[client_id] = "FIRMA"
            else:
                name[client_id] = "Kunde existiert nicht"   
                firma[client_id] = "UNBEKANNT" 
    
    query = Kasse.select().where(Kasse.datum > smallest_date).where(Kasse.kunde_id != 0)
    for rechnung in query:
        client_id = int(rechnung.kunde_id)
        if client_id in clients:
            clients[client_id] = round(clients[client_id] + rechnung.total, 2)
            invoice_quantity[client_id] = invoice_quantity[client_id] + 1            
        else:
            clients[client_id] = round(rechnung.total, 2)
            invoice_quantity[client_id] = 1
            
            query = Kunde.select().where(Kunde.identification == client_id)
            if query.exists():
                name[client_id] = query[0].name         
                if query[0].tva.rstrip() == "": firma[client_id] = "PRIVAT"           
                else: firma[client_id] = "FIRMA"
            else:
                name[client_id] = "Kunde existiert nicht"   
                firma[client_id] = "UNBEKANNT"     

    try: rechnung_db.close()
    except: True
    
    sorted_values = sorted(clients.values()) # Sort the values
    sorted_dict = {}
    for i in sorted_values:
        for k in clients.keys():
            if clients[k] == i:
                sorted_dict[k] = clients[k]
                break

    return_list = []
    for key in sorted_dict.keys():
        return_list.insert(0, key)
    
    answer_list = {}
    index = 0
    for x in return_list:
        index = index + 1
        
        answer_list[x] = {"total": round(clients[x], 2), "quantity": invoice_quantity[x], "name": name[x], "firma": firma[x]}
        if index == max_quantity: break
    
    return answer_list
    
def get_client_total(id): # Return total of client deliverys sum as [buy, sell]
    print("get_client_total(" + str(id) + ")")

    #try: lieferschein_db.connect()
    #except: True
    try: rechnung_db.connect()
    except: True
    try: rechnung_e_db.connect()
    except: True
    
    total_vk = 0.0
    total_ek = 0.0
    
    query = Rechnung.select().where(Rechnung.kunde_id == str(id))
    for rechnung in query:
        total_vk = total_vk + rechnung.total
    
    query = Kasse.select().where(Kasse.kunde_id == str(id))
    for rechnung in query:
        total_vk = total_vk + rechnung.total
        
    total_vk = libs.RoundUp.RoundUp05(total_vk)    
    print("total_vk: " + str(total_vk))


    query = Rechnung_e.select().where(Rechnung_e.lieferant_id == str(id))
    for rechnung in query:
        total_ek = total_ek + rechnung.total

    total_ek = libs.RoundUp.RoundUp05(total_ek)    
    print("total_ek: " + str(total_ek))

    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True

    return [total_vk, total_ek]

def search_client(Dict):# Give Dict with Search return List of IDs 
    print("search_client")
    for key, var in Dict.items():
        print(str(key) + ": " + str(var))

    try: kunde_db.connect()
    except: True

    where_condition = []
    Antwort = []

    if "identification" in Dict:
        if not Dict["identification"] == "":
            where_condition.append(Kunde.identification == Dict["identification"])
            
    if "name" in Dict:
        if not Dict["name"].rstrip() == "":
            Dict["name"] = libs.BlueFunc.simple_text(Dict["name"])
            where_condition.append( (Kunde.name.contains(Dict["name"].rstrip().upper()) ) | (Kunde.alias.contains(Dict["name"].rstrip().upper()) ) )
            
    if "ort" in Dict:
        if not Dict["ort"] == "":
            Dict["ort"] = libs.BlueFunc.simple_text(Dict["ort"])
            where_condition.append(Kunde.ort.contains(Dict["ort"].rstrip()))   
        
    if "info" in Dict:
        where_condition.append(Kunde.info.contains(Dict["info"]) )

    if "supplier" in Dict:
        where_condition.append(Kunde.supplier == Dict["supplier"] )
        
    if "tva" in Dict:
        where_condition.append(Kunde.tva == Dict["tva"] )
    
    NichtSuchen = False
    if len(where_condition) == 0: NichtSuchen = True

    if not NichtSuchen:
        query = Kunde.select().where(*where_condition).order_by(Kunde.name).limit(100)
        for object in query: 
            Antwort.append(str(object.identification))
                
    print("Antwort: " + str(Antwort))

    try: kunde_db.close()
    except: True
    
    return Antwort

def search_article(Dict):# Give Dict with Search return List of IDs 
    print("search_article")

    if not "{" in str(Dict): # Not a dict, fix it
        Dict = {"suche": Dict}

    for key, var in Dict.items():
        print(str(key) + ": " + str(var))

    try: stock_db.connect()
    except: True
    
    where_condition = []
    Antwort = []
    if "limit" in Dict:
        try: IndexLimit = int(Dict["limit"])
        except: IndexLimit = 100
    else:
        IndexLimit = 100

    if "suche" in Dict:
        if not Dict["suche"] == "":
            print("Suche mit allem")
            Dict["suche"] = Dict["suche"].upper()        
            
            if len(Dict["suche"]) > 15:
                print("split len of search")
                Dict["suche"] = Dict["suche"][0:15]
                print("suche: " + str(Dict["suche"]))
            
            Dict["suche"] = Dict["suche"].replace("-", "")
            
            # if is Barcode
            if Dict["suche"].isdigit():
                where_condition.append((Artikel.identification == str(Dict["suche"])) | (Artikel.barcode == int(Dict["suche"])) | (Artikel.artikel == str(Dict["suche"])) | (Artikel.artikel2 == str(Dict["suche"])) | (Artikel.artikel3 == str(Dict["suche"])) | (Artikel.artikel4 == str(Dict["suche"])) | (Artikel.artikel5 == str(Dict["suche"])) | (Artikel.artikel6 == str(Dict["suche"])) )
            else:
                where_condition.append((Artikel.identification == str(Dict["suche"])) | (Artikel.artikel == str(Dict["suche"])) | (Artikel.artikel2 == str(Dict["suche"])) | (Artikel.artikel3 == str(Dict["suche"])) | (Artikel.artikel4 == str(Dict["suche"])) | (Artikel.artikel5 == str(Dict["suche"])) | (Artikel.artikel6 == str(Dict["suche"])) )

    if "artikel" in Dict:
        print("Suche mit Artikel")
        Dict["artikel"] = Dict["artikel"].upper()
        Dict["artikel"] = Dict["artikel"].replace(" ", "").replace("-", "").replace(".", "").replace(",", "")
        
        where_condition.append(Artikel.artikel == str(Dict["artikel"]))

    if "lieferant" in Dict:
        print("Suche mit Lieferant")
        Dict["lieferant"] = Dict["lieferant"].upper()
        
        where_condition.append(Artikel.lieferant == str(Dict["lieferant"]))
        
    if "name" in Dict:
        print("Suche mit Name")
        Dict["name"] = Dict["name"].lower()
        
        where_condition.append(fn.lower(Artikel.name_de.contains(Dict["name"])) | fn.lower(Artikel.name_fr.contains(Dict["name"])))
        
    if "name_de" in Dict:
        print("Suche mit Name_de")
        Dict["name_de"] = Dict["name_de"].lower()
        
        where_condition.append(fn.lower(Artikel.name_de.contains(Dict["name_de"])))
        
    if "name_fr" in Dict:
        print("Suche mit Name_fr")
        Dict["name_fr"] = Dict["name_fr"].lower()
        
        where_condition.append(fn.lower(Artikel.name_fr.contains(Dict["name_fr"])))
        
    if "ort" in Dict:
        print("Suche mit Ort")
        Dict["ort"] = Dict["ort"].upper()
        
        where_condition.append(Artikel.ort == Dict["ort"])
        
    if "categorie" in Dict:
        print("Suche mit Categorie")
        Dict["categorie"] = str(Dict["categorie"]).replace(".0", "")
        
        where_condition.append(Artikel.categorie == Dict["categorie"])
        
    if "only_on_stock" in Dict:
        print("Search Only article on Stock: " + str(Dict["only_on_stock"]))
        if bool(Dict["only_on_stock"]):
            where_condition.append(Artikel.anzahl > 0.0)
        
    NichtSuchen = False
    if len(where_condition) == 0: NichtSuchen = True
    
    while True:
        if NichtSuchen: break
        Count = 1
        for ID in Artikel.select().where(*where_condition).order_by(Artikel.name_de):
            Antwort.append(str(ID.identification))
            if Count == IndexLimit: break
            else: Count += 1
        break

    try: stock_db.close()
    except: True
    
    return Antwort

def create_chat(title, user_identification): # create new chat and return chat identification
    print("create_chat(" + str(title) + ", " + str(user_identification) + ")")
    
    try: chat_db.connect()
    except: True
    
    userlist = [user_identification]
    userlist_as_json_str = json.dumps(userlist)
    
    new_chat = Chat.create(title = str(title), userlist = userlist_as_json_str)
    
    try: chat_db.close()
    except: True
    
    return new_chat.identification

def get_chat_list(user_identification): # get list of all chats with this user identification
    print("get_chat_list(" + str(user_identification) + ")")
    
    try: chat_db.connect()
    except: True
    
    answer_chat_list = []
    
    query = Chat.select()
    if query.exists():
        for chat in query:
            for user_id in json.loads(chat.userlist):
                if user_id == user_identification:
                    answer_chat_list.append(model_to_dict(chat))
    
    try: chat_db.close()
    except: True
    
    return answer_chat_list
    
def get_message_list(chat_identification, user_identification): # get list of all messages in chat
    print("get_message_list(" + str(chat_identification) + ", " + str(user_identification) + ")")
    
    try: chat_db.connect()
    except: True
    
    username_dict = {}
    try: user_db.connect()
    except: True

    for user in User.select():
        username_dict[int(user.identification)] = str(user.name)
    
    try: user_db.close()
    except: True
    
    answer_message_list = []
    if str(chat_identification).isdigit():
        query_chat = Chat.select().where(Chat.identification == int(chat_identification))
        if query_chat.exists():
            userlist = json.loads(query_chat[0].userlist)
            if int(user_identification) in userlist:
                query_messages = Message.select().where(Message.chat == int(chat_identification))
                if query_messages.exists():
                    for message in query_messages:
                        new_message_as_dict = model_to_dict(message)
                        user_identification_in_msg = new_message_as_dict["user"]
                        new_message_as_dict["user"] = str(user_identification_in_msg) + ": " + str(username_dict[user_identification_in_msg])
                        answer_message_list.append(new_message_as_dict)
    
    try: chat_db.close()
    except: True
    
    return answer_message_list

def create_message(chat_identification, new_message, user_identification): # create new message and return sucess
    print("create_message(" + str(chat_identification) + ", " + str(new_message) + ", " + str(user_identification) + ")")
    
    try: chat_db.connect()
    except: True
    
    answer_sucess = False
    
    query_chat = Chat.select().where(Chat.identification == int(chat_identification))
    if query_chat.exists():
        userlist = json.loads(query_chat[0].userlist)
        if int(user_identification) in userlist:
            new_message = Message.create(chat = int(chat_identification), user = int(user_identification), text = str(new_message), date = libs.BlueFunc.Date(), time = libs.BlueFunc.uhrzeit())
    
            answer_sucess = True
    
    try: chat_db.close()
    except: True
    
    return answer_sucess
    
def search_article_sorted(Dict):
    print("search_article_sorted(" + str(Dict) + ")")
    
    if "suche" in Dict and "name" in Dict:
        # search with suche only
        name = Dict["name"]
        del Dict["name"]
        list_of_articles = search_article(Dict)
        
        # search with name only
        del Dict["suche"]
        Dict["name"] = name
        second_list_of_articles = search_article(Dict)
        
        for check_article_identification in second_list_of_articles:
            if not check_article_identification in list_of_articles:
                list_of_articles.append(check_article_identification)
    else:
        list_of_articles = search_article(Dict)
        
    print("search_article_sorted: " + str(len(list_of_articles)) + " articles")
    new_dict_of_articles = {}
    
    key = str(Dict["key"])
        
    try: stock_db.connect()
    except: True
    
    if "return_full_articles" in Dict:
        try: return_full_articles = bool(Dict["return_full_articles"])
        except: return_full_articles = False
    else: return_full_articles = False
    
    for article_identification in list_of_articles:
        query = Artikel.select().where(Artikel.identification == article_identification)
        if query.exists():
            answer_art = model_to_dict(query[0])
            key_value = answer_art[key]
            if key_value in new_dict_of_articles:
                if return_full_articles: new_dict_of_articles[key_value].append(answer_art)
                else: new_dict_of_articles[key_value].append(article_identification)
            else:
                if return_full_articles: new_dict_of_articles[key_value] = [answer_art]
                else: new_dict_of_articles[key_value] = [article_identification]
                
    try: stock_db.close()
    except: True
    
    return new_dict_of_articles

def get_delivery_not_invoiced(index):
    print("get_delivery_not_invoiced(" + str(index) + ")")
    
    try: lieferschein_db.connect()
    except: True
    
    answer = {}
    
    #.where()
    #query = Lieferschein.select().where(Lieferschein.kunde_id != "0").order_by(Lieferschein.datum_todo)
    query = Lieferschein.select().order_by(Lieferschein.datum_todo)
    query = query.where(Lieferschein.angebot == False)
    query = query.where(Lieferschein.bestellung == False)
        
        
    query2 = []
    
    if query.exists():
        for q in query:
            if "RV" in q.rechnung:
                query2.append(q)
            else:
                if not "K" in q.rechnung:
                    if not "R" in q.rechnung:
                        query2.append(q)
       
    if index.isdigit():
        index = int(index)
        
        if len(query2) > index:
            answer = query2[index]
            answer = model_to_dict(query2[index])
    
    try: lieferschein_db.close()
    except: True
    
    return answer

def search_delivery_by_changed_date_and_total(datum, total): # Give date and total -> return list of delivery id changed on this date
    print("search_delivery_by_changed_date_and_total(" + str(datum) + ", " + str(total) + ")")
    try: lieferschein_db.connect()
    except: True
    try: log_db.connect()
    except: True

    answer = []
    total = float(str(total).replace(",", "."))
    
    query_delivery = Lieferschein.select().where(Lieferschein.total == total)
    if query_delivery.exists():
        print("searching in " + str(len(query_delivery)) + " entrys")
        for delivery in query_delivery:
            query_log = Log.select().where(Log.document_id == delivery.identification).where(Log.date.contains(datum)).where(Log.mode == "delivery")
            if query_log.exists():
                answer.append(delivery.identification)

    try: lieferschein_db.close()
    except: True
    try: log_db.close()
    except: True
    
    print(answer)
    return answer


def search_delivery(Dict):# Give Dict with Search return List of IDs 
    print("search_delivery")
    for key, var in Dict.items():
        print(str(key) + ": " + str(var))

    try: lieferschein_db.connect()
    except: True
      
    where_condition = []
    Antwort = []

    if not "identification" in Dict: Dict["identification"] = ""
    if not "kunde_id" in Dict: Dict["kunde_id"] = ""
    if not "fertig" in Dict: Dict["fertig"] = 2
    if not "eigene" in Dict: Dict["eigene"] = False
    if not "angebote" in Dict: Dict["angebote"] = False
    if not "bestellung" in Dict: Dict["bestellung"] = False
    if not "delivery_only" in Dict: Dict["delivery_only"] = False
    if not "inhalt" in Dict: Dict["inhalt"] = ""
    if not "limit" in Dict or not str(Dict["limit"]).isdigit(): Dict["limit"] = 150
    if not "order_by" in Dict: Dict["order_by"] = "-datum_todo"

    print("check conditions to search: " + str(Dict))
    if not Dict["identification"] == "":
        print("search with identification")
        Dict["identification"] = str(Dict["identification"]).replace("A", "").replace("B", "").replace("L", "").rstrip()
        where_condition.append(Lieferschein.identification == Dict["identification"])
    if not Dict["kunde_id"] == "":
        print("search with kunde_id")
        if str(Dict["kunde_id"]).isdigit(): where_condition.append(Lieferschein.kunde_id == Dict["kunde_id"])
        else: where_condition.append(Lieferschein.kunde_name.contains(Dict["kunde_id"])) # id is a Name
    if Dict["eigene"] == True:
        print("eigene: True")
        where_condition.append(Lieferschein.user.contains(Dict["pcname"].upper()))
    if "search_user" in Dict:
        print("search with search_user")
        where_condition.append(Lieferschein.user.contains(Dict["search_user"].upper()))
    if "rechnung" in Dict:
        if str(Dict["rechnung"]) == "-1": # not rechnung
            print("Search Deliverys that are not part of a invoice")
            where_condition.append(Lieferschein.rechnung == "")        
        else:
            if not str(Dict["rechnung"]) == "": # search with rechnung
                print("search with rechnung")
                where_condition.append(Lieferschein.rechnung.contains(Dict["rechnung"].upper()))
    if Dict["angebote"] == True:
        print("search angebote")
        where_condition.append(Lieferschein.angebot == True)
    if Dict["bestellung"] == True:
        print("search bestellung")
        where_condition.append(Lieferschein.bestellung == True) 
        
    if Dict["delivery_only"] == True:
        print("search delivery_only = without angebot - without bestellung")
        where_condition.append(Lieferschein.angebot == False)
        where_condition.append(Lieferschein.bestellung == False)
        
    if "maschine" in Dict:
        print("search with maschine")
        where_condition.append(Lieferschein.maschine.contains(Dict["maschine"]))
        
    if "info" in Dict:
        print("search with info")
        where_condition.append(Lieferschein.info.contains(Dict["info"]))      

    if Dict["fertig"] == 0:# 0=Nicht Fertig / 1=Nur Fertig / 2=Egal
        print("fertig: 0")
        where_condition.append(Lieferschein.fertig == False)
    else:
        if Dict["fertig"] == 1:
            print("fertig: 1")
            where_condition.append(Lieferschein.fertig == True)
            
    if not Dict["inhalt"] == "":
        print("search with inhalt")
        where_condition.append(Lieferschein.name.contains(Dict["inhalt"]) | Lieferschein.info.contains(Dict["inhalt"]) | Lieferschein.maschine.contains(Dict["inhalt"]) | Lieferschein.bcode.contains(Dict["inhalt"]) )

    print("where_condition: " + str(len(where_condition)))
    if len(where_condition) == 0:
        query = Lieferschein.select().limit(Dict["limit"])
        
    else:
        #if Dict["fertig"] == 0 or str(Dict["rechnung"]) == "-1":
        #    query = Lieferschein.select().where(*where_condition).order_by(Lieferschein.datum_todo).limit(100)
        #else:            
        #    query = Lieferschein.select().where(*where_condition).order_by(-Lieferschein.datum).limit(100)
        
        if Dict["order_by"] == "-datum_todo":
            query = Lieferschein.select().where(*where_condition).order_by(-Lieferschein.datum_todo).limit(Dict["limit"])
        elif Dict["order_by"] == "datum_todo":
            query = Lieferschein.select().where(*where_condition).order_by(Lieferschein.datum_todo).limit(Dict["limit"])
        elif Dict["order_by"] == "-datum":
            query = Lieferschein.select().where(*where_condition).order_by(-Lieferschein.datum).limit(Dict["limit"])
        elif Dict["order_by"] == "datum":
            query = Lieferschein.select().where(*where_condition).order_by(Lieferschein.datum).limit(Dict["limit"])
        else:
            query = Lieferschein.select().where(*where_condition).order_by(Lieferschein.identification).limit(Dict["limit"])
       
       
    for object in query: Antwort.append(object.identification) 

    try: lieferschein_db.close()
    except: True
    
    return Antwort


def search_delivery_e(Dict):# Give Dict with Search return List of IDs 
    print("search_delivery_e")
    for key, var in Dict.items():
        print(str(key) + ": " + str(var))

    try: delivery_e.connect()
    except: True
      
    where_condition = []
    Antwort = []

    if not "identification" in Dict: Dict["identification"] = ""
    if not "client_identification" in Dict: Dict["client_identification"] = ""
    if not "client_name" in Dict: Dict["client_name"] = ""
    if not "status" in Dict: Dict["status"] = 2
    if not "content" in Dict: Dict["content"] = ""
    if not "limit" in Dict or not str(Dict["limit"]).isdigit(): Dict["limit"] = 200
    if not "order_by" in Dict: Dict["order_by"] = "-date"

    print("check conditions to search: " + str(Dict))
    if not Dict["identification"] == "":
        print("search with identification")
        Dict["identification"] = str(Dict["identification"]).replace("LE", "").rstrip()
        where_condition.append(Delivery_e.identification == Dict["identification"])
    if not Dict["client_identification"] == "":
        print("search with client_identification")
        where_condition.append(Delivery_e.client_identification == Dict["client_identification"])
    if not Dict["client_name"] == "":
        print("search with client_name")
        where_condition.append(Delivery_e.client_name.contains(Dict["client_name"]))
        
    if Dict["status"] == 0:# 0=Nicht Fertig / 1=Nur Fertig / 2=Egal
        print("status: 0")
        where_condition.append(Delivery_e.status == False)
    else:
        if Dict["status"] == 1:
            print("status: 1")
            where_condition.append(Delivery_e.status == True)

    print("where_condition: " + str(len(where_condition)))
    if len(where_condition) == 0:
        query = Delivery_e.select().limit(Dict["limit"])
        
    else:
        if Dict["order_by"] == "-date":
            query = Delivery_e.select().where(*where_condition).order_by(-Delivery_e.date).limit(Dict["limit"])
        elif Dict["order_by"] == "date":
            query = Delivery_e.select().where(*where_condition).order_by(Delivery_e.date).limit(Dict["limit"])
        elif Dict["order_by"] == "-date_document":
            query = Delivery_e.select().where(*where_condition).order_by(-Delivery_e.date_document).limit(Dict["limit"])
        elif Dict["order_by"] == "date_document":
            query = Delivery_e.select().where(*where_condition).order_by(Delivery_e.date_document).limit(Dict["limit"])
        else:
            query = Delivery_e.select().where(*where_condition).order_by(Delivery_e.identification).limit(Dict["limit"])

    #for object in query: Antwort.append(object.identification)
    for object in query: Antwort.append(model_to_dict(object))

    try: delivery_e_db.close()
    except: True
    
    return Antwort

def set_delivery_e(Dict):
    print("set_delivery_e(" + str(Dict) + ")")

    if ai_helper:
        text = get_delivery_e_text(Dict["identification"])
        if int(Dict["client_identification"]) == 0:
            try:
                Dict["client_identification"] = make_prediction("predict_delivery_e_client_identification", text)
                Dict["client_identification"] = int(Dict["client_identification"])
            except: Dict["client_identification"] = 0

    try: delivery_e_db.connect()
    except: True
    
    if "identification" in Dict:
        identification = str(Dict["identification"]).replace("LE", "")
        if identification.isdigit():
            identification = int(identification)
            
            query = Delivery_e.select().where(Delivery_e.identification == identification)
            if query.exists():
                if "client_identification" in Dict:
                    query[0].client_identification = int(Dict["client_identification"])
                if "client_name" in Dict:
                    query[0].client_name = str(Dict["client_name"])
                
                query[0].save()
                answer = True
            else: answer = False
        else: answer = Fakse
    else:
        answer = False
    
    try: delivery_e_db.close()
    except: True
    
    if ai_helper:
        if "client_identification" in Dict:
            add_to_prediction("predict_delivery_e_client_identification", int(Dict["client_identification"]), get_delivery_e_text(identification))
    
    return answer

def create_delivery_e():
    print("create_delivery_e()")

    try: delivery_e_db.connect()
    except: True
    
    lines_as_json = json.dumps([])
    
    new_delivery_e =  Delivery_e.create(date = str(libs.BlueFunc.Date()), date_document = "", client_identification = 0, client_name = "", lines = lines_as_json, status = False)
    new_delivery_e.save()
    
    try: answer = model_to_dict(new_delivery_e)
    except: answer = {}
    
    try: delivery_e_db.close()
    except: True
    
    return answer
    
def get_delivery_e_text(identification):
    print("get_delivery_e_text(" + str(identification) + ")")

    try: delivery_e_db.connect()
    except: True
    
    identification = str(identification).replace("LE", "")
    if identification.isdigit():
        identification = int(identification)
        
        query = Delivery_e.select().where(Delivery_e.identification == identification)
        if query.exists():
            date_list = query[0].date.split("-")
            new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/delivery_e/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
            if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
            
            if os.path.exists(new_pdf_path + "LE" + str(identification) + ".pdf"):
                if not os.path.exists("/tmp/LE" + str(identification) + ".pdf"):
                    os.system("cp " + new_pdf_path + "/LE" + str(identification) + ".pdf /tmp/")
                os.system("cd /tmp/ && pdftotext -layout /tmp/LE" + str(identification) + ".pdf")
                answer = open("/tmp/LE" + str(identification) + ".txt", "r").read()
            else: answer = ""
        else: answer = ""
    else: answer = ""
    
    try: delivery_e_db.close()
    except: True
    
    return answer
    
def get_delivery_e(identification):
    print("get_delivery_e(" + str(identification) + ")")
            
    try: delivery_e_db.connect()
    except: True
    
    identification = str(identification).replace("LE", "")
    if identification.isdigit():
        identification = int(identification)
        
        query = Delivery_e.select().where(Delivery_e.identification == identification)
        if query.exists():
            answer = model_to_dict(query[0])
        else: answer = {}
    else: answer = {}
    
    try: delivery_e_db.close()
    except: True
    
    return answer
    
def get_delivery_e_pdf(identification):
    print("get_delivery_e_pdf(" + str(identification) + ")")

    try: delivery_e_db.connect()
    except: True
    
    identification = str(identification).replace("LE", "")
    if identification.isdigit():
        identification = int(identification)
        
        query = Delivery_e.select().where(Delivery_e.identification == identification)
        if query.exists():
            date_list = query[0].date.split("-")
            new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/delivery_e/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
            if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
            
            if os.path.exists(new_pdf_path + "LE" + str(identification) + ".pdf"):
                client_destination_path = libs.ServerTools.export_to_web_tmp(new_pdf_path + "LE" + str(identification) + ".pdf")
                answer = client_destination_path
            else: answer = ""

        else: answer = ""
    else: answer = ""
    
    try: delivery_e_db.close()
    except: True
    
    return answer

def create_client_with_identification(identification):#Kunde erstellen mit einer bestimmten id: return Dict
    print("create_client_with_identification(" + str(identification) +")")

    if not str(identification).isdigit():
        print("identification is not a number, start with 1")
        identification = 1
        
    identification = int(identification)

    try: kunde_db.connect()
    except: True

    while True:
        query = Kunde.select().where(Kunde.identification == identification)
        if query.exists():
            identification += 1
        else:
            neuer_kunde = Kunde.create(identification=int(identification), name="", alias="", land="BE", adresse="", plz="0", ort="", email1="", email2="", email3="", tel1="", tel2="", tel3="", tel4="", tel1_beschreibung="", tel2_beschreibung="", tel3_beschreibung="", tel4_beschreibung="", tva="", sprache_de=False, sprache_fr=False, sprache_nl=False, lastchange=str(Timestamp()), creation=str(Date()), info="", warning="", dealer=False, supplier=False, disabled=False, foto="", rappel=0.0, sepa=False, payment_deadline=15)
            #neuer_kunde.save()
            
            #fix for mysql with 0 as integer
            #if str(identification) == "0":
            #    print("identifcation = 0")
            #    wrong_identification = neuer_kunde.identification
            #    qry=Kunde.update({Kunde.identification:identification}).where(Kunde.identification == wrong_identification)
            #    #print (qry.sql())
            #    qry.execute()
                
            break

    Antwort = model_to_dict(neuer_kunde)

    try: kunde_db.close()
    except: True
    
    return Antwort
    
def create_client():# return Dict
    print("create_client")
    
    try: kunde_db.connect()
    except: True

    ThisKunde = Kunde.create(name="", alias="", land="BE", adresse="", plz="0", ort="", email1="", email2="", email3="", tel1="", tel2="", tel3="", tel4="", tel1_beschreibung="", tel2_beschreibung="", tel3_beschreibung="", tel4_beschreibung="", tva="", sprache_de=False, sprache_fr=False, sprache_nl=False, lastchange=str(Timestamp()), creation=str(Date()), info="", warning="", dealer=False, supplier=False, disabled=False, foto="", rappel=0.0, sepa=False, payment_deadline=15)

    ThisKunde.save()

    #object = Kunde.get(Kunde.identification == str(FreierKunde))
    Antwort = model_to_dict(ThisKunde)

    try: kunde_db.close()
    except: True
    
    return Antwort

def NeuerLieferschein(pcname):# return Dict
    print("NeuerLieferschein")
    FreierLieferschein = 0
    pcname = str(pcname).upper()
    
    try: lieferschein_db.connect()
    except: True
    
    while True:
        query = Lieferschein.select().where(Lieferschein.identification == str(FreierLieferschein))
        if not query.exists():
            break
        FreierLieferschein = FreierLieferschein + 1

    ThisLieferschein = Lieferschein.create(linien="0", anzahl="1", bcode="", name="", preis="0.0", user=str(pcname) ,identification=str(FreierLieferschein), kunde_id="0", kunde_name = "", datum=str(Date()), datum_todo=str(TodayAddDays(7)), fertig=False, info="", maschine = "", total=0.0, total_htva=0.0, lastchange=str(Timestamp()))

    ThisLieferschein.save()

    object = Lieferschein.get(Lieferschein.identification == str(FreierLieferschein))
    Antwort = model_to_dict(object)

    try: lieferschein_db.close()
    except: True

    addUserToList(str(pcname))
    return Antwort

def get_delivery(identification):# return Dict
    print("get_delivery(" + str(identification) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification).replace("A", "").replace("B", "").replace("L", "").replace(".0", "")
    print("identification: " + str(identification))
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        object = Lieferschein.get(Lieferschein.identification == identification)
        Antwort = model_to_dict(object)
        Antwort["name"] = Antwort["name"].replace("&", "+") # fix for printing ?
        Antwort["user"] = Antwort["user"].upper() # fix Lowercase

        client_id = Antwort["kunde_id"]
        print("client_id: " + str(client_id))
        client_data = get_client(client_id)
        #print(client_data)
        
        
        if client_data == {}:
            Antwort["dealer"] = False
            Antwort["supplier"] = False
        else:
            Antwort["dealer"] = client_data["dealer"]
            Antwort["supplier"] = client_data["supplier"]

        # fix of empty Delivery, why is it sometime empty?
        if Antwort["linien"] == "" or Antwort["linien"] == None:
            print("Error in Delivery, fixing it")
            Antwort["linien"] = "0"
            Antwort["anzahl"] = "1.0"
            Antwort["bcode"] = ""
            Antwort["name"] = ""
            Antwort["info"] = ""
            Antwort["preis"] = "0.0"
            Antwort["total"] = 0.0
            Antwort["tva"] = "0.0"
            
            fixed_antwort = Antwort
            del fixed_antwort["dealer"]
            del fixed_antwort["supplier"]
            Fixed_delivery = dict_to_model(Lieferschein, fixed_antwort)
            Fixed_delivery.save()
            
        if Antwort["info"] == None: Antwort["info"] = ""      

        query = Lieferschein.select().where(Lieferschein.identification == identification)
        if query.exists():
            try:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "tva": Antwort["tva"]})
                ThisDelivery.save()            
            except: 
                print("Error on saving delivery note")
                print("{identification: " + str(identification) + ", tva: " + str(Antwort["tva"]) + "}")
        else: 
            print("Delivery note not found")    
        
        #fix for old databse with no preis_htva column 
        if Antwort["preis_htva"] == "": 
            counting_lines = len(Antwort["linien"].split("|"))
            tva_list = []
            for x in range(0, counting_lines): tva_list.append("0.0")
            Antwort["preis_htva"] = "|".join(tva_list)

        query = Lieferschein.select().where(Lieferschein.identification == identification)
        if query.exists():
            try:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "preis_htva": Antwort["preis_htva"]})
                ThisDelivery.save()            
            except: 
                print("Error on saving delivery note")
                print("{identification: " + str(identification) + ", preis_htva: " + str(Antwort["preis_htva"]) + "}")
        else: 
            print("Delivery note not found")

    
    else:
        Antwort = {}
    
    try: lieferschein_db.close()
    except: True
    
    return Antwort

def get_delivery_user ( identification ): # return Dict of user with state in Delivery
    print("get_delivery_user(" + str(identification) + ")")
    try: lieferschein_db.connect()
    except: True
    try: user_db.connect()
    except: True

    checked_user = get_delivery(identification)["user"].split("|")

    user_list = []
            
    query = User.select().order_by(User.name)
    for user in query:
        username = str(user.name).upper()
        if username in checked_user: user_list.append({"name": username, "isChecked": True})
        else: user_list.append({"name": username, "isChecked": False})

    #identification = str(identification)
    #query = Lieferschein.select().where(Lieferschein.identification == identification)
    #if query.exists():
    #    object = Lieferschein.get(Lieferschein.identification == identification)
    #    Antwort = model_to_dict(object)
    #    Antwort["name"] = Antwort["name"].replace("&", "+") # fix for printing ?
    #else:
    #    Antwort = {}
    Antwort = user_list
    #[{"name": "Kenny", "isChecked": True}]    

    try: lieferschein_db.close()
    except: True
    try: user_db.close()
    except: True
    
    return Antwort

def set_delivery_user ( identification , user, state ): # return bool(sucess)
    print("set_delivery_user(" + str(identification) + ", " + str(user) + ", " + str(state) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    user = str(user).upper()
    state = bool(state)

    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        if True:#try:
            object = Lieferschein.get(Lieferschein.identification == identification)

            if True:#if object.rechnung == "" or "RV" in object.rechnung:
                dict = model_to_dict(object)
                user_list = dict["user"].split("|")

                if state:# add user to list
                    if not user in user_list:
                        print("add " + user + " to user_list")
                        user_list.append(user)
                else:# remove user from list
                    if user in user_list:
                        print("remove " + user + " from user_list")
                        del user_list[user_list.index(user)]

                user_list = "|".join(user_list)
                print("user_list: " + str(user_list))
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "user": user_list, "lastchange": Timestamp() })
                ThisDelivery.save()
                sucess = True
            else:
                sucess = False
        if False:#except: 
            print("Error: return False")
            sucess = False
    else:
        print("Error: return False")
        sucess = False
    
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess      

def add_delivery_article(identification, bcode, user):# return bool(sucess)
    print("add_delivery_article(" + str(identification) + ", " + str(bcode) + ", " + str(user) + ")")

    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    bcode = str(bcode)
    user = str(user)
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        counter = 0
        index = -1
        quantity = 1
        tva = standart_tva
        
        for q_bcode in query[0].bcode.split("|"):
            if q_bcode == bcode:
                index = counter
                quantity = float(query[0].anzahl.split("|")[counter]) + 1
                tva = float(query[0].tva.split("|")[counter]) + 1
                break
            counter += 1
    
    try: lieferschein_db.close()
    except: True
    
    if query.exists(): 
        set_delivery_line(identification, index, quantity, bcode, "", 0, tva, user)
        sucess = True
    else:
        sucess = False
    
    return sucess 

def set_delivery_line(identification, index, quantity, barcode, name, price, tva, user):# return bool(sucess)
    print("set_delivery_line(" + str(identification) + ", " + str(index) + ", " + str(quantity) + ", " + str(barcode) + ", " + str(name) + ", " + str(price) + ", " + str(tva) + ", " + str(user) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    index = int(index)

    try: quantity = float(quantity)
    except: 
        print("Quantity isn't a float -> 0.0")
        quantity = 0.0
    barcode = str(barcode).upper()
    name = str(name).replace("|", "")
    # fix for LibreOffice
    for f in ["&", "<", ">"]:
        name = name.replace(f, "")
    
    if "SN:" in name.upper():
        name = name.upper().replace(" ", "")

    print("price: " + str(price))
    try: price = float(price)
    except: price = 0.0
    price = round(price, 2)

    try: tva = float(tva)
    except: tva = 0.0
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        if True:#try:
            #dict = get_delivery(identification)
            dict = model_to_dict(query[0])
            client_dict = get_client(dict["kunde_id"])
            print("client_dict: " + str(client_dict))
            if client_dict == {}:
                dict["dealer"] = False
                dict["supplier"] = False
            else:
                dict["dealer"] = client_dict["dealer"]
                dict["supplier"] = client_dict["supplier"]
            
            if str(index) == "-1": index = len(dict["bcode"].split("|")) - 1
                            
            if dict["rechnung"] == "" or "RV" in dict["rechnung"]:
                old_quantity = float(dict["anzahl"].split("|")[index])
                old_barcode = str(dict["bcode"].split("|")[index])
                old_name = str(dict["name"].split("|")[index])
                old_price = float(dict["preis"].split("|")[index])

                # Verify for article if art = barcode
                if len(str(barcode)) > 6:
                    art_identification = search_article({"suche": barcode})
                    if len(art_identification) > 0: 
                        barcode = str(art_identification[0])
                    else:
                        barcode = ""

                dictOfArticle = get_article({"identification": barcode})
                print("dictOfArticle: " + str(dictOfArticle))
                sucess = True

                if not dictOfArticle == {}: # Barcode exists
                    # Replace empty fields with default of article
                    if barcode == "": barcode = str(dictOfArticle["name_de"])
                    if name == "": 
                        if client_dict["sprache_de"]:
                            name = str(dictOfArticle["name_de"])                            
                            if name == "":
                                name = libs.BlueFunc.translate("FR", "DE", str(dictOfArticle["name_fr"]))
                                name = name["text"]
                            
                        else:
                            name = str(dictOfArticle["name_fr"])
                            if name == "": 
                                name = libs.BlueFunc.translate("FR", "DE", str(dictOfArticle["name_de"]))
                                name = name["text"]
                                                    
                        ### if dealer
                        ### add artikel before name for easy identification                        
                        if client_dict["dealer"]:
                            name = dictOfArticle["artikel"] + " " + name
                        
                    #name = name.replace("<woche>", datetime.datetime.now().strftime("%W") )
                    
                    if str(price) == "0.0": 
                        print("Price is 0.0 -> set to default = " + str(dictOfArticle["preisvk"]))
                        price = str(dictOfArticle["preisvk"])

                    ### promo_modules ###
                    for promo_module in promo_modules:
                        print("run promo_module: " + promo_module)
                        # promo(client_as_dict, delivery_as_dict, article_as_dict, price)
                        try:
                            price = promo_modules[promo_module].promo(client_dict, dict, dictOfArticle, price, quantity, tva)
                            print("price: " + str(price))
                        except:
                            print("Error in promo module")
                        
                    if not old_barcode == barcode: old_quantity = 0.0
                    if not old_quantity == quantity and not dict["angebot"] and not dict["bestellung"]: # Quantity changed and kein Angebot und keine Bestellung
                        print("quantity changed !")
                        diff_quantity = float(quantity - old_quantity)
                        sucess = add_article({"identification": barcode, "add": -diff_quantity, "user": user, "price": price})
                        if not sucess: # STOP
                            try: lieferschein_db.close()
                            except: True
                            return sucess

                #print(str(len(dict["linien"].split("|"))))
                if len(dict["linien"].split("|")) == index + 1 and len(dict["linien"].split("|")) < 30:# if index is last and delivery isn't too long
                    if not dictOfArticle == {} or barcode == "":
                        print("Append lines")
                        if not dictOfArticle == {}: print("not dictOfArticle == {}")
                        if barcode == "": print("barcode == ''")
                        # Append to lines
                        liste = dict["linien"].split("|"); liste.append(str(int(liste[-1]) + 1)); dict["linien"] = str("|".join(liste))
                        # Change quantity
                        liste = dict["anzahl"].split("|"); liste.append("1.0"); dict["anzahl"] = str("|".join(liste))
                        # Change barcode
                        liste = dict["bcode"].split("|"); liste.append(""); dict["bcode"] = str("|".join(liste))
                        # Change name
                        liste = dict["name"].split("|"); liste.append(""); dict["name"] = str("|".join(liste))
                        # Change price
                        liste = dict["preis"].split("|"); liste.append("0.0"); dict["preis"] = str("|".join(liste))
                        # Change tva
                        liste = dict["tva"].split("|"); liste.append(str(standart_tva)); dict["tva"] = str("|".join(liste))
                
                if dictOfArticle == {} and not barcode == "":
                    print("article doesn't exist")
                    barcode = ""
                    sucess = False

                # Change quantity
                liste = dict["anzahl"].split("|"); liste[index] = str(quantity); dict["anzahl"] = str("|".join(liste))
                # Change barcode
                liste = dict["bcode"].split("|"); liste[index] = str(barcode); dict["bcode"] = str("|".join(liste))
                # Change name
                liste = dict["name"].split("|"); liste[index] = str(name); dict["name"] = str("|".join(liste))
                # Change price
                liste = dict["preis"].split("|"); liste[index] = str(price); dict["preis"] = str("|".join(liste))
                # Change tva
                liste = dict["tva"].split("|"); liste[index] = str(tva); dict["tva"] = str("|".join(liste))

                dict["lastchange"] = Timestamp()
                del dict["dealer"]
                del dict["supplier"]

                ThisDelivery = dict_to_model(Lieferschein, dict)
                ThisDelivery.save()

                set_delivery_total(identification)
            else:
                sucess = False
    
        
    try: lieferschein_db.close()
    except: True
    
    delete_delivery_pdf(identification)
    
    if "[DELETE]" in name.upper():
        remove_unused_deliverys()
        return True
    
    return sucess 

def set_delivery_info(identification, info):# return bool(sucess)
    print("set_delivery_info(" + str(identification) + ", " + str(info) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    info = str(info)

    # fix info printing errors of LibreOffice
    info = info.replace(">", ":")
    info = info.replace("<", ":")
    info = info.replace("&", "-")

    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        try:     
            # Darf immer geändert werden
            if True: #if query[0].rechnung == "" or "RV" in query[0].rechnung:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "info": info, "lastchange": Timestamp() })
                ThisDelivery.save()
                sucess = True

            else: sucess = False

        except: sucess = False
    else:
        sucess = False
        
    if sucess:
        delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess   

def set_delivery_total(identification):# return bool(sucess)
    print("set_delivery_total(" + str(identification) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)

    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        if True: #try:
            #dict = get_delivery(identification)
            dict = model_to_dict(query[0])

            if dict["rechnung"] == "" or "RV" in dict["rechnung"]:

                list_quantity = dict["anzahl"].split("|")
                list_price = dict["preis"].split("|")
                list_tva = dict["tva"].split("|")

                total = 0.0
                total_htva = 0.0
                list_preis_htva = []
                steuern = {}
                total_ohne_steuern = {0.0: 0.0}

                for index in range(0, len(dict["linien"].split("|"))):
                    #print("index: " + str(index))
                    index = int(index)
                    try: quantity = float(list_quantity[index])
                    except: quantity = 0.0
                    try: price = float(list_price[index])
                    except: price = 0.0
                    try: tva = float(list_tva[index])
                    except: tva = standart_tva
                    
                    
                    pricevkh = float(price) / ((tva / 100) + 1)
                    pricevkh = round(pricevkh, 4)    

                    list_preis_htva.append(str(pricevkh))            
                    
                    if not float(tva) in total_ohne_steuern:
                        total_ohne_steuern[float(tva)] = 0.0
                        
                    total_ohne_steuern[float(tva)] =  total_ohne_steuern[float(tva)] + (quantity * pricevkh)
                    total_ohne_steuern[float(tva)] = round(total_ohne_steuern[float(tva)], 4)
                  
                for tva_satz in total_ohne_steuern:
                    #print("tva_satz: " + str(tva_satz))
                    
                    steuern[tva_satz] =  total_ohne_steuern[tva_satz] * (tva_satz / 100)
                    steuern[tva_satz] = round( steuern[tva_satz] , 2)
                    
                    total_htva = total_htva + total_ohne_steuern[tva_satz]
                    total_htva = round(total_htva, 2)

                print("total_ohne_steuern: " + str(total_ohne_steuern))                
                print("steuern: " + str(steuern))
                
                print("-------------")
                
                print("total_htva: " + str(total_htva))

                
                preis_htva = "|".join(list_preis_htva)

                preis_steuern = ""
                for st in steuern:
                    if not str(st) == "0.0":
                        if preis_steuern == "":
                            preis_steuern = str(st) + ";" + str(steuern[st])                            
                        else:
                            preis_steuern = preis_steuern + "|" + str(st) + ";" + str(steuern[st])
                        
                        #print("total = " + str(total) + " + " + str(steuern[st]) )
                        total = total + steuern[st]
                        #total = float(int(total * 100)/100)
                        total = round(total, 2)
                        #print("new total = " + str(total))
                            

                preis_total_ohne_steuern = ""
                for st in total_ohne_steuern:
                    if True:#if not str(st) == "0.0":
                        #total_ohne_steuern[st] = round(total_ohne_steuern[st], 2)
                        if preis_total_ohne_steuern == "":                        
                            preis_total_ohne_steuern = str(st) + ";" + str(total_ohne_steuern[st])
                        else:
                            preis_total_ohne_steuern = preis_total_ohne_steuern + "|" + str(st) + ";" + str(total_ohne_steuern[st])
                        
                        #print("total = " + str(total) + " + " + str(total_ohne_steuern[st]) )
                        total = total + total_ohne_steuern[st]
                        #total = float(int(total * 100)/100)
                        total = round(total, 2)
                        #print("new total = " + str(total))
                            

                print("total: " + str(total))
                
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "total": total, "total_ohne_steuern": preis_total_ohne_steuern, "preis_htva": preis_htva, "steuern": preis_steuern, "total_htva": total_htva, "lastchange": Timestamp() })
                ThisDelivery.save()

                sucess = True
            #except: 
            else: sucess = False
    else:
        sucess = False
        
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess        

def set_delivery_date_todo ( identification , date ): # return bool(sucess)
    print("set_delivery_date_todo(" + str(identification) + ", " + str(date) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    date = str(date)

    # Fix Date format
    dateList=date.split("-")
    if len(str(dateList[1])) == 1: dateList[1] = "0" + str(dateList[1])# correction of month : 5 -> 05
    if len(str(dateList[2])) == 1: dateList[2] = "0" + str(dateList[2])# correction of day : 5 -> 05
    date = "-".join(dateList)

    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        try:
            if query[0].rechnung == "" or "RV" in query[0].rechnung:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "datum_todo": date, "lastchange": Timestamp() })
                ThisDelivery.save()
                sucess = True
            else: sucess = False
        except: sucess = False
    else:
        sucess = False
    
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    #update_delivery_calendar()
    
    return sucess    

def returnItem1(item):
    return item[1]

def search_belt ( min_thick, max_thick, min_length, max_length, index): # return next Article with BELT in Size and withing the range
    min_thick = int(min_thick)
    max_thick = int(max_thick)
    min_length = int(min_length)
    max_length = int(max_length)
    index = int(index)
    print("search_belt(" + str(min_thick) + ", " + str(max_thick) + ", " + str(min_length) + ", " + str(max_length) + ", " + str(index) + ")")

    try: stock_db.connect()
    except: True

    #query = Artikel.select() # need optimisation, to slow !
    query = Artikel.select().where(Artikel.groesse.contains("BELT"))
    query = query.where(Artikel.groesse.contains("X"))
    
    answer = []
    print(len(query))  

    if query.exists():
        for art in query:
            groesse = str(art.groesse).upper()
            if "BELT" in groesse:
                #print("groesse: " + str(groesse))
                groesse = groesse.replace("BELT", "")
                g_list = groesse.split("X")
                thick = int(g_list[0])
                length = list(g_list[1])
                if len(length) == 0:
                    length = "OL"
                else:
                    del length[-1]
                    length = "".join(length)

                try: length = int(length)
                except: length = 2500

                #print(str(min_thick) + " < " + str(thick) + " < " + str(max_thick))
                if thick > min_thick - 1 and thick < max_thick + 1:
                    if length > min_length - 1 and length < max_length + 1:
                        #print("groesse: " + str(groesse))
                        
                        if len(str(length)) == 3: length = "0" + str(length)
                        float_length = float(str(thick) + "." + str(length))

                        answer.append([art.identification, float_length])
        answer.sort(key=returnItem1)

        print(str(index) + " < " + str(len(answer)))
        if index < len(answer):
            identification = str(answer[index][0])
            answer = get_article({"identification": identification})
            answer["type"] = str(answer["groesse"])[-1].upper()
        else:
            answer = {"identification": "-1"}
    else:
        answer = {"identification": "-1"}
        
    try: stock_db.close()
    except: True

    return answer

def set_delivery_client ( identification , client_id ): # return bool(sucess)
    print("set_delivery_client(" + str(identification) + ", " + str(client_id) + ")")
    try: lieferschein_db.connect()
    except: True
    try: kunde_db.connect()
    except: True

    identification = str(identification)
    client_id = str(client_id)
    
    query = Kunde.select().where(Kunde.identification == client_id)
    if query.exists() or client_id == "0":
        try: client_name = query[0].name
        except: client_name = ""

        query = Lieferschein.select().where(Lieferschein.identification == identification)
        if query.exists():
            try:
                if query[0].rechnung == "" or "RV" in query[0].rechnung:
                    ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "kunde_id": client_id, "kunde_name": client_name, "lastchange": Timestamp() })
                    ThisDelivery.save()
                    sucess = True
                else: sucess = False
            except: 
                print("Error on saving delivery note")
                print("{identification: " + str(identification) + ", kunde_id: " + str(client_id) + ", kunde_name: " + str(client_name) + ", lastchange: before " + str(Timestamp()) + " }")
                sucess = False
        else:
            sucess = False
    else: 
        print("client_id not found")
        sucess = False
    
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    try: kunde_db.close()
    except: True
    
    return sucess

def set_delivery_engine ( identification , text ): # return bool(sucess)
    print("set_delivery_engine(" + str(identification) + ", " + str(text) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    text = str(text)
    
    
    # fix info printing errors of LibreOffice
    text = text.replace(">", ":")
    text = text.replace("<", ":")
    text = text.replace("&", "-")
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        try:
            # Engine can always be set !
            if True: #if query[0].rechnung == "" or "RV" in query[0].rechnung:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "maschine": text, "lastchange": Timestamp() })
                ThisDelivery.save()
                sucess = True
            else: sucess = False
        except: 
            print("Error on saving delivery note")
            sucess = False
    else: 
        print("Delivery note not found")
        sucess = False
        
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess    

def set_delivery_finish ( identification , finish ): # return bool(sucess)
    print("set_delivery_finish(" + str(identification) + ", " + str(finish) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    finish = bool(finish)
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        if True:#try:
            if query[0].rechnung == "" or "RV" in query[0].rechnung:
                ThisDelivery = dict_to_model(Lieferschein, {"identification": identification, "fertig": finish, "lastchange": Timestamp() })
                ThisDelivery.save()
                                  
                sucess = True
            else: sucess = False
        if False:#except: 
            print("Error on saving delivery note")
            sucess = False
    else: 
        print("Delivery note not found")
        sucess = False
    
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess    
    
def create_delivery(user): # return dict
    print("create_delivery(" + str(user) + ")")
    try: lieferschein_db.connect()
    except: True
    
    client_name = get_client(0)
    client_name = str(client_name["name"])
    
    this_delivery = Lieferschein.create(linien="0", anzahl="1", bcode="", name="",
                                        preis="0.0", user=str(user),
                                        kunde_id="0", kunde_name = client_name, datum=str(Date()), datum_todo=str(TodayAddDays(7)),
                                        fertig=False, info="", maschine = "", total=0.0, total_htva=0.0, tva=str(float(standart_tva)), preis_htva="0.0", steuern="", total_ohne_steuern="0.0;0.0", rechnung="", lastchange=str(Timestamp()), angebot=False, bestellung=False )
    try:
        this_delivery.save()
        print("Delivery created: " + str(this_delivery.identification))
    except: print("Delivery creating ERROR")
    answer = model_to_dict(this_delivery)
    
    try: lieferschein_db.close()
    except: True
    
    #update_delivery_calendar()
    
    return answer        

    
def create_delivery_with_identification(identification, user): # return dict
    print("create_delivery_with_identification(" + str(identification) + ", " + str(user) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = int(identification)
    
    client_name = str(get_client(0)["name"])
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        print("delivery " + str(identification) + " exists!")
        this_delivery = Lieferschein.create(linien="0", anzahl="1", bcode="", name="",
                                        preis="0.0", user=str(user),
                                        kunde_id="0", kunde_name = client_name, datum=str(Date()), datum_todo=str(TodayAddDays(7)),
                                        fertig=False, info="", maschine = "", total=0.0, total_htva=0.0, tva=str(float(standart_tva)), preis_htva="0.0", steuern="", total_ohne_steuern="0.0;0.0", rechnung="", lastchange=str(Timestamp()), angebot=False, bestellung=False )    
    else:
        this_delivery = Lieferschein.create(identification=identification, linien="0", anzahl="1", bcode="", name="",
                                        preis="0.0", user=str(user),
                                        kunde_id="0", kunde_name = client_name, datum=str(Date()), datum_todo=str(TodayAddDays(7)),
                                        fertig=False, info="", maschine = "", total=0.0, total_htva=0.0, tva=str(float(standart_tva)), preis_htva="0.0", steuern="", total_ohne_steuern="0.0;0.0", rechnung="", lastchange=str(Timestamp()), angebot=False, bestellung=False )
    try:
        this_delivery.save()
        print("Delivery created: " + str(this_delivery.identification))
    except: print("Delivery creating ERROR")

    answer = model_to_dict(this_delivery)
    
    try: lieferschein_db.close()
    except: True
    
    return answer

def create_delivery_angebot(user): # return dict
    print("create_delivery_angebot()")
    this_delivery = create_delivery(user)
        
    try: lieferschein_db.connect()
    except: True
    
    this_delivery = dict_to_model(Lieferschein, this_delivery)
    this_delivery.angebot = True
    this_delivery.save()
    
    answer = model_to_dict(this_delivery)
    
    try: lieferschein_db.close()
    except: True
    
    return answer
    
def create_delivery_bestellung(user): # return dict
    print("create_delivery_bestellung()")
    this_delivery = create_delivery(user)
        
    try: lieferschein_db.connect()
    except: True
    
    this_delivery = dict_to_model(Lieferschein, this_delivery)
    this_delivery.bestellung = True
    this_delivery.save()
    
    answer = model_to_dict(this_delivery)
    
    try: lieferschein_db.close()
    except: True
    
    return answer

def delete_delivery_line (identification, index, username): # return bool(sucess)
    print("delete_delivery_line(" + str(identification) + ", " + str(index) + ", " + str(username) + ")")
    try: lieferschein_db.connect()
    except: True

    identification = str(identification)
    index = int(index)
    username = str(username)
    
    query = Lieferschein.select().where(Lieferschein.identification == identification)
    if query.exists():
        try:
            object = Lieferschein.get(Lieferschein.identification == str(identification))
            dict = model_to_dict(object)

            if dict["rechnung"] == "" or "RV" in dict["rechnung"]:
                barcode = dict["bcode"].split("|")[index]
                quantity = dict["anzahl"].split("|")[index]
                price = dict["preis"].split("|")[index]
                price = dict["tva"].split("|")[index]
                price = dict["preis_htva"].split("|")[index]

                dict["lastchange"] = libs.BlueFunc.Timestamp()
                for name in ["linien", "anzahl", "bcode", "name", "preis", "tva", "preis_htva"]:
                    name_list = dict[name].split("|")
                    del name_list[index]
                    dict[name] = "|".join(name_list)
                       
                this_delivery = dict_to_model(Lieferschein, dict)
                this_delivery.save()

                set_delivery_total(identification)

                if not dict["angebot"] and not dict["bestellung"]:# wenn kein Angebot und keine Bestellung
                    add_article({"identification": barcode, "add": float(quantity), "price": float(price), "user": str(username)})

                sucess = True
            else: sucess = False
        except: 
            print("Error on saving delivery note")
            sucess = False
    else: 
        print("Delivery note not found")
        sucess = False
    
    delete_delivery_pdf(identification)
    
    try: lieferschein_db.close()
    except: True
    
    return sucess    

def create_backup():
    print("create_backup()")
    
    if not os.path.exists("/etc/zk-data-server/web/tmp/Backup/" + server_name):
        os.system("mkdir -p  /etc/zk-data-server/web/tmp/Backup/" + server_name)
        os.system("chmod -R 777 /etc/zk-data-server/web/tmp/Backup/" + server_name)
        
    backup_file_name = "backup_" + libs.BlueFunc.Date().replace("-", "") + "_" + libs.BlueFunc.uhrzeit().replace(":", "") + "_" + server_name + ".tar.xz"
    backup_path = "/etc/zk-data-server/web/tmp/Backup/" + server_name + "/" + backup_file_name
    answer = "/tmp/Backup/" + server_name + "/" + backup_file_name
    
    cmd = "cd /etc/zk-data-server/data/ && tar -cJvf " + backup_file_name + " " + server_name + " && mv " + backup_file_name + " " + backup_path
    os.system(cmd)
    
    return answer

def set_client(Dict):# return Bool of sucess
    print("set_client")
    id = str(Dict["identification"])
    print("id: " + str(id))
    try: kunde_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True
    try: rechnung_db.connect()
    except: True
    try: rechnung_e_db.connect()
    except: True
    
    if "pcname" in Dict:
        del Dict["pcname"]
    if "user" in Dict:
        del Dict["user"]
    if "secure_key" in Dict:
        del Dict["secure_key"]
    
    if "tva_status" in Dict:
        del Dict["tva_status"]
    if "tva_status_text" in Dict:
        del Dict["tva_status_text"]
        
    if True:#try:
        Dict["lastchange"] = str(Timestamp())        
        
        if "ort" in Dict:
            Dict["ort"] = libs.BlueFunc.simple_text(Dict["ort"])
            
        if "adresse" in Dict:
            Dict["adresse"] = Dict["adresse"].upper().replace("\n", "")
            
        if "tva" in Dict:
            Dict["tva"] = libs.BlueFunc.ultra_simple_text(Dict["tva"])
            Dict["tva"] = libs.BlueFunc.simple_text(Dict["tva"])
            
        if "name" in Dict:
            Dict["name"] = libs.BlueFunc.simple_text(Dict["name"])
            Dict["name"] = Dict["name"].replace("&", "")
            
            if "alias" in Dict:
                name_content = Dict["name"] + " | " + str(Dict["alias"])
            else:
                name_content = Dict["name"]
                
            q = (Lieferschein.update({Lieferschein.kunde_name: name_content}).where(Lieferschein.kunde_id == Dict["identification"]))
            q.execute()
            
            q = (Rechnung.update({Rechnung.kunde_name: name_content}).where(Rechnung.kunde_id == Dict["identification"]))
            q.execute()
            q = (Kasse.update({Kasse.kunde_name: name_content}).where(Kasse.kunde_id == Dict["identification"]))
            q.execute()
            q = (RechnungVorlage.update({RechnungVorlage.kunde_name: name_content}).where(RechnungVorlage.kunde_id == Dict["identification"]))
            q.execute()
            
            q = (Rechnung_e.update({Rechnung_e.lieferant_name: name_content}).where(Rechnung_e.lieferant_id == Dict["identification"]))
            q.execute()
    
        for t in ["tel1", "tel2", "tel3", "tel4"]:
            if t in Dict:
                init_tel = Dict[t].rstrip().replace("+", "00")
                #if str(init_tel) == "": init_tel = "0"
                            
                new_tel = ''.join([i for i in init_tel if i.isdigit()])
                      
                if len(str(new_tel)) > 0:
                    if not str(new_tel)[0] == "0": new_tel = "0" + new_tel
                
                Dict[t] = libs.BlueFunc.simple_text(new_tel) # remove newlines

        if "payment_deadline" in Dict:
            try: Dict["payment_deadline"] = int(Dict["payment_deadline"])
            except: Dict["payment_deadline"] = 15

        ThisKunde = dict_to_model(Kunde, Dict)
        ThisKunde.save()
        Antwort = True


    if False: #except:
        Antwort = False
        print("Error saving client")
        
    try: kunde_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    
    return Antwort
    
def get_next_changed_client(timestamp):
    print("get_next_changed_client(" + str(timestamp) + ")")    
    
    try: kunde_db.connect()
    except: True
    
    timestamp = str(timestamp)
      
    query = Kunde.select().where(Kunde.lastchange > float(timestamp)).order_by(Kunde.lastchange)    
    if query.exists():
        answer = [ str(query[0].identification), float(query[0].lastchange) ]
    else:
        answer = ["", 0.0]
  
    try: kunde_db.close()
    except: True
    
    return answer

def get_next_changed_article(timestamp):
    print("get_next_changed_article(" + str(timestamp) + ")")    
    
    try: stock_db.connect()
    except: True
    
    timestamp = str(timestamp)   
    
    query = Artikel.select().where(Artikel.lastchange > float(timestamp)).order_by(Artikel.lastchange)    
    if query.exists():
        answer = [ str(query[0].identification), float(query[0].lastchange) ]
    else:
        answer = ["", 0.0]
      
    try: stock_db.close()
    except: True
    
    return answer

def get_article(Dict):# return Dict
    print("get_article(" + str(Dict) + ")")
    try: stock_db.connect()
    except: True

    if not "{" in str(Dict):
        print("Digit ?")
        #if Dict.isdigit(): 
        #    print("Digit -> TRUE")
        Dict = {"identification": Dict}
        #else: 
        #    print("Digit -> FALSE")
        #    Dict = {"identification": 0}

    try:
        bcode = str(Dict["identification"]).replace(".0", "")

        query = Artikel.select().where(Artikel.identification == bcode)
        print("get_article identification " + str(bcode) + " = " + str(query.exists()))
        if query.exists():
            object = Artikel.get(Artikel.identification == bcode)
            Antwort = model_to_dict(object)
            Antwort["name_de"] = Antwort["name_de"].rstrip()
            Antwort["name_de"] = Antwort["name_de"].replace("&", "+") # Fix, LibreOffice doesn't support & in content file
            Antwort["name_de"] = Antwort["name_de"].replace(",", "")

            Antwort["name_fr"] = Antwort["name_fr"].rstrip()
            Antwort["name_fr"] = Antwort["name_fr"].replace("&", "+") # Fix, LibreOffice doesn't support & in content file
            Antwort["name_fr"] = Antwort["name_fr"].replace(",", "")
            Antwort["beschreibung_de"] = Antwort["beschreibung_de"].replace("\"", "").replace(",", "")
            Antwort["beschreibung_fr"] = Antwort["beschreibung_fr"].replace("\"", "").replace(",", "")
            
            if Antwort["categorie"] == "": Antwort["categorie"] = 0
            else: Antwort["categorie"] = int(Antwort["categorie"])
            
            Antwort["preisek"] = round(Antwort["preisek"], 4)
            Antwort["preisvkh"] = round(Antwort["preisvkh"], 4)
            Antwort["preisvk"] = round(Antwort["preisvk"], 2)
            Antwort["anzahl"] = round(Antwort["anzahl"], 2)
        else:
            Antwort = {}
    except:
        barcode = int(Dict["barcode"])
        query = Artikel.select().where(Artikel.barcode == barcode)
        print("get_article barcode")
        if query.exists():
            object = Artikel.get(Artikel.barcode == barcode)
            Antwort = model_to_dict(object)
        else:
            Antwort = {}

    print("Antwort: " + str(Antwort))
    try: stock_db.close()
    except: True
    
    return Antwort

def get_invoices_file(year):# return file url
    print("get_invoices_file(" + str(year) + ")")
    try: invoice_db.connect()
    except: True
    
    year = str(year)
    if "-" in year: year.split("-")[0] # if date ist send, only take year
    
    invoice_list = []
    for invoice_object in Rechnung.select().where(Rechnung.datum.contains(year)):
        invoice_as_dict = model_to_dict(invoice_object)
        invoice_as_dict["identification"] = "R" + str(invoice_as_dict["identification"])
        invoice_list.append(invoice_as_dict)
    for invoice_object in Kasse.select().where(Kasse.datum.contains(year)):
        invoice_as_dict = model_to_dict(invoice_object)
        invoice_as_dict["identification"] = "K" + str(invoice_as_dict["identification"])
        invoice_list.append(invoice_as_dict)
    
    try: invoice_db.close()
    except: True
    
    print("invoice_list: " + str(len(invoice_list)))
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/invoices_" + str(year)
    answer = "/zk-data/file/files/" + server_name + "/invoices_" + str(year)
    
    open(pdf_path, "w").write(json.dumps(invoice_list))    
    
    return answer

def get_deliverys_file(year):# return file url
    print("get_deliverys_file(" + str(year) + ")")
    try: lieferschein_db.connect()
    except: True
    
    year = str(year)
    delivery_list = []
    
    for delivery_object in Lieferschein.select().where(Lieferschein.datum.contains(year)):
        delivery_as_dict = model_to_dict(delivery_object)            
        delivery_list.append(delivery_as_dict)
    
    try: lieferschein_db.close()
    except: True
    
    print("delivery_list: " + str(len(delivery_list)))
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/deliverys_" + str(year)
    answer = "/zk-data/file/files/" + server_name + "/deliverys_" + str(year)
    
    open(pdf_path, "w").write(json.dumps(delivery_list))
    return answer

def get_articles_file():# return file url
    print("get_articles_file()")
    try: stock_db.connect()
    except: True
    
    articles_list = []
    for article_object in Artikel.select():
        article_as_dict = model_to_dict(article_object)            
        articles_list.append(article_as_dict)
    
    try: stock_db.close()
    except: True
    
    #print("articles_list: " + str(len(articles_list)))
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/articles"
    answer = "/zk-data/file/files/" + server_name + "/articles"
    
    open(pdf_path, "w").write(json.dumps(articles_list))
    return answer

def get_pdfs_file():# return file url
    print("get_pdfs_file()")
    
    pdf_list = []
    
    #try: rechnung_pdf_db.connect()
    #except: True
    #for pdf_object in Rechnung_pdf.select(): 
    #    pdf_list.append(pdf_object.identification)
    #try: rechnung_pdf_db.close()
    #except: True
    
    #<TODO> need to adapt for new structure
    #try: lieferschein_pdf_db.connect()
    #except: True
    #for pdf_object in Lieferschein_pdf.select():
    #    pdf_list.append("L" + str(pdf_object.identification))
    #try: lieferschein_pdf_db.close()
    #except: True
    '''
    try: kassenbuch_pdf_db.connect()
    except: True
    for pdf_object in Kassenbuch_pdf.select():
        pdf_list.append("KB" + str(pdf_object.identification))
    try: kassenbuch_pdf_db.close()
    except: True
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/pdfs"
    answer = "/zk-data/file/files/" + server_name + "/pdfs"
    
    open(pdf_path, "w").write(json.dumps(pdf_list))
    '''
    
    return "answer"

def get_clients_file():# return file url
    print("get_clients_file()")
    try: kunde_db.connect()
    except: True
    
    client_list = []
    for client_object in Kunde.select():
        client_as_dict = model_to_dict(client_object)            
        client_list.append(client_as_dict)
    
    try: kunde_db.close()
    except: True
    
    print("client_list: " + str(len(client_list)))
    
    if not os.path.exists("/var/www/html/zk-data/file/files/" + server_name):
        os.system("mkdir -p /var/www/html/zk-data/file/files/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/file/files/" + server_name)
    
    pdf_path = "/var/www/html/zk-data/file/files/" + server_name + "/clients"
    answer = "/zk-data/file/files/" + server_name + "/clients"
    
    open(pdf_path, "w").write(json.dumps(client_list))
    return answer
    
def get_client(identification):# return Dict
    print("get_client(" + str(identification) + ")")
    try: kunde_db.connect()
    except: True

    if True:#try:
        query = Kunde.select().where(Kunde.identification == str(identification))
        if query.exists():            
            object = Kunde.get(Kunde.identification == str(identification))
            Antwort = model_to_dict(object)
            
            if not Antwort["tva"] == "":
                check_vat_dict = check_vat(Antwort["tva"], str(identification))
                Antwort["tva_status"] = check_vat_dict["status"]
                Antwort["tva_status_text"] = check_vat_dict["status_text"]
            
            #rappel = set_client_rappel(identification)
            #Antwort["rappel"] = float(rappel)
            
            print("get_client(" + str(identification) + ") = " + str(Antwort))
        else:
            print("Client " + str(identification) + " not found")
            Antwort = {}
    if False:#except:
        print("Client " + str(identification) + " -> Error")
        Antwort =  {}
    
    try: kunde_db.close()
    except: True
    
    return Antwort

def get_all_predictions():
    if ai_helper:
        try:
            answer = ai_helper.get_all_predictions()
        except:
            answer = False
    else:
        answer = "ai_helper disabled"
        
    return answer
    
def add_to_prediction(prediction_name, answer, word_data):
    if ai_helper:
        try:
            ai_helper.add_to_prediction(prediction_name, answer, word_data)
            answer = True
        except:
            answer = False
    else:
        answer = "ai_helper disabled"
        
    return answer
    
def make_prediction(prediction_name, word_data):
    if ai_helper:
        try:
            answer = ai_helper.make_prediction(prediction_name, word_data)
        except:
            answer = "Error"
    else:
        answer = "ai_helper disabled"
        
    return answer
                    
def set_article(Dict):# return Bool of sucess
    print("set_article")
    try: stock_db.connect()
    except: True
    
    article_exists = False
    if "identification" in Dict:
        query = Artikel.select().where(Artikel.identification == int(Dict["identification"]))
        if query.exists():
            article_exists = True
            old_article_data = model_to_dict(query[0])
    
    if article_exists:
        Dict["lastchange"] = str(Timestamp())
        
        if "pcname" in Dict:
            del Dict["pcname"]
        if "user" in Dict:
            username = Dict["user"]
            del Dict["user"]
        else:
            username = "NONAME"
            
        if "secure_key" in Dict:
            del Dict["secure_key"]
            
        ## Wieso ???? Entfernen ???? <TODO>
        if "article" in Dict:
            for key in Dict["article"]:
                Dict[key] = Dict["article"][key]
            del Dict["article"]
            
        for artikel_word in ["artikel", "artikel2", "artikel3", "artikel4"]:
            if artikel_word in Dict:
                old_article_string = str(Dict[artikel_word]).upper()
                Dict[artikel_word] = ""
                for character in old_article_string:
                    if character.isalpha() or character.isdigit():
                        Dict[artikel_word] = Dict[artikel_word] + str(character)
         
        for preis_word in ["preisek", "preisvkh", "preisvk", "anzahl", "minimum", "tva"]:
            if preis_word in Dict:
                try: Dict[preis_word] = float(str(Dict[preis_word]).replace(",", "."))
                except: Dict[preis_word] = 0.0
            
        if "preisvk" in Dict and "tva" in Dict:
            Dict["preisvk"] = round(float(Dict["preisvk"]), 2)
            Dict["tva"] = round(float(Dict["tva"]), 2)
            Dict["preisvkh"] = round(Dict["preisvk"] / (Dict["tva"]/100 + 1), 4)
        elif "preisvkh" in Dict and "tva" in Dict:
            Dict["preisvkh"] = round(float(Dict["preisvkh"]), 2)
            Dict["tva"] = round(float(Dict["tva"]), 2)
            Dict["preisvk"] = round(Dict["preisvkh"] * (Dict["tva"]/100 + 1), 2)
        else:
            if "preisvk" in Dict: del Dict["preisvk"]
            if "preisvkh" in Dict: del Dict["preisvkh"]
            
        id = str(Dict["identification"])
        print("id: " + str(id))
    
        if "name_fr" in Dict and "name_de" in Dict:
            if Dict["name_fr"] == "":
                if not Dict["name_de"] == "":
                    Dict["name_fr"] = libs.BlueFunc.translate("DE", "FR", Dict["name_de"])
                    Dict["name_fr"] = Dict["name_fr"]["text"]
            else:
                if Dict["name_de"] == "":
                    Dict["name_de"] = libs.BlueFunc.translate("FR", "DE", Dict["name_fr"])
                    Dict["name_de"] = Dict["name_de"]["text"]
                
            Dict["name_fr"] = Dict["name_fr"].replace("\n\r", " ")
            Dict["name_fr"] = Dict["name_fr"].replace("\n", " ")
            Dict["name_fr"] = Dict["name_fr"].rstrip()
            
            Dict["name_de"] = Dict["name_de"].replace("\n\r", " ")
            Dict["name_de"] = Dict["name_de"].replace("\n", " ")
            Dict["name_de"] = Dict["name_de"].rstrip()
        
        if "lieferant" in Dict:
            Dict["lieferant"] = str(Dict["lieferant"]).upper().rstrip()
            
        if "categorie" in Dict:
            if Dict["categorie"] == "": Dict["categorie"] = 0        

        if "bild" in Dict and "name_de" in Dict and "artikel" in Dict and "lieferant" in Dict:
            if Dict["bild"] == "" and not Dict["name_de"] == "" and not Dict["artikel"] == "" and not Dict["lieferant"] == "":
                try:
                    search = Dict["lieferant"] + " " + Dict["artikel"] + " " + Dict["name_de"]
                    Dict["bild"] = libs.get_image_online.search_image_online(search)
                    Dict["image_nextcheck_timestamp"] = float(libs.BlueFunc.timestamp()) + 2592000 # 30 days
                except:
                    Dict["bild"] = ""
                    Dict["image_nextcheck_timestamp"] = 0.0
        
        ThisArtikel = dict_to_model(Artikel, Dict)        
        ThisArtikel.save()

        Antwort = True
        
    if False:#except:
        Antwort = False
        
    try: stock_db.close()
    except: True

    if "anzahl" in Dict: # save move
        if "preisvk" in Dict:
            add_article({"identification": Dict["identification"], "final_quantity": float(Dict["anzahl"]), "price": float(Dict["preisvk"]), "user": username})
        else:
            add_article({"identification": Dict["identification"], "final_quantity": float(Dict["anzahl"]), "user": username})
    return Antwort
    
def get_cashbook_date_last(konto):
    try: transaktion_db.connect()
    except: True
    
    konto = int(konto)
    
    test_query = Transaktion.select().where(Transaktion.konto_in == konto).order_by(Transaktion.datum)
    if test_query.exists():
        answer = str(test_query[-1].datum)
    else:
        answer = False
        
    try: transaktion_db.close()
    except: True
    
    return answer

def get_cashbook_date_before(konto, datum):
    try: transaktion_db.connect()
    except: True
    
    konto = int(konto)
    test_timeout = 1000
    
    while True:
        test_date = DateAddDays(datum, -1)    
        test_query = Transaktion.select().where(Transaktion.konto_in == konto)
        if test_query.exists():
            test_query = test_query.where(Transaktion.datum.contains(test_date))
            if test_query.exists():
                answer = str(test_query[0].datum)
                break
            else:
                datum = test_date
        else:
            answer = False
            break
            
        test_timeout = test_timeout - 1
        if test_timeout == 0: # no more transaktions ? return first
            test_query = Transaktion.select().where(Transaktion.konto_in == konto).order_by(Transaktion.datum)
            if test_query.exists(): answer = str(test_query[0].datum)
            else: answer = False
            break
        
    try: transaktion_db.close()
    except: True
    
    return answer

def get_cashbook_date_after(konto, datum):
    try: transaktion_db.connect()
    except: True
    
    konto = int(konto)
    test_timeout = 10000
    
    while True:
        test_date = DateAddDays(datum, 1)    
        test_query = Transaktion.select().where(Transaktion.konto_in == konto)
        if test_query.exists():
            test_query = test_query.where(Transaktion.datum.contains(test_date))
            if test_query.exists():
                answer = str(test_query[0].datum)
                break
            else:
                datum = test_date
        else:
            answer = False
            break
            
        test_timeout = test_timeout - 1
        if test_timeout == 0: # no more transaktions ? return last
            print("no more transaktions ? return last")
            test_query = Transaktion.select().where(Transaktion.konto_in == konto).order_by(Transaktion.datum)
            if test_query.exists(): answer = str(test_query[-1].datum)
            else: answer = False
            break
        
    try: transaktion_db.close()
    except: True
    
    return answer

def get_cashbook_pdf(konto, datum):# print cashbook return path or False
    print("get_cashbook_pdf(" + str(konto) + "," + str(datum) + ")")

    konto = int(konto)
    datum = str(datum)
    
    date_list = datum.split("-")

    new_pdf_path = "/etc/zk-data-server/data/" + server_name + "/files/cashbook/" + date_list[0] + "/" + date_list[1] + "/" + date_list[2] + "/"
    if not os.path.exists(new_pdf_path): os.system("mkdir -p " + new_pdf_path)
    new_pdf_filename = new_pdf_path + str(konto) + "_" + str(datum) + ".pdf"
        
    if not os.path.exists(new_pdf_filename):
        print("PDF Doesn't exist -> create it")
        if str(datum) == libs.BlueFunc.Date(): # if Today
            new_pdf_filename = create_cashbook_pdf(konto, datum) # create pdf in /tmp
        else:
            new_tmp_pdf_path = create_cashbook_pdf(konto, datum) # create pdf in /tmp
            os.system("mv " + new_tmp_pdf_path + " " + new_pdf_filename)
            
    client_destination_path = libs.ServerTools.export_to_web_tmp(new_pdf_filename)
    answer = client_destination_path

    
    """
    
    try: kassenbuch_pdf_db.connect()
    except: True
    
    if not os.path.exists("/var/www/html/zk-data/Kassenbuch/" + server_name):
        os.system("mkdir -p  /var/www/html/zk-data/Kassenbuch/" + server_name)
        os.system("chmod -R 777 /var/www/html/zk-data/Kassenbuch/" + server_name)

    pdf_path = "/var/www/html/zk-data/Kassenbuch/" + server_name + "/" + str(konto) + "-" + str(datum) + ".pdf"
    answer = "/zk-data/Kassenbuch/" + server_name + "/" + str(konto) + "-" + str(datum) + ".pdf"
    
    if str(datum) == libs.BlueFunc.Date(): # if Today
        new_pdf_path = get_cashbook(konto, datum)  # create pdf
    else:
        query_pdf = Kassenbuch_pdf.select().where(Kassenbuch_pdf.identification == str(konto) + "-" + str(datum))    
        if query_pdf.exists():
            print("PDF exist")
            if not os.path.exists(pdf_path):
                open(pdf_path, "wb").write(query_pdf[0].pdf)
        else:
            print("PDF Doesn't exist -> create it")
            new_pdf_path = get_cashbook(konto, datum)  # create pdf
            print("new_pdf_path: " + str(new_pdf_path))
            if str(new_pdf_path) == "False":
                answer = False
            else:
                new_pdf_path = "/var/www/html" + new_pdf_path
                pdf_blob = open(new_pdf_path, "rb").read() # read pdf
                new_pdf = Kassenbuch_pdf.create(identification = str(konto) + "-" + str(datum), pdf = pdf_blob) # write it to database

    try: kassenbuch_pdf_db.close()
    except: True
    """
    
    return answer
    
def create_cashbook_pdf(konto, datum):# print cashbook return path or False
    print("create_cashbook_pdf(" + str(konto) + ", " + str(datum) + ")")
    
    try: transaktion_db.connect()
    except: True
    try: kunde_db.connect()
    except: True
    try: rechnung_db.connect()
    except: True
    try: rechnung_e_db.connect()
    except: True

    gewinn = {}
    solde = {}
    tagestand = 0.0
    total_vorher = 0.0
    #gewinn_pro_tag = {}
    #verlust_pro_tag = {}
    
    test_query = Transaktion.select().where(Transaktion.datum.contains(datum))
    test_query = test_query.where(Transaktion.konto_in == konto)
    if not test_query.exists():
        return False
    
    query = Transaktion.select().order_by(Transaktion.datum)

    for transaktion in query:  
        if transaktion.konto_in == konto:    
            if transaktion.datum in gewinn:
                gewinn[transaktion.datum] = gewinn[transaktion.datum] + float(transaktion.betrag)
            else:
                gewinn[str(transaktion.datum)] = float(transaktion.betrag)                
            
            gewinn[str(transaktion.datum)] = round(gewinn[str(transaktion.datum)], 2)
        
    for tag in gewinn:
        solde[tag] = gewinn[tag] + tagestand
        solde[tag] = round(solde[tag], 2)
        
        #print(str(tag))
        #print(str(gewinn[tag]) + " + " + str(tagestand)  + " = " + str(solde[tag]) )
        
        tagestand = tagestand + gewinn[tag]
        tagestand = round(tagestand, 2)
    
    transaktions = Transaktion.select().where(Transaktion.konto_in == konto).order_by(Transaktion.datum)
    transaktions = transaktions.where(Transaktion.datum.contains(datum)).order_by(Transaktion.datum)
    
    timestamp = str(Timestamp())
    
    #os.system("unzip -q /etc/zk-data-server/data/" + server_name + "/VorlageKassenbuch.ods -d /tmp/kassenbuch-tmp-" + timestamp)
    #daten = open("/tmp/kassenbuch-tmp-" + timestamp + "/content.xml", "r").read()
    
    default_ods_file = "/etc/zk-data-server/data/" + server_name + "/VorlageKassenbuch.ods"
    daten = libs.ServerTools.get_ods_content(server_name, default_ods_file)
          
    # linien pro seite
    printing_max_line_integer = 0
    for x in range(0, 100):
        if "@" + str(x) + "_datum@" in daten:
            printing_max_line_integer += 1
    print("printing_max_line_integer: " + str(printing_max_line_integer))
        
    # seitenanzahl 
    print("Anzahl der Transaktionen: " + str(len(transaktions)))       
    page_total = int(round(len(transaktions) / printing_max_line_integer, 1))
    
    if (len(transaktions) / printing_max_line_integer) - page_total > 0:
        page_total = page_total + 1
    print("page_total: " + str(page_total) )
    
    printing_total_line_integer = page_total * printing_max_line_integer
    print("printing_total_line_integer: " + str(printing_total_line_integer) )
    
    document_id = str(konto) + "-" + datum
    
    #if not os.path.exists("/var/www/html/zk-data/Kassenbuch/" + server_name):
    #    os.system("mkdir -p /var/www/html/zk-data/Kassenbuch/" + server_name)
    #    os.system("chmod -R 777 /var/www/html/zk-data/Kassenbuch/" + server_name)
        
    index = 0
    for page_integer in range(0, page_total):
        print("page_integer: " + str(page_integer))
        
        einnahmen = 0.0
        total_vorher = 0.0
        einnahmen_tag = 0.0
        ausgaben_tag = 0.0
        
        # print
        #os.system("rm -r /tmp/kassenbuch-tmp-" + timestamp)
        #os.system("unzip -q /etc/zk-data-server/data/" + server_name + "/VorlageKassenbuch.ods -d /tmp/kassenbuch-tmp-" + timestamp)
        #daten = open("/tmp/kassenbuch-tmp-" + timestamp + "/content.xml", "r").read()
    	
        daten = libs.ServerTools.get_ods_content(server_name, default_ods_file)
        list_of_files = []
    
        for line in range(0, printing_max_line_integer):
            #print("index: " + str(index))
            #print("len(transaktions): " + str(len(transaktions)))
            
            if index > len(transaktions)-1:
                daten = daten.replace("@" + str(line) + "_info@", "")
                daten = daten.replace("@" + str(line) + "_datum@", "")
                daten = daten.replace("@" + str(line) + "_transaktion@", "")
                daten = daten.replace("@" + str(line) + "_-@", "")
                daten = daten.replace("@" + str(line) + "_+@", "")      
           
            else:
                #transaktions[index].datum = transaktions[index].datum.split("-")[1] + "-" + transaktions[index].datum.split("-")[2]
                if not transaktions[index].rechnung == "":
                
                    if "RE" in transaktions[index].rechnung:
                        rechnung = Rechnung_e.get(Rechnung_e.identification == transaktions[index].rechnung.replace("RE", ""))
                        kunde = Kunde.get(Kunde.identification == rechnung.lieferant_id)
                        
                        daten = daten.replace("@" + str(line) + "_info@", "Rechnung E.: " + str(rechnung.identification) +  ": " + str(kunde.identification) + " " + kunde.name)
                    else:
                        if "R" in transaktions[index].rechnung:
                            rechnung = Rechnung.get(Rechnung.identification == transaktions[index].rechnung.replace("R", ""))
                            kunde = Kunde.get(Kunde.identification == rechnung.kunde_id)
                            
                            daten = daten.replace("@" + str(line) + "_info@", "Rechnung: " + str(rechnung.identification) +  ": " + str(kunde.identification) + " " + kunde.name)
                        else:
                            rechnung = Kasse.get(Kasse.identification == transaktions[index].rechnung.replace("K", ""))
                            kunde = Kunde.get(Kunde.identification == rechnung.kunde_id)
                            
                            daten = daten.replace("@" + str(line) + "_info@", "Kassenverkauf " + str(rechnung.identification) + ": " + str(kunde.identification) + " " + kunde.name)
                    
                    tva = round(rechnung.total - rechnung.total_htva, 2)
                else:
                    mitteilung = transaktions[index].mitteilung
                    mitteilung = mitteilung.replace("&", "-")
                    
                    daten = daten.replace("@" + str(line) + "_info@", mitteilung)
                    
                
                daten = daten.replace("@" + str(line) + "_datum@", transaktions[index].datum)
                daten = daten.replace("@" + str(line) + "_transaktion@", str(transaktions[index].identification))
                
                if transaktions[index].betrag > 0: # +
                    daten = daten.replace("@" + str(line) + "_+@", str(transaktions[index].betrag).replace(".", ","))
                    daten = daten.replace("@" + str(line) + "_-@", "")
                    einnahmen_tag = einnahmen_tag + float(transaktions[index].betrag)
                else: # -
                    daten = daten.replace("@" + str(line) + "_+@", "")
                    daten = daten.replace("@" + str(line) + "_-@", str(transaktions[index].betrag).replace(".", ","))
                    ausgaben_tag = ausgaben_tag - float(transaktions[index].betrag)      
            
                einnahmen = einnahmen + float(transaktions[index].betrag)
                einnahmen = round(einnahmen, 2)
                
        ####################################
                if total_vorher == 0.0:
                    print("datum: " + str(transaktions[index].datum))
                    total_vorher_index = list(solde.keys()).index(transaktions[index].datum) - 1
                    total_vorher_key = list(solde.keys())[total_vorher_index]
                    print("datum vorher: " + str(total_vorher_key))
                    
                    total_vorher = solde[total_vorher_key]
                      
        ####################################    
            index = index + 1
        daten = daten.replace("@einnahmen_tag@", str(round(einnahmen_tag, 2)))
        daten = daten.replace("@ausgaben_tag@", str(round(ausgaben_tag, 2)))
    
        daten = daten.replace("@einnahmen@", str(einnahmen))
        daten = daten.replace("@total_vorher@", str(total_vorher))
        daten = daten.replace("@total@", str(round(total_vorher+einnahmen,2)))
    
        # replace content
        #for x in range(0, printing_max_line_integer):
        #   daten = daten.replace("@datum_0@", "")
        
        # compress and move file    
        #open("/tmp/kassenbuch-tmp-" + timestamp + "/content.xml", "w").write(daten)
        #os.system("cd /tmp/kassenbuch-tmp-" + timestamp + " && zip -r -q kassenbuch-tmp-" + timestamp + ".ods *")
        #os.system("cd /tmp/ && libreoffice --convert-to pdf:writer_pdf_Export kassenbuch-tmp-" + timestamp + "/kassenbuch-tmp-" + timestamp + ".ods && mv kassenbuch-tmp-" + timestamp + ".pdf /tmp/" + document_id + "_" + str(page_integer) + ".pdf && rm -r kassenbuch-tmp-" + timestamp)
    	
        # write file
        new_file = libs.ServerTools.write_ods_file(server_name, default_ods_file, daten, [])
        list_of_files.append(new_file)
        
    
    #print("Drucken " + str(page_total) + " pages")
    #if page_total > 1:
    #    command = "pdftk "
    #    for page_integer in range(0, page_total):
    #        command = command + "/tmp/" + document_id + "_" + str(page_integer) + ".pdf "
    # 
    #    command = command + "cat output " + "/var/www/html/zk-data/Kassenbuch/" + server_name + "/" + document_id + ".pdf"
    #    print("command: " + str(command))
    #    os.system(command)
    #else:
    #    os.system("mv -f /tmp/" + document_id + "_0.pdf /var/www/html/zk-data/Kassenbuch/" + server_name + "/" + document_id + ".pdf")
    #              
    #answer = "/zk-data/Kassenbuch/" + server_name + "/" + str(document_id) + ".pdf" 

	
    answer = libs.ServerTools.convert_ods_to_pdf(list_of_files)
    

    try: kunde_db.close()
    except: True
    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    try: transaktion_db.close()
    except: True
    
    return answer


def get_mwst_data(datum):
    print("get_mwst_data(" + str(datum) + ")")    
    
    rechnung_db.connect()
    rechnung_e_db.connect()
    
    einnahmen = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 6: 0.0, 7: 0.0, 8: 0.0, 9: 0.0, 10: 0.0, 11: 0.0, 12: 0.0}
    einnahmen_mwst = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 6: 0.0, 7: 0.0, 8: 0.0, 9: 0.0, 10: 0.0, 11: 0.0, 12: 0.0}
    ausgaben = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 6: 0.0, 7: 0.0, 8: 0.0, 9: 0.0, 10: 0.0, 11: 0.0, 12: 0.0}
    ausgaben_mwst = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 6: 0.0, 7: 0.0, 8: 0.0, 9: 0.0, 10: 0.0, 11: 0.0, 12: 0.0}

    
    rechnungen = Rechnung.select().where(Rechnung.datum.contains(datum)) + Kasse.select().where(Kasse.datum.contains(datum))
    for rechnung in rechnungen:
        monat = int(rechnung.datum.split("-")[1])
        
        einnahmen[monat] = einnahmen[monat] + rechnung.total
        einnahmen[monat] = round(einnahmen[monat], 2)
            
        steuern = rechnung.total - rechnung.total_htva
        einnahmen_mwst[monat] = einnahmen_mwst[monat] + steuern
        einnahmen_mwst[monat] = round(einnahmen_mwst[monat], 2)
    
      
    rechnungen = Rechnung_e.select().where(Rechnung_e.datum.contains(datum))
    for rechnung in rechnungen:
        monat = int(rechnung.datum.split("-")[1])
        
        ausgaben[monat] = ausgaben[monat] + rechnung.total
        ausgaben[monat] = round(ausgaben[monat], 2)
            
        ausgaben_mwst[monat] = float(ausgaben_mwst[monat]) + float(rechnung.steuern_abziebar)
        ausgaben_mwst[monat] = round(ausgaben_mwst[monat], 2)
         
    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    
    return {"einnahmen": einnahmen, "einnahmen_mwst": einnahmen_mwst, "ausgaben": ausgaben, "ausgaben_mwst": ausgaben_mwst}

def add_article(Dict):# return Bool of sucess
    #global BeID
    print("add_article(" + str(Dict) + ")")
    id = str(Dict["identification"])

    try: user = Dict["user"]
    except: user = "NONAME"

    try: stock_db.connect()
    except: True

    #BeID = getFreieBewegungID()

    try: object = Artikel.get(Artikel.identification == id)
    except: return False
    object.lastchange = float(Timestamp())
    
    if "final_quantity" in Dict:
        add = float(Dict["final_quantity"]) - float(object.anzahl)
        del Dict["final_quantity"]
    else:
        add = str(Dict["add"])
        del Dict["add"]
        
    print("add: " + str(id) +  "->" + str(add))
    
    print("start Anzahl " + str(object.anzahl))
    start = float(object.anzahl)
    if "-" in str(add):
        print(" - ")
        add = float(add[1:])
        StartErgebnis = object.anzahl
        Anzahl = float(add)
        object.anzahl = object.anzahl - float(add)
        EndErgebnis = object.anzahl        
    else:
        print(" + ")
        object.anzahl = object.anzahl + float(add)
        
    print("end Anzahl " + str(object.anzahl))
    
    if "datum" in Dict:
        bewegung_datum = str(Dict["datum"])
    else:
        bewegung_datum = str(Date())

    if "price" in Dict:
        bewegung_preisvk = libs.RoundUp.RoundUp05( float(Dict["price"]) )
        bewegung_preisvkh = libs.RoundUp.RoundUp00(bewegung_preisvk / 1.21)
    else:    
        bewegung_preisvk = object.preisvk
        bewegung_preisvkh = object.preisvkh
    
    if object.anzahl < 0:
        Antwort = False
        #object.anzahl = 0.0
        #start = object.anzahl + float(add)
    else:
        object.save()
        end = float(object.anzahl)
    
        object2 = Bewegung.create(bcode=str(id),
                                      datum=bewegung_datum,
                                      start=start,
                                      end=end,
                                      preisek=object.preisek,
                                      preisvkh=bewegung_preisvkh,
                                      preisvk=bewegung_preisvk,
                                      mode="MISC",
                                      user=str(user))                   
        object2.save()
    
        Antwort = True
    
    try: stock_db.close()
    except: True

    return Antwort

def GetGewinn(Dict): #return Float
    print("GetGewinn")
    
    try: stock_db.connect()
    except: True
    query = Bewegung.select().where(Bewegung.datum == str(Dict["datum"]))
    
    try: stock_db.close()
    except: True
    
    Summe = 0.0
    print(str(query.count()) + " Bewegungen für " + str(Dict["datum"]))
    for Each in query:
        print("Bewegung= " + str(model_to_dict(Each)))
        Anzahl = Each.start - Each.end
        if Anzahl > 0:
            Summe = Summe + ((Each.preisvkh - Each.preisek) * Anzahl)
    
    return Summe

def create_article(new_id):# return Dict
    global FreeID
    print("create_article(" + str(new_id) + ")")

    try: stock_db.connect()
    except: True
    try: categorie_db.connect()
    except: True    

    new_id_copy = new_id
    new_id = ""
    for char in str(new_id_copy):
        if char.isdigit():
            new_id = new_id + str(char)
    print("new_id without Char: " + str(new_id))
     
    try:
        FreeID = int(FreeID)
    except:
        FreeID = 0
             
    try:
        new_id = int(new_id)
    except:
        new_id = FreeID   
     
    while True:
        query = Artikel.select().where(Artikel.identification == int(new_id))
        if not query.exists():
            break
        new_id = new_id + 1
    print("new_id: " + str(new_id))

    ThisArtikel = Artikel.create(creation=str(Date()),
                                 barcode=int(libs.barcode.IDToBarcode(new_id)),
                                 lastchange=str(Timestamp()),
                                 identification=int(new_id),
                                 name_de="",
                                 name_fr="",
                                 artikel="",
                                 artikel2="",
                                 artikel3="",
                                 artikel4="",
                                 artikel5="",
                                 artikel6="",
                                 lieferant="",
                                 preisek=0.0,
                                 preisvkh=0.0,
                                 preisvk=0.0,
                                 tva=standart_tva,
                                 anzahl=0.0,
                                 ort="",
                                 minimum=0.0,
                                 categorie=0,
                                 bild="",
                                 image_nextcheck_timestamp=0.0,
                                 beschreibung_de="",
                                 beschreibung_fr="",
                                 baujahr=0,
                                 web=False,
                                 farbe="",
                                 groesse="",
                                 promo=0.0)

    ThisArtikel.save()

    object = Artikel.get(Artikel.identification == int(new_id))
    Antwort = model_to_dict(object)

    try: stock_db.close()
    except: True
    try: categorie_db.close()
    except: True
    
    return Antwort

def get_article_average ( identification ): # return average per year (365days)
    print("get_article_average(" + str(identification) + ")")

    total_quantity_sell = 0
    first_date = ""
    last_date = ""
    x = "-1"
    while True:
        if x == "0": break
        else:
            move = get_article_move(x, identification)
            print("move: " + str(move))
        
            if move == {} or move == {"identification": "0"}: break

            quantity_sell = float(move["start"]) - float(move["end"])
            if quantity_sell > 0:
                total_quantity_sell += quantity_sell
                #print(move)
       
                first_date = move["datum"]

            if x == "-1": last_date = move["datum"]
            x = move["identification"]
        
    print("first_date: " + str(first_date))
    print("last_date: " + str(last_date))
    if not first_date == "":
        first_date_days = int(first_date.split("-")[0])*365 + int(first_date.split("-")[1])*30 + int(first_date.split("-")[2])
        last_date_days = int(last_date.split("-")[0])*365 + int(last_date.split("-")[1])*30 + int(last_date.split("-")[2])
        delta_days = last_date_days - first_date_days

        print("total_quantity_sell: " + str(total_quantity_sell))
        print("first_date: " + str(first_date))
        print("last_date: " + str(last_date))
        print("delta_days: " + str(delta_days))

        if float(delta_days) == 0.0: average = 0.0
        else: average = float(total_quantity_sell) / float(delta_days) * 365
        print("average: " + str(average))
    else:
        print("first_date not existing -> average = 0.0")
        average = 0.0
        
    return float(average)


def get_article_move ( identification , bcode ): # return next move of article
    print("get_article_move(" + str(identification) + ", " + str(bcode) + ")")
    try: stock_db.connect()
    except: True

    identification = str(identification)
    bcode = str(bcode)

    query = Bewegung.select().where(Bewegung.bcode == bcode)
    if query.exists():
        query = query.order_by(Bewegung.datum)
        print("query: " + str(query))
        
        if identification == "-1":
            move = query[-1]
            Antwort = model_to_dict(move)
        else:
            move_identification = 0
            for eachMove in query:
                if int(eachMove.identification) > move_identification and int(eachMove.identification) < int(identification):
                    move_identification = int(eachMove.identification)
            
            if not move_identification == 0:
                query = Bewegung.select().where(Bewegung.identification == move_identification)  
                move = query[0]
                print(move)
                Antwort = model_to_dict(move)
            else:
                Antwort = {"identification": "0"}
    else: Antwort = {"identification": "0"}

    try: stock_db.close()
    except: True

    return Antwort

def get_marchants(): # return List of marchants
    print("get_marchants()")

    try: stock_db.connect()
    except: True

    marchants = []
    for article in Artikel.select():
        if not article.lieferant == "":
            if not str(article.lieferant).upper() in marchants:
                marchants.append(str(article.lieferant).upper())
                
    try: stock_db.close()
    except: True
    
    return marchants

def get_article_sell(identification, date): # return number at date
    print("get_article_sell(" + str(identification) + ", " + str(date) + ")")
    
    try: rechnung_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True
    
    quantity = 0.0
    identification = str(identification)
    
    invoices = Rechnung.select().where(Rechnung.datum.contains(date))    
    invoices = invoices + Kasse.select().where(Kasse.datum.contains(date))
    for invoice in invoices:
        #print("invoice: " + str(invoice))        
        for delivery_id in invoice.lieferscheine.split("|"):
            #print("delivery_id: " + str(delivery_id))
            delivery_query = Lieferschein.select().where(Lieferschein.identification == delivery_id)
            if delivery_query.exists():
                if identification in delivery_query[0].bcode:
                    for index in range(0, len(delivery_query[0].bcode.split("|"))):
                        #print("index: " + str(index))
                        bcode = str(delivery_query[0].bcode.split("|")[index])
                        if bcode == identification:
                            quantity_to_add = float(delivery_query[0].anzahl.split("|")[index])
                            quantity += quantity_to_add

    try: rechnung_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
    
    print("identification: " + str(identification) + " -> date: " + str(date) + " -> quantity: " + str(quantity))
    return quantity

def get_article_categories(): # return List of Dicts
    print("get_article_categories()")

    try: categorie_db.connect()
    except: True

    categories = []
    for cat in Categorie.select():
        categories.append(model_to_dict(cat))

    try: categorie_db.close()
    except: True

    return categories

def get_barcode_helper ( barcode ): # return List + Name of Articles (<ID 100)
    print("get_barcode_helper(" + str(barcode) + ")")

    answer = []
    barcode = str(barcode).upper()

    try: stock_db.connect()
    except: True

    if not barcode == "":
        query = Artikel.select().where(Artikel.artikel == "BARCODEHELPER")
        if query.exists():
            for each in query:
                identification = str(each.identification).rstrip().upper()
                name = each.name_de.rstrip()
        
                if barcode in identification:
                    answer.append(identification + ": " + name)

    try: stock_db.close()
    except: True

    answer = "\n".join(answer)
    return answer


def get_stats_delivery(date_start, date_end):
    print("get_stats_delivery(" + str(date_start) + ", " + str(date_end) + ")")

    try: lieferschein_db.connect()
    except: True
    
    dict_of_dates = {}    
    for lieferschein in Lieferschein.select():
        if not lieferschein.datum > date_end and not lieferschein.datum < date_start:
            if lieferschein.datum in dict_of_dates:
                dict_of_dates[lieferschein.datum] = dict_of_dates[lieferschein.datum] + 1
            else:
                dict_of_dates[lieferschein.datum] = 1
    
    answer_list = libs.ServerTools.create_list_for_stats(dict_of_dates)               
   
    try: lieferschein_db.close()
    except: True
    
    return answer_list

def get_stats_clients(date_start, date_end):
    print("get_stats_clients(" + str(date_start) + ", " + str(date_end) + ")")

    try: lieferschein_db.connect()
    except: True
    
    dict_of_dates = {}
    dict_of_clients = {}
    
    for lieferschein in Lieferschein.select():
        if not lieferschein.datum > date_end and not lieferschein.datum < date_start:
        
            if not lieferschein.datum in dict_of_clients:
                dict_of_clients[lieferschein.datum] = []
        
            if not lieferschein.kunde_id in dict_of_clients[lieferschein.datum] or str(lieferschein.kunde_id) == "0":
                
                if lieferschein.datum in dict_of_dates:
                    dict_of_dates[lieferschein.datum] = dict_of_dates[lieferschein.datum] + 1
                else:
                    dict_of_dates[lieferschein.datum] = 1
                    
                if not str(lieferschein.kunde_id) == "0":
                    dict_of_clients[lieferschein.datum].append(lieferschein.kunde_id)
       
    answer_list = libs.ServerTools.create_list_for_stats(dict_of_dates)   
    
    try: lieferschein_db.close()
    except: True
    
    return answer_list

def get_stats_per_hour(date): # return dict per hour and date
    try: log_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True

    answer = {}
    delivery_checked = []
    
    query_logs = Log.select().where(Log.function.contains("create_rechnung_vorlage")).where(Log.date.contains(date))
    if query_logs.exists():
        for log in query_logs:
            if "lieferschein_id" in log.function:
                delivery_id = log.function.split("lieferschein_id': '")[-1].split("'")[0]            
            else:
                delivery_id = log.function.split("(")[1].split(")")[0]

            if not delivery_id in delivery_checked:
                #print("delivery_id: " + delivery_id)
                
                tmp = datetime.datetime.fromtimestamp(float(log.timestamp))
                #zeit = str(tmp.strftime("%Y-%m-%d %H:%M"))
                datum = str(tmp.strftime("%Y-%m-%d"))
                stunde = str(tmp.strftime("%H"))
                
                query_delivery = Lieferschein.select().where(Lieferschein.identification == delivery_id)
                if query_delivery.exists():
                    total = query_delivery[0].total
                    total = round(total, 2)
                else:
                    total = 0.0
                
                if not datum in answer: answer[datum] = {}
                
                if stunde in answer[datum]:
                    answer[datum][stunde] = answer[datum][stunde] + total
                else:
                    answer[datum][stunde] = total
                
                delivery_checked.append(delivery_id)

    try: log_db.close()
    except: True
    try: lieferschein_db.close()
    except: True

    return answer

def get_stats_umsatz_date_list(date_list):
    print("get_stats_umsatz_date_list(" + str(date_list) + ")")

    try: rechnung_db.connect()
    except: True

    list_of_dates = []
    for date in date_list:
        print("check date umsatz " + str(date))
        total_of_date = 0.0
        date = str(date)
        if "W" in date:
            search_week_first_day = libs.BlueFunc.date_from_weeknumber(date)
            for x in range(0, 8):
                search_date = libs.BlueFunc.DateAddDays(search_week_first_day, x)
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum == search_date) + Kasse.select().where(Kasse.datum == search_date)
                for rechnung in liste_der_rechnungen:
                    total_of_date = round(total_of_date + rechnung.total_htva, 2)
        elif "Q" in date:
            search_year = date.split("-")[0]
            search_quartal = date.split("Q")[1].split("-")[0]
            if search_quartal == "1":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-01"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-01"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-02"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-02"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-03"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-03"))
            elif search_quartal == "2":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-04"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-04"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-05"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-05"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-06"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-06"))
            elif search_quartal == "3":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-07"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-07"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-08"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-08"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-09"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-09"))    
            elif search_quartal == "4":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-10"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-10"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-11"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-11"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-12"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-12"))

            for rechnung in liste_der_rechnungen:
                total_of_date = round(total_of_date + rechnung.total_htva, 2)       
        else:
            liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(date)) + Kasse.select().where(Kasse.datum.contains(date))
            for rechnung in liste_der_rechnungen:
                total_of_date = round(total_of_date + rechnung.total_htva, 2)
        list_of_dates.append(total_of_date)

    try: rechnung_db.close()
    except: True

    return list_of_dates

def get_stats_stock_date_list(DATA):
    print("get_stats_stock_date_list(" + str(DATA) + ")")

    try: stock_db.connect()
    except: True
    
    date_list = DATA["date_list"]
    
    if not "article" in DATA: articles = Artikel.select()
            
    list_of_dates = []
    for date in date_list:
        print("check date stock " + str(date))
        total_of_date = 0.0
        date = str(date)
           
        if "W" in date:
            search_week_first_day = libs.BlueFunc.date_from_weeknumber(date)
            datum = libs.BlueFunc.DateAddDays(search_week_first_day, 6)
        elif "Q" in date:
            search_year = date.split("-")[0]
            search_quartal = date.split("Q")[1].split("-")[0]
            
            if search_quartal == "1":
                datum = search_year + "-03-32"
            elif search_quartal == "2":
                datum = search_year + "-06-32"
            elif search_quartal == "3":
                datum = search_year + "-09-32"   
            elif search_quartal == "4":
                datum = search_year + "-12-32"
        else:
            datum = date

        data = {} 
        if "article" in DATA:
            moves = Bewegung.select().where(Bewegung.datum <= datum).where(Bewegung.bcode == str(DATA["article"])).order_by(Bewegung.identification)
            for move in moves:
                data[str(move.bcode)] = {"anzahl": float(move.end)}
            for identification in data:
                anzahl = data[identification]["anzahl"]
                total_of_date = round(total_of_date + anzahl, 2)  
        else:
            #articles = Artikel.select()
            for article in articles:
                #print("article: " + str(article))
                #moves = Bewegung.select().where(Bewegung.datum <= datum).where(Bewegung.bcode == str(article.identification)).order_by(Bewegung.identification)
                
                moves = Bewegung.select().where(Bewegung.bcode == str(article.identification))
                moves = moves.where(Bewegung.datum <= datum)
                
                #moves = moves.where(Bewegung.datum <= datum)#.order_by(Bewegung.identification)
                
                if moves.exists():
                    wert = round(float(moves[-1].end) * float(moves[-1].preisek), 2)
                    total_of_date = round(total_of_date + wert, 2)
                
                # mysql_db.execute_sql('UPDATE categorie SET identification = 1 WHERE identification = ' + str(new_cat.identification) + ';')
                #SELECT start * preisek AS total_wert
                #FROM bewegung
                #WHERE datum < "2024-01";
                
                # SELECT SUM(start * preisek) AS total_wert
                #FROM (
                #    SELECT DISTINCT bcode, start, preisek
                #    FROM orders
                #) AS distinct_orders
                #WHERE datum < "2024-01";
                
            #moves = Bewegung.select().where(Bewegung.datum <= datum).order_by(Bewegung.identification)
            #for move in moves:
            #    data[str(move.bcode)] = {"anzahl": float(move.end), "preis": float(move.preisek)}
            #for identification in data:
            #    wert = round(data[identification]["anzahl"] * data[identification]["preis"], 2)
            #    total_of_date = round(total_of_date + wert, 2)
                
        list_of_dates.append(total_of_date)

    try: stock_db.close()
    except: True

    return list_of_dates

def get_stats_sales_date_list(DATA):
    print("get_stats_sales_date_list(" + str(DATA) + ")")

    try: rechnung_db.connect()
    except: True

    date_list = DATA["date_list"]
    
    list_of_dates = []
    for date in date_list:
        print("check date sales " + str(date))
        total_of_date = 0.0
        date = str(date)
        if "W" in date:
            search_week_first_day = libs.BlueFunc.date_from_weeknumber(date)
            for x in range(0, 8):
                search_date = libs.BlueFunc.DateAddDays(search_week_first_day, x)
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum == search_date) + Kasse.select().where(Kasse.datum == search_date)
                for rechnung in liste_der_rechnungen:
                    total_of_date += 1
        elif "Q" in date:
            search_year = date.split("-")[0]
            search_quartal = date.split("Q")[1].split("-")[0]
            if search_quartal == "1":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-01"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-01"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-02"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-02"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-03"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-03"))
            elif search_quartal == "2":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-04"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-04"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-05"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-05"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-06"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-06"))
            elif search_quartal == "3":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-07"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-07"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-08"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-08"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-09"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-09"))    
            elif search_quartal == "4":
                liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(search_year + "-10"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-10"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-11"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-11"))
                liste_der_rechnungen += Rechnung.select().where(Rechnung.datum.contains(search_year + "-12"))
                liste_der_rechnungen += Kasse.select().where(Kasse.datum.contains(search_year + "-12"))

            for rechnung in liste_der_rechnungen:
                total_of_date += 1 
        else:
            liste_der_rechnungen = Rechnung.select().where(Rechnung.datum.contains(date)) + Kasse.select().where(Kasse.datum.contains(date))
            for rechnung in liste_der_rechnungen:
                total_of_date += 1
        list_of_dates.append(total_of_date)

    try: rechnung_db.close()
    except: True

    return list_of_dates

def get_stats_umsatz(date_start, date_end):
    print("get_stats_umsatz(" + str(date_start) + ", " + str(date_end) + ")")

    try: rechnung_db.connect()
    except: True
    
    dict_of_dates = {}       
    liste_der_rechnungen = Rechnung.select() + Kasse.select()
    for rechnung in liste_der_rechnungen:
        if not rechnung.datum > date_end and not rechnung.datum < date_start:
            if rechnung.datum in dict_of_dates:
                dict_of_dates[rechnung.datum] = round(dict_of_dates[rechnung.datum] + rechnung.total_htva, 2)
            else:
                dict_of_dates[rechnung.datum] = round(rechnung.total_htva, 2)
       
    answer_list = libs.ServerTools.create_list_for_stats(dict_of_dates)   
    
    try: rechnung_db.close()
    except: True
    
    return answer_list
    
def get_directory_content(path):
    list_of_files = ["test.html"]
    if not os.path.exists("/etc/zk-data-server/web/documents"):
        os.system("mkdir " + "/etc/zk-data-server/web/documents && chmod 777 -R /etc/zk-data-server/web/documents")
    
    if str(path) == "None": path = ""
    path = "/etc/zk-data-server/web/documents/" + path.rstrip()
    path = path.replace("//", "/")
    
    if not os.path.exists(path): path = "/etc/zk-data-server/web/documents/"
    old_path = "/".join(path.split("/")[:-1])
    print("old_path: " + str(old_path))

    list_of_files = os.listdir(path)
    print("list_of_files: " + str(list_of_files))
    
    return [path.replace("/etc/zk-data-server/web/documents/", ""), old_path.replace("/etc/zk-data-server/web/documents/", ""), list_of_files]
    
def get_stats_gewinn(jahr):# 
    print("get_stats_gewinn(" + str(jahr) + ")")
    try: rechnung_db.connect()
    except: True
    try: rechnung_e_db.connect()
    except: True
    try: stock_db.connect()
    except: True
    try: lieferschein_db.connect()
    except: True
    try: stats_db.connect()
    except: True
    
    list_gewinn = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    list_ausgaben = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    list_umsatz = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    list_stock = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    list_stock_diff = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    
    # Umsatz rechnungen
    query = Rechnung.select().where(Rechnung.datum.contains(jahr))
    if query.exists():
        for rechnung in query:            
            month = int( str(rechnung.datum).split("-")[1] ) - 1            
            list_umsatz[month] = round(list_umsatz[month] + float(rechnung.total_htva), 2)            
            
    # Umsatz kassenverkaufe
    query = Kasse.select().where(Kasse.datum.contains(jahr))
    if query.exists():
        for rechnung in query:            
            month = int( str(rechnung.datum).split("-")[1] ) - 1            
            list_umsatz[month] = round(list_umsatz[month] + float(rechnung.total_htva), 2)  
          
    # Ausgaben    
    query = Rechnung_e.select().where(Rechnung_e.datum.contains(jahr))
    if query.exists():
        for rechnung in query:            
            month = int( str(rechnung.datum).split("-")[1] ) - 1
            a_summe = round(rechnung.ware + rechnung.unkosten + rechnung.investition, 2)            
            list_ausgaben[month] = round(list_ausgaben[month] - a_summe, 2)              
       
    # get last stock value of year before
    total_stock_year_before = 0.0            
    query_last_date_year_before = Stats_Stock.select().where(Stats_Stock.date.contains(str(int(jahr)-1) + "-12-")).order_by(-Stats_Stock.date).limit(1)
    if query_last_date_year_before.exists():
        last_date_year_before = str(query_last_date_year_before[0].date)
        print("last_date_year_before: " + str(last_date_year_before))    
        query = Stats_Stock.select().where(Stats_Stock.date == last_date_year_before)
        for obj in query:
            total_stock_year_before = total_stock_year_before + obj.wert
    total_stock_year_before = round(total_stock_year_before, 2)
    print("total_stock_year_before: " + str(total_stock_year_before))
        
    for month_index in range(0, 12): 
        print("month_index: " + str(month_index))
        month = month_index + 1
        print("month: " + str(month))
        
        list_gewinn[month_index] = round(list_ausgaben[month_index] + list_umsatz[month_index], 2)
    
        if len(str(month)) == 1: mount_double = "0" + str(month)
        else: mount_double = month
        
        query_last_date = Stats_Stock.select().where(Stats_Stock.date.contains(str(jahr) + "-" + str(mount_double) + "-")).order_by(-Stats_Stock.date).limit(1)
        
        if query_last_date.exists():
            last_date = str(query_last_date[0].date)
            print("last_date: " + str(last_date))
           
            total_stock = 0.0
            
            query = Stats_Stock.select().where(Stats_Stock.date == last_date)
            for obj in query:
                total_stock = total_stock + obj.wert
                
            list_stock[month_index] = round(total_stock, 2)
           
            if month_index == 0:
                list_stock_diff[month_index] = list_stock[month_index] - total_stock_year_before
            else:
                list_stock_diff[month_index] = list_stock[month_index] - list_stock[month_index-1]
            list_stock_diff[month_index] = round(list_stock_diff[month_index], 2)
       
    try: rechnung_db.close()
    except: True
    try: rechnung_e_db.close()
    except: True
    try: stock_db.close()
    except: True
    try: stats_db.close()
    except: True
    try: lieferschein_db.close()
    except: True
        
    
    return {"gewinn": list_gewinn, "ausgaben": list_ausgaben, "umsatz": list_umsatz, "stock": list_stock, "stock_diff": list_stock_diff}

def get_stats_categorieprofit(date):
    print("get_stats_categorieprofit(" + str(date) + ")")
    
    categorieprofit = {}
    date = str(date)
    
    try: stats_db.connect()
    except: True
    
    query = Stats_CategorieProfit.select().where(Stats_CategorieProfit.date.contains(date))
    for entry in query:
        cat = int(entry.categorie)
        wert = round(entry.wert, 2)
        if cat in categorieprofit:
            categorieprofit[cat] = round(categorieprofit[cat] + wert, 2)
        else:
            categorieprofit[cat] = wert
    
    stats_db.close() 
    
    return categorieprofit
    
    
########################
## Create First User

try: user_db.connect()
except: True
query = User.select()
if not query.exists():
    username_t = str(libs.BlueFunc.Timestamp())[0:6]
    pin_t = str(libs.BlueFunc.Timestamp())[0:6]
    secure_key_t = str(libs.BlueFunc.Timestamp())[0:10]
    admin = True
    create_user(username_t, pin_t, secure_key_t, admin)

try: user_db.close()
except: True
########################


########################


### ### ### ### ### ### ### ### ###    
# Use ps2pdf on RE invoices                           

try: stock_db.connect()
except: True
print("set next free ID for Article")      
try: FreeID = int(FreeID)
except: FreeID = 0
while True:
    query = Artikel.select().where(Artikel.identification == int(FreeID))
    if not query.exists(): break
    else: 
        query2 = Artikel.select().where(Artikel.identification == int(FreeID + 100))
        if query2.exists():
            FreeID = FreeID + 101
        else:
            FreeID = FreeID + 1
print("FreeID: " + str(FreeID))
libs.BlueFunc.BlueSave("FreeID", int(FreeID), data_dir)

### ### ### ### ### ### ### ### ###
# Create Client 0
### ### ### ### ### ### ### ### ###
try: kunde_db.connect()
except: True
query = Kunde.select().where(Kunde.identification == "0")
if not query.exists():
    create_client_with_identification(0)
    set_client({"identification": 0, "name": "BARKUNDE"})
   
### write Stats
print("write Stats")
try: stock_db.connect()
except: True
try: stats_db.connect()
except: True
try: categorie_db.connect()
except: True
try: lieferschein_db.connect()
except: True
try: rechnung_db.connect()
except: True

datum = Date()
datum_before = DateAddDays(datum, -1)
#print("datum: " + str(datum))

print("write Stats: Stats_Stock by Categorie")
for cat in Categorie.select():
    #print("cat: " + str(cat.identification))
    
    query = Stats_Stock.select().where(Stats_Stock.date == datum).where(Stats_Stock.categorie == str(cat.identification))
    if not query.exists():
        #print(datum + " " + str(cat.identification))
        wert = 0.0
        for art in Artikel.select().where(Artikel.categorie == str(cat.identification)):
            #print("art: " + str(art.identification))
            #print("old wert: " + str(wert))
        
            wert = wert + (art.preisek * art.anzahl)
            #print("new wert: " + str(wert))
        
        if not wert == 0.0:
            #print("write")
            new_stock_stat = Stats_Stock.create(date = datum, categorie = cat.identification, wert = round(wert, 2))


liste_of_article_is_work = []
for art in Artikel.select().where(Artikel.categorie == 1):
    liste_of_article_is_work.append(art.identification)

liste_of_work_profit_at_start = []
for entry in Stats_WorkProfit.select():
    if not entry.date in liste_of_work_profit_at_start:
        liste_of_work_profit_at_start.append(entry.date)

liste_of_categorie_profit_at_start = []
for entry in Stats_CategorieProfit.select():
    if not entry.date in liste_of_categorie_profit_at_start:
        liste_of_categorie_profit_at_start.append(entry.date)

print("write Stats: Stats_WorkProfit and Stats_CategorieProfit")  
liste_der_rechnungen = Rechnung.select() + Kasse.select()
for rechnung in liste_der_rechnungen:
    if not rechnung.datum == datum:
        if not rechnung.datum in liste_of_work_profit_at_start:
            print("Write Stats (WorkProfit) for " + str(rechnung.identification))
            for lieferschein_id in rechnung.lieferscheine.split("|"):
                print("lieferschein_id: " + lieferschein_id)
                query_lieferschein = Lieferschein.select().where(Lieferschein.identification == lieferschein_id)  
                if query_lieferschein.exists():
                    lieferschein = query_lieferschein[0]          
                    index = 0
                    for bcode in lieferschein.bcode.split("|"):
                        print("bcode: " + str(bcode))
                        if not bcode == "":
                            if bcode in liste_of_article_is_work:
                                wert_einheit = float(lieferschein.preis_htva.split("|")[index])
                                anzahl = float(lieferschein.anzahl.split("|")[index])
                                wert = round(wert_einheit * anzahl, 2)
                            else:
                                wert = 0.0
                
                            if True:#if not wert == 0.0:
                                query = Stats_WorkProfit.select().where(Stats_WorkProfit.date == rechnung.datum).where(Stats_WorkProfit.article == bcode)
                                if query.exists():
                                    query[0].wert = round(query[0].wert + wert, 2)
                                    query[0].save()
                                    print("update")
                                else:
                                    print("new entry")
                                    new_entry = Stats_WorkProfit.create(date = rechnung.datum, article = bcode, wert = wert)
                                    print("L" + str(lieferschein_id) + " " + rechnung.datum)
                        index = index + 1
        
        if not rechnung.datum in liste_of_categorie_profit_at_start:
            print("Write Stats (Categorie Profit) for " + str(rechnung.identification))
            for lieferschein_id in rechnung.lieferscheine.split("|"):
                print("lieferschein_id: " + lieferschein_id)
                query_lieferschein = Lieferschein.select().where(Lieferschein.identification == lieferschein_id)  
                if query_lieferschein.exists():
                    lieferschein = query_lieferschein[0]              
                    index = 0
                    for bcode in lieferschein.bcode.split("|"):
                        if not bcode == "":
                            if not bcode in liste_of_article_is_work:
                                wert_einheit = float(lieferschein.preis_htva.split("|")[index])
                                anzahl = float(lieferschein.anzahl.split("|")[index])                                               
                                query_article = Artikel.select().where(Artikel.identification == bcode)
                                if query_article.exists():
                                    categorie = query_article[0].categorie
                                    #preisek = query_article[0].preisek
                                    
                                    if categorie == "": categorie = 0
                                else:
                                    categorie = 0  
                                    #preisek = 0.0
                                
                                #wert = round((wert_einheit - preisek) * anzahl, 2)
                                wert = round(wert_einheit * anzahl, 2)
                            else:
                                wert = 0.0
                            
                            if True:#if not wert == 0.0:
                                query = Stats_CategorieProfit.select().where(Stats_CategorieProfit.date == rechnung.datum).where(Stats_CategorieProfit.categorie == categorie)
                                if query.exists():
                                    query[0].wert = round(query[0].wert + wert, 2)
                                    query[0].save()
                                else:
                                    new_entry = Stats_CategorieProfit.create(date = rechnung.datum, categorie = categorie, wert = wert)        
                                    print("L" + str(lieferschein_id) + " " + rechnung.datum)                    
                        index = index + 1


try: stock_db.close()
except: True
try: stats_db.close()
except: True
try: categorie_db.close()
except: True
try: lieferschein_db.close()
except: True
try: rechnung_db.close()
except: True

#######################################

remove_unused_deliverys()
#remove_unused_articles()

def run_mode(DATA):
    print("run_mode(" + str(DATA) + ")")

    if debug: print("DATA: " + str(DATA))

    if True:
        if True:
            if "secure_key" in DATA:
                mode = DATA["mode"]
                user_secure_key = DATA["secure_key"]                    
            else:
                print("is not encrypted and no secure_key in DATA")
                DATA = ""
                user_secure_key = "x"
            #else:
            #    DATA = json.loads(DATA)

            if not "mode" in DATA:
                mode = "NONE"
                userdata = {}
                Antwort = ""
            else:
                mode = DATA["mode"]
                print("mode: " + str(mode))
                del DATA["mode"]

                userdata = getUserData_from_key(user_secure_key)
                Antwort = ""

            if not userdata == {}:
                username = userdata["name"] #get_username_from_key(user_secure_key)
                print("username: " + str(username))
                
                DATA["pcname"] = username
                DATA["user"] = username
                
                ########   
                # DELIVERY_E
                if userdata["permission_delivery_e"] > 0: # user has access to delivery

                    if mode == "get_delivery_e":#return Dict of Delivery_e
                        Antwort = get_delivery_e(DATA["identification"])
                        
                    if mode == "get_delivery_e_pdf":#return path of Delivery_e pdf
                        Antwort = get_delivery_e_pdf(DATA["identification"])
                        
                    if mode == "create_delivery_e":#return Dict of Delivery_e
                        Antwort = create_delivery_e()
                        
                    if mode == "search_delivery_e":#return List of dict Delivery_e
                        Antwort = search_delivery_e(DATA)
                        
                    if mode == "set_delivery_e":#return Dict of Delivery_e
                        Antwort = set_delivery_e(DATA)
                        
                        
                        
                # DELIVERY
                if userdata["permission_delivery"] > 0: # user has access to delivery
                
                    if mode == "get_delivery_not_invoiced":#return Dict of Delivery
                        Antwort = get_delivery_not_invoiced(DATA["index"])
                        
                    if mode == "get_delivery":#return Dict
                        if "log" in DATA:
                            if bool(DATA["log"]):
                                set_log("delivery", "get_delivery_with_log()", str(DATA["identification"]), user_secure_key)
                        Antwort = get_delivery(DATA["identification"])

                    if mode == "get_delivery_user":#return Dict
                        Antwort = get_delivery_user(DATA["identification"])
                        
                    if mode == "search_delivery":#return List of IDs
                        Antwort = search_delivery(DATA)
                        
                    if mode == "search_delivery_by_changed_date_and_total":
                        Antwort = search_delivery_by_changed_date_and_total(DATA["datum"], DATA["total"])
                        
                    if mode == "get_delivery_pdf":#return answer path
                        Antwort = get_delivery_pdf(DATA["identification"])

                    if mode == "get_deliverys_file":
                        Antwort = get_deliverys_file(DATA["year"])
              
                
                    if userdata["permission_delivery"] == 2: # user has write access to delivery
        
                        if mode == "set_delivery_info":#return bool(sucess)
                            set_log("delivery", "set_delivery_info(" + str(DATA["info"]) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_info(DATA["identification"], DATA["info"])

                        if mode == "set_delivery_date_todo":#return bool(sucess)
                            set_log("delivery", "set_delivery_date_todo(" + str(DATA["date"]) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_date_todo(DATA["identification"], DATA["date"])

                        if mode == "set_delivery_client":#return bool(sucess)
                            set_log("delivery", "set_delivery_client(" + str(DATA["kunde_id"]) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_client(DATA["identification"], DATA["kunde_id"])

                        if mode == "set_delivery_engine":#return bool(sucess)
                            set_log("delivery", "set_delivery_engine(" + str(DATA["maschine"]) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_engine(DATA["identification"], DATA["maschine"])
                            
                        if mode == "add_delivery_article":#return bool(sucess)
                            set_log("delivery", "add_delivery_article(" + str(DATA) + ")", str(DATA["identification"]), username)
                            Antwort = add_delivery_article(DATA["identification"], DATA["bcode"], username)

                        if mode == "set_delivery_line":#return bool(sucess)
                            set_log("delivery", "set_delivery_line(" + str(DATA) + ")", str(DATA["identification"]), username)
                            Antwort = set_delivery_line(DATA["identification"], DATA["index"], DATA["anzahl"], DATA["barcode"], DATA["name"], DATA["preis"], DATA["tva"], username)

                        if mode == "set_delivery_finish":#return bool(sucess)
                            set_log("delivery", "set_delivery_finish(" + str(DATA["fertig"]) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_finish(DATA["identification"], DATA["fertig"])

                        if mode == "set_delivery_line_pos":#return bool(sucess)
                            #Antwort = set_delivery_line_pos(DATA["identification"], DATA["old_pos"], DATA["new_pos"])
                            Antwort = True # disabled

                        if mode == "set_delivery_user": #return bool(sucess)
                            set_log("delivery", "set_delivery_user(" + str(DATA) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = set_delivery_user(DATA["identification"], DATA["add_user"], DATA["state"])

                        if mode == "create_delivery_with_identification":#return Dict
                            set_log("delivery", "create_delivery_with_identification(" + str(DATA) + ")", "", user_secure_key)
                            Antwort = create_delivery_with_identification(DATA["identification"], username)
                            
                        if mode == "create_delivery" or mode == "NeuerLieferschein":#return Dict
                            set_log("delivery", "create_delivery", "", user_secure_key)
                            Antwort = create_delivery(username)
                            
                        if mode == "create_delivery_angebot":#return Dict
                            set_log("delivery", "create_delivery_angebot", "", user_secure_key)
                            Antwort = create_delivery_angebot(username)
                            
                        if mode == "create_delivery_bestellung":#return Dict
                            set_log("delivery", "create_delivery_bestellung", "", user_secure_key)
                            Antwort = create_delivery_bestellung(username)

                        if mode == "delete_delivery_line":#return bool(sucess)
                            set_log("delivery", "delete_delivery_line(" + str(DATA) + ")", str(DATA["identification"]), user_secure_key)
                            Antwort = delete_delivery_line(DATA["identification"], DATA["index"], username)

                    
                ########   
                # INVOICE
                if userdata["permission_invoice"] > 0: # user has access to invoice
        
                    if mode == "get_invoices_file":
                        Antwort = get_invoices_file(DATA["year"])
                        
                    if mode == "search_invoice" or mode == "search_rechnung":
                        Antwort = search_invoice(DATA["identification"], DATA["kunde"], DATA["rest"], DATA["datum"], DATA["index"])

                    if mode == "get_invoice" or mode == "get_rechnung":
                        Antwort = get_invoice(DATA["identification"])
                        
                    if mode == "get_invoice_pdf":#return answer path
                        Antwort = get_invoice_pdf(DATA["identification"])

                    if userdata["permission_invoice"] == 2: # user has write access to invoice
                        
                        if mode == "delete_invoice_pdf":
                            Antwort = delete_invoice_pdf(DATA["identification"])   
                        
                        if mode == "create_rechnung" or mode == "create_invoice":
                            set_log("invoice", "create_invoice(" + str(DATA) + ")", "", user_secure_key)
                            Antwort = create_invoice(DATA["rechnung_mode"], DATA["vorlage_identification"], DATA["user_pin"])

                        if mode == "add_lieferschein_to_vorlage":
                            set_log("invoice", "add_lieferschein_to_vorlage(" + str(DATA["lieferschein_id"]) + ")", str(DATA["rechnung_id"]), user_secure_key)
                            set_log("delivery", "add_lieferschein_to_vorlage(" + str(DATA) + ")", str(DATA["lieferschein_id"]), user_secure_key)
                            Antwort = add_lieferschein_to_vorlage(DATA["lieferschein_id"], DATA["rechnung_id"])

                        if mode == "remove_lieferschein_from_vorlage":
                            set_log("invoice", "remove_lieferschein_from_vorlage(" + str(DATA["lieferschein_id"]) + ")", str(DATA["rechnung_id"]), user_secure_key)
                            set_log("delivery", "remove_lieferschein_from_vorlage(" + str(DATA) + ")", str(DATA["lieferschein_id"]), user_secure_key)
                            Antwort = remove_lieferschein_from_vorlage(DATA["lieferschein_id"], DATA["rechnung_id"])

                        if mode == "create_rechnung_vorlage":
                            set_log("invoice", "create_rechnung_vorlage(" + str(DATA["lieferschein_id"]) + ")", "", user_secure_key)
                            set_log("delivery", "create_rechnung_vorlage(" + str(DATA) + ")", str(DATA["lieferschein_id"]), user_secure_key)
                            Antwort = create_rechnung_vorlage(DATA["lieferschein_id"])
                            
                        if mode == "set_invoice_rappel" or mode == "set_rechnung_rappel": 
                            set_log("invoice", "set_invoice_rappel", str(DATA["identification"]), user_secure_key)
                            Antwort = set_invoice_rappel(DATA["identification"])
                
                        if userdata["admin"]:
                            if mode == "set_invoice_date_todo" or mode == "set_rechnung_date_todo":
                                set_log("invoice", "set_invoice_date_todo", str(DATA["identification"]), user_secure_key)
                                Antwort = set_invoice_date_todo(DATA["identification"], DATA["date"])
                
                ########
                # INVOICE_E
                if userdata["permission_invoice_e"] > 0: # user has access to invoice_e
                
                    if mode == "search_invoice_e" or mode == "search_rechnung_e":
                        Antwort = search_invoice_e(DATA["identification"], DATA["dokument_id"], DATA["lieferant"], DATA["rest"], DATA["datum"], DATA["index"])
                        
                    if mode == "get_invoice_e" or mode == "get_rechnung_e":
                        Antwort = get_invoice_e(DATA["identification"])
                        
                    if mode == "get_invoice_e_pdf":
                        Antwort = get_invoice_e_pdf(DATA["identification"])               
                 
                    if userdata["permission_invoice_e"] == 2: # user has write access to invoice_e

                        if mode == "set_invoice_e" or mode == "set_rechnung_e":
                            set_log("invoice_e", "set_invoice_e(" + str(DATA) + ")", "", user_secure_key)
                            Antwort = set_invoice_e(DATA)
                            
                        if mode == "create_invoice_e" or mode == "create_rechnung_e":#create rechnung_e                
                            set_log("invoice_e", "create_invoice_e(" + str(DATA) + ")", "", user_secure_key)
                            Antwort = create_invoice_e(DATA["lieferant"], DATA["dokument_datum"], DATA["doc_id"], DATA["total"], DATA["pdf_link"], user_secure_key)
                
                ########   
                # ARTICLE
                if userdata["permission_article"] > 0: # user has access to article
                
                    if mode == "get_marchants":
                        Antwort = get_marchants() 
                        
                    if mode == "get_article_categories":
                        Antwort = get_article_categories() 

                    if mode == "get_article_average": # return average per year (365days)
                        Antwort = get_article_average(DATA["identification"])

                    if mode == "get_next_changed_article": #
                        Antwort = get_next_changed_article(DATA["timestamp"])                        
                        
                    if mode == "GetArt" or mode == "get_article":#return Dict
                        Antwort = get_article(DATA)
                        
                    if mode == "get_article_move":# return next move of article
                        Antwort = get_article_move ( DATA["identification"] , DATA["bcode"] )
                        
                    if mode == "GetBewegungIndex":# return Int of aktual bewegung
                        print("GetBewegungIndex")
                        Antwort = int(BeID)

                    if mode == "get_articles_file":
                        Antwort = get_articles_file()
                        
                    if mode == "search_article" or mode == "SearchArt":#return List of IDs
                        Antwort = search_article(DATA)
                        
                    if mode == "search_article_sorted":#return List Categorie with list of IDs
                        Antwort = search_article_sorted(DATA)
                       
                    if mode == "print_article_stock":
                        Antwort = print_article_stock(DATA["datum"])
                        
                    if userdata["permission_article"] == 2: # user has write access to article
                    
                        if mode == "add_article" or mode == "AddArt":#return Dict
                            set_log("article", "add_article", str(DATA), user_secure_key)
                            DATA["user"] = get_username_from_key(user_secure_key)
                            Antwort = add_article(DATA)

                        if mode == "create_article" or mode == "GetID":#return Dict
                            if "identification" in DATA:
                                set_log("article", "create_article", str(DATA["identification"]), user_secure_key)
                                Antwort = create_article(DATA["identification"])
                            else:
                                set_log("article", "create_article", "", user_secure_key)
                                Antwort = create_article(FreeID)

                        if mode == "set_article" or mode == "SetArt":#return Bool of sucess   
                            set_log("article", "set_article(" + str(DATA) + ")", "", user_secure_key)
                            DATA["user"] = get_username_from_key(user_secure_key)
                            Antwort = set_article(DATA)
                   
                ########   
                # CLIENT
                if userdata["permission_client"] > 0: # user has access to client
                
                    if mode == "get_next_changed_client": #
                        Antwort = get_next_changed_client(DATA["timestamp"])
                        
                    if mode == "get_best_clients":#return best clients
                        Antwort = get_best_clients(DATA["max_quantity"], DATA["days"])

                    if mode == "SearchKunden" or mode == "search_client":#return List of IDs
                        Antwort = search_client(DATA)

                    if mode == "get_client_total":#return total sum of deliverys
                        Antwort = get_client_total(DATA["identification"])

                    if mode == "get_client" or mode == "GetKunden":#return Dict
                        Antwort = get_client(DATA["identification"])
                        
                    if mode == "get_clients_file":
                        Antwort = get_clients_file()
                
                    if userdata["permission_client"] == 2: # user has write access to client
                    
                        if mode == "set_client" or mode == "SetKunde":#return Bool of sucess
                            if "identification" in DATA:
                                set_log("client", "set_client(" + str(DATA) + ")", str(DATA["identification"]), user_secure_key)
                                Antwort = set_client(DATA)
                            
                        if mode == "create_client_with_identification":
                            set_log("client", "create_client_with_identification", str(DATA["identification"]), user_secure_key)
                            Antwort = create_client_with_identification(DATA["identification"])

                        if mode == "create_client":#return Dict
                            set_log("client", "create_client", "", user_secure_key)
                            Antwort = create_client() 
                           
                ########   
                # Inventory
                if userdata["permission_inventory"] > 0: # user has access to inventory
                
                    if mode == "getInventar" or mode == "get_inventar": # return Next Inventar Line
                        Antwort = get_inventar(DATA)

                    if mode == "print_inventar":
                        Antwort = print_inventar(DATA["start_jahr"], DATA["start_monat"], DATA["start_tag"], DATA["end_jahr"], DATA["end_monat"], DATA["end_tag"], DATA["anwenden"])
                    
                    if userdata["permission_inventory"] == 2: # user has write access to inventory

                        if mode == "add_inventar" or mode == "addInventar": # return Bool of sucess
                            Antwort = add_inventar(DATA)

                ########   
                # Stats
                if userdata["permission_stats"] > 0: # user has access to stats
                
                    if mode == "get_article_sell":#return float
                        Antwort = get_article_sell(DATA["identification"], DATA["date"])
                        
                    if mode == "get_stats_gewinn":#return Bool of sucess
                        Antwort = get_stats_gewinn(DATA["jahr"])
                        
                    if mode == "get_stats_per_hour":#return Dict of Dict
                        Antwort = get_stats_per_hour(DATA["date"])
                        
                    if mode == "get_stats_umsatz": #return List of List
                        Antwort = get_stats_umsatz(DATA["date_start"], DATA["date_end"])
                        
                    if mode == "get_stats_umsatz_date_list": #return List of List
                        Antwort = get_stats_umsatz_date_list(DATA["date_list"])
                        
                    if mode == "get_stats_sales_date_list": #return List of List
                        Antwort = get_stats_sales_date_list(DATA)
                        
                    if mode == "get_stats_stock_date_list": #return List of List
                        Antwort = get_stats_stock_date_list(DATA) # TODO Take too long if there are a lot of moves...
                        
                    if mode == "get_stats_delivery":#return List of List
                        Antwort = get_stats_delivery(DATA["date_start"], DATA["date_end"])
                        
                    if mode == "get_stats_clients":#return List of List
                        Antwort = get_stats_clients(DATA["date_start"], DATA["date_end"])
                        
                    if mode == "get_stats_categorieprofit":#return Dict
                        Antwort = get_stats_categorieprofit(DATA["date"])
                
                
                ########
                # Cashbook
                if userdata["permission_cashbook"] > 0: # user has access to cashbook
                                    
                    if mode == "get_cashbook_pdf":
                        Antwort = get_cashbook_pdf(DATA["konto"], DATA["datum"])
                        
                    if mode == "get_cashbook_date_last":
                        Antwort = get_cashbook_date_last(DATA["konto"])

                    if mode == "get_cashbook_date_before":
                        Antwort = get_cashbook_date_before(DATA["konto"], DATA["datum"])

                    if mode == "get_cashbook_date_after":
                        Antwort = get_cashbook_date_after(DATA["konto"], DATA["datum"])    

                    if mode == "get_mwst_data":
                        Antwort = get_mwst_data(DATA["datum"])
                    
                ########
                # MISC
                
                if mode == "search_tag":#return List with Tags as Dict
                    Antwort = search_tag(DATA)
                        
                if mode == "get_all_predictions":
                    Antwort = get_all_predictions()
                    
                if mode == "add_to_prediction":
                    Antwort = add_to_prediction(DATA["prediction_name"], DATA["answer"], DATA["word_data"])
                    
                if mode == "make_prediction":
                    Antwort = make_prediction(DATA["prediction_name"], DATA["word_data"])
        
                if mode == "create_chat":
                    Antwort = create_chat(DATA["title"], userdata["identification"])
                    
                if mode == "get_chat_list":
                    Antwort = get_chat_list(userdata["identification"])
                    
                if mode == "get_message_list":
                    Antwort = get_message_list(DATA["chat"], userdata["identification"])
                    
                if mode == "create_message":
                    Antwort = create_message(DATA["chat"], DATA["text"], userdata["identification"])

                if mode == "get_user_page_settings":
                    Antwort = get_user_page_settings(username, DATA)
        
                if mode == "set_user_page_settings":
                    Antwort = set_user_page_settings(username, DATA)
        
                if mode == "get_pdfs_file":
                    Antwort = get_pdfs_file()
                
                if mode == "get_directory_content":
                    Antwort = get_directory_content(DATA["path"])
                    
                if mode == "search_sn_by_dates":
                    Antwort = search_sn_by_dates(DATA["date_from"], DATA["date_to"])
                    
                if mode == "get_sn_data":
                    Antwort = get_sn_data(DATA["sn"])
                    
                if mode == "check_all_vat":
                    Antwort = "check_all_vat() DISABLED at the moment"
                    
                if mode == "get_mwst":
                    Antwort = get_mwst()
                    
                if mode == "get_all_mwst":
                    Antwort = get_all_mwst()
                
                if mode == "get_file_length":
                    Antwort = get_file_length(DATA["identification"])
                
                if mode == "get_file_part":
                    Antwort = get_file_part(DATA["identification"], DATA["index"])
                    
                if mode == "send_file_part":
                    Antwort = send_file_part(DATA["identification"], DATA["index"], DATA["data"])
                
                if mode == "pre_printing_documents":
                    Antwort = pre_printing_documents()

                if mode == "get_logs":
                    if userdata["admin"]:
                        Antwort = get_logs(DATA["document_type"], DATA["document_id"])
                        
                if mode == "create_backup":
                    if userdata["admin"]:
                        Antwort = create_backup()

                #if mode == "Print":#return Bool of sucess
                #    Antwort = Print(DATA["printer"], DATA["identification"], DATA["info"], DATA["quantity"])
                    
                if mode == "print_rappel":
                    Antwort = print_rappel(DATA["client"])

                if mode == "GetPrinter" or mode == "get_printer":#return List of Printer
                    Antwort = libs.BlueFunc.get_printer()

                if mode == "getUserList":#return Nothing
                    Antwort = getUserList()

                if mode == "addUserToList":#return List of User
                    set_log("user", "addUserToList", str(DATA["user"]), user_secure_key)
                    Antwort = addUserToList(DATA["user"])

                if mode == "getUserData":#return List of User
                    Antwort = getUserData(DATA["user"])

                if mode == "getUserData_from_key":#return List of User
                    Antwort = getUserData_from_key(user_secure_key)

                if mode == "setUserData":#set dict for User
                    set_log("user", "setUserData(" + str(DATA) + ")", "", user_secure_key)
                    Antwort = setUserData(DATA)

                if mode == "GetBarcode":#return String
                    Antwort = GetBarcode(DATA["position"], DATA["bytes"], ipname[0])
                
                if mode == "GetGewinn": # return Float
                    Antwort = GetGewinn(DATA)

                if mode == "get_barcode_helper": # return List + Name of Articles (<ID 100)
                    Antwort = get_barcode_helper(DATA["barcode"])

                if mode == "search_belt": # ( min_thick, max_thick, min_length, max_length, index):
                    Antwort = search_belt(DATA["min_thick"], DATA["max_thick"], DATA["min_length"], DATA["max_length"], DATA["index"])
                    
                if mode == "get_worker_total":
                    Antwort = get_worker_total(DATA["days"])

                if mode == "get_konto":
                    Antwort = get_konto(DATA["index"])

                if mode == "get_transaktion":
                    Antwort = get_transaktion(DATA["identification"])

                if mode == "search_transaktion_by_date":
                    Antwort = search_transaktion_by_date(DATA["datum"], DATA["index"])
                    
                if mode == "search_transaktion_by_rechnung":
                    Antwort = search_transaktion_by_rechnung(DATA["rechnung"])              
                    
                if mode == "create_transaktion":
                    if "RE" in str(DATA["rechnung"]):
                        set_log("invoice_e", "create_transaktion(" + str(DATA) + ")", str(DATA["rechnung"]), user_secure_key)
                    else:
                        set_log("invoice", "create_transaktion(" + str(DATA) + ")", str(DATA["rechnung"]), user_secure_key)
                        
                    Antwort = create_transaktion(DATA["konto"], DATA["betrag"], DATA["rechnung"], DATA["pin_code"])

                if mode == "create_transaktion_free":
                    set_log("transaction", "create_transaktion_free(" + str(DATA) + ")", "", user_secure_key)
                    Antwort = create_transaktion_free(DATA["datum"], DATA["uhrzeit"], DATA["betrag"], username, DATA["konto_in"], DATA["konto_out"], DATA["mitteilung"])

                if mode == "get_server_name":
                    Antwort = str(server_name)
                    
                if mode == "remove_unused_deliverys":
                    Antwort = remove_unused_deliverys()
                    
                #if mode == "remove_unused_articles":
                #    Antwort = remove_unused_articles()
                # Disabled as it remove also Stats
                    
                if mode == "get_server_version":
                    Antwort = str(server_version)                

                if mode == "close": # close server:
                    exit()

            try: print(str(mode) + " -> Antwort: " + str(Antwort))
            except: print("Antwort: " + str(Antwort))

    return Antwort

print("ZK-DATA SERVER Started")
print("version: " + server_version)

libs.ServerTools.set_server_ip(server_name)
libs.BlueFunc.BlueSave("api_ip", "127.0.0.1", data_dir)

api_secure_key = str(libs.BlueFunc.BlueLoad("api_secure_key", data_dir))
if api_secure_key == "None":
    api_secure_key = str(Timestamp())
    create_user("API", api_secure_key, api_secure_key, True)
    libs.BlueFunc.BlueSave("api_secure_key", api_secure_key, data_dir)

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        #Antwort = threading.currentThread().getName().encode() + b'\t' + str(threading.active_count()).encode()
        
        # /etc/zk-data-server/web/tmp/
        
        if debug:
            url_path = "../../zk-data-server-web/web" + self.path
        else:
            #url_path = "/etc/zk-data-server/web" + self.path
            url_path = "./web" + self.path
            
        url_path = url_path.replace("//", "/")
        
        #print("url_path: " + str(url_path))
        #url_path = urllib.parse.urlparse(url_path).path
        url_path = url_path.replace("%20", " ")
        url_path = url_path.replace("%C3%A4", "ä")
        print("url_path: " + str(url_path))
        
        if os.path.isdir(url_path):
            url_path = url_path + "/index.html"
        if not os.path.exists(url_path):
            if "/" in self.path:
                if url_path.split("/")[-1] == "":
                    if debug: url_path = "../web/index.html"
                    else: url_path = "./web/index.html"
        
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "*")
        
        if url_path.endswith(".js"):
            self.send_header("Content-type", "application/javascript")
            self.send_header("Cache-Control", "max-age=3600, public")
        elif url_path.endswith(".html"):
            self.send_header("Content-type", "text/html")
            self.send_header("Cache-Control", "max-age=3600, public")
        elif url_path.endswith(".pdf"):
            self.send_header("Content-type", "application/pdf")
            self.send_header("Cache-Control", "max-age=0, public")
        elif url_path.endswith(".css"):
            self.send_header("Content-type", "text/css")
            self.send_header("Cache-Control", "max-age=3600, public")
        elif url_path.endswith(".ico"):
            self.send_header("Content-type", "image/x-icon")
            self.send_header("Cache-Control", "max-age=3600, public")
        else: # Directory ?
            self.send_header("Content-type", "text/html")
            self.send_header("Cache-Control", "max-age=0, public")

        self.end_headers()
        
        Antwort = open(url_path, "rb").read()
        self.wfile.write(Antwort)
        
    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        
        '''Reads post request body'''
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        data_from_client = json.loads(body.decode('utf-8'))
        print("data_from_client: " + str(data_from_client))
        
        Antwort = run_mode(data_from_client)
        
        self.wfile.write(json.dumps(Antwort).encode())

class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    #address_family = socket.AF_INET6
    pass
  

def run():
    print("run...")
    server = ThreadingSimpleServer(('0.0.0.0', 51515), Handler)
    if USE_HTTPS:
        # openssl req -new -x509 -keyout cert.pem -out cert.pem -days 365 -nodes
        if not os.path.exists("/etc/zk-data-server/data/" + server_name + "/cert.pem"):
            os.system("cd /etc/zk-data-server/data/" + server_name + "/ && openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365")
            
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        context.load_cert_chain("/etc/zk-data-server/data/" + server_name + "/cert.pem", "/etc/zk-data-server/data/" + server_name + "/key.pem")
        server.socket = context.wrap_socket(server.socket, server_side=True)
        

    server.serve_forever()
    
if __name__ == '__main__':
    run()
