#!/usr/bin/env python3

man_list = []

file_data = open("python3-server/Server.py", "r").readlines()
for line in file_data:    
    if line.startswith("def "):
        line = line.rstrip()
        function_name = line.split("def ")[1]
        function_name = function_name.split("):")[0] + ")"
        print(function_name)

        man_list.append(function_name)
        
open("man", "w").write("\n".join(man_list))
